package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.Holiday;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

/**
 * Created by fujitsuPC on 5/16/2017.
 */
public interface HolidayDao extends JpaRepository<Holiday, Long> {
	
	Holiday findByDate(Date date);
	
}