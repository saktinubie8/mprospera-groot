package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchJobExecution;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchStepExecution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BatchStepExecutionDao extends JpaRepository<BatchStepExecution, Long> {

	Page<BatchStepExecution> findByBatchJobExecution(BatchJobExecution batchJobExecution, Pageable pageRequest);

}