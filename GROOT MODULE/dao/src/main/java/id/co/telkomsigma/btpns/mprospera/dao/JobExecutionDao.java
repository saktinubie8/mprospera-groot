package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchJobExecution;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;
import java.util.Date;

public interface JobExecutionDao extends JpaRepository<BatchJobExecution, BigInteger> {

	Page<BatchJobExecution> findByCreateTimeBetween(Date startDate, Date endDate, Pageable pageRequest);

	Page<BatchJobExecution> findAllByOrderByCreateTimeDesc(Pageable pageRequest);

}