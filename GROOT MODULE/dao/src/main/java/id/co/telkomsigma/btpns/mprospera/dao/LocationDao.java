package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface LocationDao extends JpaRepository<Location, String> {

	Location findByLocationCode(String code);
	
	Location findByLocationId(String id);

	@Query("SELECT l FROM Location l WHERE l.level = :level")
	List<Location> findAllLoc(@Param("level") String levelKfo);
	
	List<Location> findByParentLocation(String id);
	
}