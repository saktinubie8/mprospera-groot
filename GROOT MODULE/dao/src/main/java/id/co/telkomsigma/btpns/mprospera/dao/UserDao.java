package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserDao extends JpaRepository<User, String> {

	/**
	 * Get single user by email
	 * 
	 * @param email
	 * @return single User object
	 */
	User findByEmailIgnoreCase(String email);

	/**
	 * Get single user by username
	 * 
	 * @param username
	 * @return single User object
	 */
	User findByUsername(String username);

	User findByUserId(Long userId);

	/**
	 * Get user by username / email address from offset as much limit defined in
	 * pageable param
	 * 
	 * @param keyword
	 * @param pageable
	 * @return list of User object
	 */
	Page<User> findByUsernameLikeOrEmailLikeOrNameLikeAllIgnoreCaseOrderByUsernameAsc(String username, String email,
			String name, Pageable pageable);

	/**
	 * Get user by username / email address with given role from offset as much
	 * limit defined in pageable param
	 * 
	 * @param keyword
	 * @param authority
	 * @param pageable
	 * @return list of User object
	 */
	@Query("SELECT DISTINCT u FROM User u INNER JOIN u.roles r WHERE (LOWER(u.username) LIKE :keyword OR LOWER(u.email) LIKE :keyword OR LOWER(u.name) LIKE :keyword) AND r.authority = :authority ORDER BY u.username ASC, u.email ASC")
	Page<User> getUsers(@Param("keyword") String keyword, @Param("authority") String authority, Pageable pageable);

	@Query("SELECT DISTINCT u FROM User u WHERE (LOWER(u.username) LIKE :keyword OR LOWER(u.email) LIKE :keyword OR LOWER(u.name) LIKE :keyword) AND u.sessionKey IS NOT NULL ORDER BY u.username ASC, u.email ASC")
	Page<User> getUsersLogin(@Param("keyword") String keyword, Pageable pageable);

	@Query("SELECT DISTINCT u FROM User u WHERE (LOWER(u.username) LIKE :keyword OR LOWER(u.email) LIKE :keyword OR LOWER(u.name) LIKE :keyword) AND u.sessionKey IS NULL ORDER BY u.username ASC, u.email ASC")
	Page<User> getUsersLogout(@Param("keyword") String keyword, Pageable pageable);

	List<User> findByAccountNonLocked(Boolean accountNonLocked);

	List<User> findByOfficeCode(String locId);

	@Modifying
	@Transactional
	@Query("UPDATE User u set u.accountNonLocked = :accountNonLocked , u.raw = :raw WHERE u.userId = :userId")
	Integer failedLoginAttempt(@Param("userId") Long userId, @Param("raw") String raw,
			@Param("accountNonLocked") Boolean accountNonLocked);

}