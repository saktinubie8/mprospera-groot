package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PRSParamDao extends JpaRepository<PRSParameter, Long> {

	PRSParameter findByParameterId(@Param("parameterId") Long parameterId);
	
	@Query("SELECT DISTINCT p FROM PRSParameter p WHERE (LOWER(p.paramName) LIKE :keyword OR LOWER(p.paramDescription) LIKE :keyword) AND p.paramGroup = :paramGroup ORDER BY p.paramName ASC, p.paramDescription ASC")
	Page<PRSParameter> getParams(@Param("keyword") String keyword, @Param("paramGroup") String paramGroup,
			Pageable pageable);
	
	Page<PRSParameter> findByParamNameLikeOrParamDescriptionLikeAllIgnoreCaseOrderByParamNameAsc(String paramName,
			String paramDescription, Pageable pageable);
	
	void delete(PRSParameter parameterId);

}