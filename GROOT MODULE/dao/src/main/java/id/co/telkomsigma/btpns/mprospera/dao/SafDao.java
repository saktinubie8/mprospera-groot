package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.saf.Saf;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SafDao extends JpaRepository<Saf, Long> {

}