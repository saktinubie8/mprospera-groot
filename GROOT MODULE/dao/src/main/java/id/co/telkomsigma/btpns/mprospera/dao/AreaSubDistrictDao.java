package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AreaSubDistrictDao extends JpaRepository<AreaSubDistrict, String> {

    List<AreaSubDistrict> findByParentAreaId(String areaId);

}
