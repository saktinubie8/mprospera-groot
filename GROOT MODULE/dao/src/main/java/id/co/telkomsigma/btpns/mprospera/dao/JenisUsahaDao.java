package id.co.telkomsigma.btpns.mprospera.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.telkomsigma.btpns.mprospera.model.JenisUsaha;

public interface JenisUsahaDao extends JpaRepository<JenisUsaha, Long> {

	JenisUsaha findByKdSectorEkonomi(Long kdSectorEkonomi);
	
	JenisUsaha findByIdentifier(Long identifier);

	JenisUsaha findTop1ByCreatedDateOrderByIdDesc(Date date);

	JenisUsaha findTop1ByCreatedDateGreaterThanOrderByCreatedDateDesc(Date date);

	JenisUsaha findTop1ByOrderByCreatedDateDesc();

	List<JenisUsaha> findByOrderByCreatedDate();
}
