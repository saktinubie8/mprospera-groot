package id.co.telkomsigma.btpns.mprospera.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;

public interface MailParamDao extends JpaRepository<MailParameter, String> {

    MailParameter findById(Long id);

    MailParameter findByMessageParam(String messageParam);

    Page<MailParameter> findByMessageParamLikeOrMessageValueLikeAllIgnoreCaseOrderByMessageParamAsc(
            String messageParam, String messageValue, Pageable pagable);

}