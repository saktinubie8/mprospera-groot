package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
public interface TerminalDao extends JpaRepository<Terminal, Long> {

	Terminal findByImei(String imei);

	Page<Terminal> findByModelNumberLikeOrImeiLikeAllIgnoreCase(String modelNumber, String imei, Pageable pageable);

}