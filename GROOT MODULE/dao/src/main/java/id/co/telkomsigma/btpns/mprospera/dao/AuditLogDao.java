package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Date;

public interface AuditLogDao extends JpaRepository<AuditLog, String> {

	Page<AuditLog> findByDescriptionLikeIgnoreCase(String keyword, Pageable pageable);

	Page<AuditLog> findByDescriptionLikeIgnoreCaseAndActivityTypeAndCreatedDateBetweenOrderByCreatedDateDesc(
			String keyword, int activityType, Date startDate, Date endDate, Pageable pageable);

	Page<AuditLog> findByDescriptionLikeIgnoreCaseAndCreatedDateBetweenOrderByCreatedDateDesc(String keyword,
			Date startDate, Date endDate, Pageable pageable);

	Page<AuditLog> findByDescriptionLikeIgnoreCaseAndActivityType(String keyword, int activityType, Pageable pageable);

}