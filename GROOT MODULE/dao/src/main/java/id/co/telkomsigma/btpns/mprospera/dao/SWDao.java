package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface SWDao extends JpaRepository<SurveyWawancara, String> {

	@Query("SELECT COUNT(m) FROM SurveyWawancara m WHERE m.isDeleted = false")
	int countAll();

	@Query("SELECT m FROM SurveyWawancara m WHERE m.isDeleted = false ORDER BY m.createdBy ASC")
	Page<SurveyWawancara> findAll(Pageable pageable);

	@Query("SELECT m FROM SurveyWawancara m WHERE m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false AND areaId in :areaList ORDER BY m.createdBy ASC")
	Page<SurveyWawancara> findByCreatedDate(@Param("areaList") List<String> kelIdList,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

	Page<SurveyWawancara> findByAreaIdIn(List<String> kelIdList, Pageable pageRequest);

}