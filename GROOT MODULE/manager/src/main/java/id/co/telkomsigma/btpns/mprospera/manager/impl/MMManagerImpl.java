package id.co.telkomsigma.btpns.mprospera.manager.impl;


import id.co.telkomsigma.btpns.mprospera.dao.MMDao;
import id.co.telkomsigma.btpns.mprospera.manager.MMManager;
import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("mmManager")
public class MMManagerImpl implements MMManager {

	@Autowired
	private MMDao mmDao;

	@Override
	public Page<MiniMeeting> findAll(List<String> kelIdList) {
		return mmDao.findByAreaIdIn(kelIdList,new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<MiniMeeting> findByCreatedDate(List<String> kelIdList,Date startDate, Date endDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.DATE, 1);
		return mmDao.findByCreatedDate(startDate, cal.getTime(),kelIdList, new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<MiniMeeting> findByCreatedDatePageable(List<String> kelIdList,Date startDate, Date endDate, PageRequest pageRequest) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.DATE, 1);
		return mmDao.findByCreatedDate(startDate, cal.getTime(),kelIdList, pageRequest);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		
	}

}