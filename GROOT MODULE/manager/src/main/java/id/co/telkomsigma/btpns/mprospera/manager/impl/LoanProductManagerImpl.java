package id.co.telkomsigma.btpns.mprospera.manager.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import id.co.telkomsigma.btpns.mprospera.dao.LoanProductDao;
import id.co.telkomsigma.btpns.mprospera.manager.LoanProductManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

@Service("loanProductManager")
public class LoanProductManagerImpl implements LoanProductManager {

	@Autowired
	private LoanProductDao loanProductDao;

	
	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	//LoanProduct
	@Override
	public List<LoanProduct> getAllLoanProduct() {
		return loanProductDao.findAll();
	}	
	
	//detailMappingProduct
//	@Override
//	public List<DetailMappingProduct> getDetailMappingProduct() {
//		return loanProductMappingDao.findAll();
//	}
	
	@Override
	public LoanProduct findById(Long id) {
		return loanProductDao.findOne(id);
	}

	@Override
	public LoanProduct findByProductCode(String productCode) {
		return loanProductDao.findByProductCode(productCode);
	}
	


	@Override
	@Transactional
	public LoanProduct save(LoanProduct loanProduct) {
		LoanProduct dbProduct = new LoanProduct();
		if(loanProduct.getProductId()!=null)
			dbProduct = loanProductDao.findOne(loanProduct.getProductId());
		ILock lock = hazelcastInstance.getLock("Loan Product #"+loanProduct.getProductId());
		lock.lock();
		try {
			dbProduct.setProductRate(loanProduct.getProductRate());
			dbProduct.setMargin(loanProduct.getProductRate());
			dbProduct.setIir(loanProduct.getIir());
			dbProduct.setJenisNasabah(loanProduct.getJenisNasabah());
			dbProduct.setTujuanpembiayaan(loanProduct.getTujuanpembiayaan());
			dbProduct.setRegularPiloting(loanProduct.getRegularPiloting());
			dbProduct.setTypeNasabah(loanProduct.getTypeNasabah());
			dbProduct.setUpdateDate(new Date());
			dbProduct.setTenorBulan(loanProduct.getTenorBulan());
			
			loanProductDao.save(dbProduct);
		}finally {
			lock.unlock();
		}
		return dbProduct;
	}

	@Override
	public Page<LoanProduct> searchLoanMappingproduct(String keyword, LinkedHashMap<String, String> orderMap, int offset, int limit) {
		if (keyword == null) {
            keyword = "";
        }
		keyword = "%" + keyword + "%";
		  PageRequest pageRequest = new PageRequest((offset / limit), limit);
          if (keyword != null && !keyword.equalsIgnoreCase("")) {
        	  return loanProductDao.getMappingLoanProduct(keyword, pageRequest);
          }else {
        	return loanProductDao.findByJenisNasabahLikeOrRegularPilotingLikeAllIgnoreCaseOrderByUpdateDateDesc(keyword, keyword, pageRequest);
          }
	}
	//list Detail Mapping search with loan product
//	@Override
//	public Page<DetailMappingProduct> searchMappingProduct(String keyword, LinkedHashMap<String, String> orderMap,
//			int offset, int limit) {
//		if (keyword == null) {
//            keyword = "";
//        }
//		keyword = "%" + keyword + "%";
//		PageRequest pageRequest = new PageRequest((offset / limit), limit);
//		 if (keyword != null && !keyword.equalsIgnoreCase("")) {
//			 return loanProductMappingDao.getMappingLoanProduct(keyword, pageRequest);
//		 }else {
//			return loanProductMappingDao.findByJenisMMSLikeOrKetentuanLikeAllIgnoreCaseOrderByKetentuanAsc(keyword, keyword, pageRequest);
//		 }
//	}


	@Override
	public LoanProduct getLoanProductById(Long productId) {
		return loanProductDao.findByProductId(productId);
	}
	
	@Transactional
	@Override
	public LoanProduct insertLoanProduct(LoanProduct loanProduct) {
	    loanProduct = loanProductDao.save(loanProduct);
        return loanProduct;
	}
	
	@Transactional
	@Override
	public LoanProduct updateLoanProduct(LoanProduct loanProduct) {	
		LoanProduct loanFromDb = loanProductDao.findByProductId(loanProduct.getProductId());
		loanFromDb.setJenisNasabah(loanProduct.getJenisNasabah());
		loanFromDb.setUpdateDate(new Date());
		loanFromDb.setTujuanpembiayaan(loanProduct.getTujuanpembiayaan());
		loanFromDb.setRegularPiloting(loanProduct.getRegularPiloting());
		loanFromDb.setProductName(loanProduct.getProductName());
        loanProductDao.save(loanFromDb);
        return loanProduct;
	}



}
