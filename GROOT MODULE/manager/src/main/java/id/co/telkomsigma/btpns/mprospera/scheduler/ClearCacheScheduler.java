package id.co.telkomsigma.btpns.mprospera.scheduler;

import id.co.telkomsigma.btpns.mprospera.feign.HawkeyeInterface;
import id.co.telkomsigma.btpns.mprospera.feign.IronmanInterface;
import id.co.telkomsigma.btpns.mprospera.feign.VisionInterface;
import id.co.telkomsigma.btpns.mprospera.feign.WolverineInterface;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ClearCacheScheduler {

    protected final Log log = LogFactory.getLog(getClass());

    @Autowired
    private WolverineInterface wolverineInterface;

    @Autowired
    private HawkeyeInterface hawkeyeInterface;

    @Autowired
    private IronmanInterface ironmanInterface;

    @Autowired
    private VisionInterface visionInterface;

    @Scheduled(cron="0 0 4 * * ?")
    public void clearAllCache(){
        log.info("Clear Vision Cache");
        visionInterface.clearAllCache();
        log.info("Clear Wolverine Cache");
        wolverineInterface.clearAllCache();
        log.info("Clear Hawkeye Cache");
        hawkeyeInterface.clearAllCache();
        log.info("Clear Ironman Cache");
        ironmanInterface.clearAllCache();
    }
}
