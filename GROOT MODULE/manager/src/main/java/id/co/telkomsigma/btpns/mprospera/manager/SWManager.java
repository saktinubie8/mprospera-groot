package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface SWManager {

	int countAll();

	Page<SurveyWawancara> findAll(List<String> kelIdList);

	Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);

	Page<SurveyWawancara> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate,
			PageRequest pageRequest);

	void clearCache();

}