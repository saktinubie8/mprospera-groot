package id.co.telkomsigma.btpns.mprospera.service;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.JenisUsahaManager;
import id.co.telkomsigma.btpns.mprospera.model.JenisUsaha;

@Service("jenisUsahaService")
public class JenisUsahaService extends GenericService {

	@Autowired
	private JenisUsahaManager jenisUsahaManager;

	public List<JenisUsaha> getAllJenisUsaha() {
		return jenisUsahaManager.getAllJenisUsaha();
	}
	
	public Page<JenisUsaha> listJenisUsaha(int offset, int limit){
		return jenisUsahaManager.listJenisUsahaPageable(offset, limit);
	}

	public Date getLastData() {
		JenisUsaha jenisUsahaLastDateData = jenisUsahaManager.findTop1ByCreateDateOrderByCreatedDateDesc();
		log.info("jenisUsahaLastDateData : "+jenisUsahaLastDateData);
		return jenisUsahaLastDateData.getCreatedDate();
	}
}
