package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AuditLogManager;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("auditLogService")
public class AuditLogService extends GenericService {

	@Autowired
    private AuditLogManager auditLogManager;

    public List<AuditLog> getLogs() {
        return auditLogManager.getAll();
    }

	public AuditLog insertAuditLog(AuditLog auditLog)
	{
		return auditLogManager.insertAuditLog(auditLog);
	}

	public Page<AuditLog> searchAuditLog(String keyword, String startDate,
			String endDate, String activityType, LinkedHashMap<String, String> orderMap,
			int finalOffset, int finalLimit) {
		return auditLogManager.searchAuditLog(keyword, startDate, endDate, activityType, orderMap, finalOffset, finalLimit);
	}

}