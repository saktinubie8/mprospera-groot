package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import id.co.telkomsigma.btpns.mprospera.model.JenisUsaha;


public interface JenisUsahaManager {
	
	JenisUsaha findById(Long id);
	JenisUsaha findByKdSectorEkonomi(Long kdSectorEkonomi);
	
	JenisUsaha findByIdentifier(Long identifier);
	
	JenisUsaha doSave(JenisUsaha jenisUsaha);
	List<JenisUsaha> getAllJenisUsaha();
	
	JenisUsaha getJenisUsahaByDate(Date date);
	
	JenisUsaha findTop1ByCreateDateMoreThanOrderByCreatedDateDesc(Date date);
	
	JenisUsaha findTop1ByCreateDateOrderByCreatedDateDesc();
	
	String doDelete(JenisUsaha jenisUsaha);
	
	JenisUsaha doEdit(JenisUsaha jenisUsaha);
	
	Page<JenisUsaha> listJenisUsahaPageable(int offset,int limit);
}
