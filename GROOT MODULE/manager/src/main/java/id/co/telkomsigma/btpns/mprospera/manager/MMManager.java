package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.mm.MiniMeeting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface MMManager {

	Page<MiniMeeting> findAll(List<String> kelIdList);

	Page<MiniMeeting> findByCreatedDate(List<String> kelIdList,Date startDate, Date endDate);

	Page<MiniMeeting> findByCreatedDatePageable(List<String> kelIdList,Date startDate, Date endDate, PageRequest pageRequest);

	void clearCache();

}