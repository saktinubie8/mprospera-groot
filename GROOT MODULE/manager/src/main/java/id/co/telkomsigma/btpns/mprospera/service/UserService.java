package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.exception.UserProfileException;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.nio.charset.Charset;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userService")
public class UserService extends GenericService implements UserDetailsService {

    @Autowired
    private UserManager userManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        User user = userManager.getUserByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User '" + username + "' not found.");
        }
        return (UserDetails) user;
    }

    public UserDetails loadUserByUsernameNoException(String username) {
        User user = userManager.getUserByUsername(username);
        return (UserDetails) user;
    }

    public User loadUserByUserId(final Long userId) throws UsernameNotFoundException {
        return userManager.getUserByUserId(userId);
    }

    public void lockUser(User user, final Long lockDuration) throws Exception {
        userManager.lockUser(user, lockDuration);
    }

    public void unlockUser(User user) {
        userManager.unlockUser(user);
    }

    public User createUser(User user) {
        try {
            return userManager.insertUser(user);
        } catch (UserProfileException e) {
            log.error(e.getMessage(), e);
            byte[] responseBody = e.getMessage().getBytes();
            throw new HttpClientErrorException(HttpStatus.CONFLICT, "CONFLICT", responseBody, Charset.forName("UTF-8"));
        }
    }

    public User updateUser(User user) {
        return userManager.updateUser(user);
    }

    public User updateProfile(User user) {
        return userManager.updateProfileAdmin(user);
    }

    public List<User> getAllLockedUser() {
        return userManager.getAllLockedUser();
    }

    public Integer failedAttempt(User user) {
        return userManager.failedLoginAttempt(user.getUserId(), user.getRaw(), user.isAccountNonLocked());
    }

    public void resetPassword(User user) {
        userManager.resetPassword(user);
    }

    public void enableUser(User user) {
        userManager.enableUser(user);
    }

    public void disableUser(User user) {
        userManager.disableUser(user);
    }

    public Page<User> searchUser(String keyword, String authority,
                                 LinkedHashMap<String, String> sbOrder, int finalOffset, int limit) {
        return userManager.searchUsers(keyword, authority, sbOrder, finalOffset, limit);
    }

    public Page<User> searchUserLogin(String keyword, String status,
                                      LinkedHashMap<String, String> sbOrder, int finalOffset, int limit) {
        return userManager.searchUsersLogin(keyword, status, sbOrder, finalOffset, limit);
    }

    public void clearUserCache() {
        userManager.clearCache();
    }

    public User logout(User user) {
        user.setSessionKey(null);
        user.setSessionTime(null);
        userManager.updateUser(user);
        return user;
    }

    public User logoutScheduler(User user) {
        user.setSessionKey(null);
        user.setSessionTime(null);
        userManager.updateUser(user);
        return user;
    }

    public List<User> getAllUser() {
        return userManager.getAllUser();
    }

}