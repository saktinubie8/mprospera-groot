package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.CustomerDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.pojo.CustomerIDPhoto;

import java.util.LinkedHashMap;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("customerManager")
public class CustomerManagerImpl implements CustomerManager {

    private String queryStr = "select cust.id as customer_id, cust.cif_number as cif_number, cust.id_number as id_number, cust.name as name, cust.date_of_birth as date_of_birth, photo.id_photo as id_photo "
            + "from t_customer cust "
            + "left join t_sw_id_photo photo on cust.sw_id = photo.sw_id where cust.cif_number like :keyword or cust.id_number like :keyword ";

    private String queryCountStr = "select count(cust.cif_number) from t_customer cust "
            + "left join t_sw_id_photo photo on cust.sw_id = photo.sw_id where cust.cif_number like :keyword or cust.id_number like :keyword ";

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private CustomerDao customerDao;

    @Override
    public void clearCache() {
        // TODO Auto-generated method stub
    }

    @Override
    public Customer findById(long parseLong) {
        // TODO Auto-generated method stub
        return customerDao.findOne(parseLong);
    }

    @SuppressWarnings("unchecked")
    @Override
    public Page<CustomerIDPhoto> searchCustomerByCiffOrIdNumber(String keyword, LinkedHashMap<String, String> orderMap,
                                                                int offset, int limit) {
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
        PageRequest pageRequest = new PageRequest((offset / limit), limit);
        Query query = this.em.createNativeQuery(queryStr, "CustomerIDPhoto");
        query.setParameter("keyword", keyword);
        query.setFirstResult(offset);
        query.setMaxResults(limit);

        Query qCount = this.em.createNativeQuery(queryCountStr);
        qCount.setParameter("keyword", keyword);

        List<CustomerIDPhoto> listCustomer = query.getResultList();
        long listSize = ((Number) qCount.getSingleResult()).longValue();
        Page<CustomerIDPhoto> customerId = new PageImpl<>(listCustomer, pageRequest, listSize);
        em.close();
        return customerId;
    }

}