package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.SpringBatchManager;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchJobExecution;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchStepExecution;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;

@Service("springBatchService")
public class SpringBatchService extends GenericService{

	@Autowired
	private SpringBatchManager springBatchManager;

	public Page<BatchJobExecution> getAllBatchJobExecution(String strStartDate,
			String strEndDate, int offset, int limit) throws Exception {	
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		if(strStartDate==null && strEndDate==null)
		{
			return springBatchManager.findJobExecution(offset,limit);
		}
		else if(strStartDate==null)
		{
			Date endDate = formatter.parse(strEndDate);
			Date startDate = DateUtils.addDays(endDate, -1);
			return springBatchManager.findJobExecution(startDate,endDate,offset,limit);
		}
		else if(strEndDate==null)
		{
			Date startDate = formatter.parse(strStartDate);
			Date endDate = DateUtils.addDays(startDate, 1);
			return springBatchManager.findJobExecution(startDate,endDate,offset,limit);
		}
		else
		{
			Date endDate = formatter.parse(strEndDate);
			Date startDate = formatter.parse(strStartDate);
			return springBatchManager.findJobExecution(startDate,endDate,offset,limit);
		}
	}

	public Page<BatchStepExecution> getStepExecution(Long jobExecutionId,
			int offset, int limit) {
		return springBatchManager.findStepExecution(jobExecutionId,offset,limit);
	}

}