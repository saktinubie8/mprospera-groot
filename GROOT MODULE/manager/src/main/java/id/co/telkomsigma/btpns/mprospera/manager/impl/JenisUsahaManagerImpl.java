package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;

import id.co.telkomsigma.btpns.mprospera.dao.JenisUsahaDao;
import id.co.telkomsigma.btpns.mprospera.manager.JenisUsahaManager;
import id.co.telkomsigma.btpns.mprospera.model.JenisUsaha;

@Service("jenisUsahaManager")
public class JenisUsahaManagerImpl implements JenisUsahaManager {
	
	private static final Logger log = Logger.getLogger(JenisUsahaManagerImpl.class.getName());
	
	private final Log loging = LogFactory.getLog(getClass());

	@Autowired
	private JenisUsahaDao jUDao;

	@Autowired
	private HazelcastInstance hazelcastInstance;

	@Override
	@Transactional
	public JenisUsaha doSave(JenisUsaha jenisUsaha) {
//		JenisUsaha dbData = new JenisUsaha();
//		if (jenisUsaha.getId() != null) {
//			dbData =  jUDao.findOne(jenisUsaha.getId());
//		}
//			ILock lock = hazelcastInstance.getLock("Jenis Usaha - " + jenisUsaha.getIdentifier().toString());
//			try {
//			lock.lock();
//			loging.info("Hazelcast Lock accessed");
//			dbData.setIdentifier(jenisUsaha.getIdentifier());
//			dbData.setTujuanPembiayaan(jenisUsaha.getTujuanPembiayaan());log.info(dbData.getTujuanPembiayaan());
//			dbData.setKatBidangUsaha(jenisUsaha.getKatBidangUsaha());log.info(dbData.getKatBidangUsaha());
//			dbData.setDescUsaha(jenisUsaha.getDescUsaha());log.info(dbData.getDescUsaha());
//			dbData.setKdSectorEkonomi(jenisUsaha.getKdSectorEkonomi());log.info(dbData.getKdSectorEkonomi());
//			dbData.setSandiBaruDesc(jenisUsaha.getSandiBaruDesc());log.info(dbData.getSandiBaruDesc());
//			dbData.setCreatedDate(jenisUsaha.getCreatedDate());log.info(dbData.getSandiBaruDesc());
				jUDao.save(jenisUsaha);
//			}catch(Exception e){
//			log.error(e.getMessage());
//			} finally {
//				lock.unlock();
//			}
			
		return jenisUsaha;
	}
		

	@Override
	public JenisUsaha findByKdSectorEkonomi(Long kdSectorEkonomi) {
		return jUDao.findByKdSectorEkonomi(kdSectorEkonomi);
	}


	@Override
	public JenisUsaha findById(Long id) {
		return jUDao.findOne(id);
	}


	@Override
	public String doDelete(JenisUsaha jenisUsaha) {
		jUDao.delete(jenisUsaha);
		return "Data Have Been Deleted";
	}


	@Override
	@Transactional
	public JenisUsaha doEdit(JenisUsaha jenisUsaha) {
		JenisUsaha dbData = new JenisUsaha();
		if (jenisUsaha.getId() != null) {
			dbData = jUDao.findByIdentifier(jenisUsaha.getIdentifier());
			log.info(jenisUsaha.getIdentifier().toString());
			ILock lock = hazelcastInstance.getLock("Jenis Usaha #" + jenisUsaha.getIdentifier().toString());
			try {
			lock.lock();
			loging.info("Hazelcast Lock accessed");
				dbData.setTujuanPembiayaan(jenisUsaha.getTujuanPembiayaan());log.info(dbData.getTujuanPembiayaan());
				dbData.setKatBidangUsaha(jenisUsaha.getKatBidangUsaha());log.info(dbData.getKatBidangUsaha());
				dbData.setDescUsaha(jenisUsaha.getDescUsaha());log.info(dbData.getDescUsaha());
				dbData.setKdSectorEkonomi(jenisUsaha.getKdSectorEkonomi());log.info(dbData.getKdSectorEkonomi());
				dbData.setSandiBaruDesc(jenisUsaha.getSandiBaruDesc());log.info(dbData.getSandiBaruDesc());
				dbData.setCreatedDate(jenisUsaha.getCreatedDate());log.info(dbData.getSandiBaruDesc());
				jUDao.save(dbData);
			}catch(Exception e){
			log.error(e.getMessage());
			} finally {
				lock.unlock();
			}
		}
		return dbData;
	}

	@Override
	public List<JenisUsaha> getAllJenisUsaha() {
		
		return jUDao.findAll(sortByCreatedDate());
	}
	
	private Sort sortByCreatedDate() {
		return new Sort(Sort.Direction.ASC,"createdDate");
	}


	@Override
	public JenisUsaha getJenisUsahaByDate(Date date) {
		return jUDao.findTop1ByCreatedDateOrderByIdDesc(date);
	}


	@Override
	public JenisUsaha findTop1ByCreateDateMoreThanOrderByCreatedDateDesc(Date date) {
		return jUDao.findTop1ByCreatedDateGreaterThanOrderByCreatedDateDesc(date);
	}


	@Override
	public JenisUsaha findTop1ByCreateDateOrderByCreatedDateDesc() {
		return jUDao.findTop1ByOrderByCreatedDateDesc();
	}


	@Override
	public Page<JenisUsaha> listJenisUsahaPageable(int offset, int limit) {
		PageRequest pageRequest = new PageRequest((offset/limit),limit);
		return jUDao.findAll(pageRequest);
	}


	@Override
	public JenisUsaha findByIdentifier(Long identifier) {
		return jUDao.findByIdentifier(identifier);
	}
}
