package id.co.telkomsigma.btpns.mprospera.service;


import id.co.telkomsigma.btpns.mprospera.manager.MessageLogsManager;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;

@Service("messageLogsService")
public class MessageLogsService extends GenericService {

    @Autowired
    private MessageLogsManager messageLogsManager;

    public Page<MessageLogs> search(String terminalId, String date,
                                    LinkedHashMap<String, String> columnMap, int finalOffset,
                                    int finalLimit) {
        // TODO Auto-generated method stub
        return messageLogsManager.search(terminalId, finalOffset, finalLimit);
    }

}