package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalService")
public class TerminalService extends GenericService {
    @Autowired
    private TerminalManager terminalManager;
    
    @Autowired
	private UserManager userManager;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    public Terminal logout(Terminal terminal)
    {
    	terminal.setLogin(false);
    	terminalManager.updateTerminal(terminal);
    	return terminal;
    }
    
    public Terminal logoutScheduler(Terminal terminal)
    {
    	terminal.setLogin(false);
    	terminalManager.updateTerminal(terminal);
    	return terminal;
    }

    public List<Terminal> getAllTerminal() {
        return terminalManager.getAllTerminal();
    }

	public Page<Terminal> search(String keyword,
			LinkedHashMap<String, String> columnMap, int finalOffset, int finalLimit) {
		// TODO Auto-generated method stub
		return terminalManager.searchTerminals(keyword, columnMap, finalOffset, finalLimit);
	}

	public Page<TerminalActivity> searchActivity(String keyword,
			Long terminalId, String startedDate, String endedDate,
			LinkedHashMap<String, String> columnMap, int finalOffset,
			int finalLimit) {
		// TODO Auto-generated method stub
		return terminalManager.searchTerminalActivity(keyword, terminalId, startedDate, endedDate, columnMap, finalOffset, finalLimit);
	}

    public Terminal loadTerminalByImei(final String imei) throws UsernameNotFoundException {
        return terminalManager.getTerminalByImei(imei);
    }
	
	
}