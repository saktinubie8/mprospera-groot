package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.PmDao;
import id.co.telkomsigma.btpns.mprospera.manager.PmManager;
import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("pmManager")
public class PmManagerImpl implements PmManager{

	@Autowired
	private PmDao pmDao; 

	@Override
	public Page<ProjectionMeeting> findByCreatedDate(List<String> kelIdList, Date startDate,
			Date endDate) {
		// TODO Auto-generated method stub
		return pmDao.findByCreatedDate(kelIdList, startDate,endDate,new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<ProjectionMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate,
			Date endDate, PageRequest pageRequest) {
		// TODO Auto-generated method stub
		return pmDao.findByCreatedDate(kelIdList, startDate,endDate,pageRequest);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		
	}

}