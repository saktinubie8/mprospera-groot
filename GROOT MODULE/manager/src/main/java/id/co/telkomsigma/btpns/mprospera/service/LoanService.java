package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.pojo.Ap3rData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("loanService")
public class LoanService extends GenericService {

	@Autowired
	private LoanManager loanManager;

	public Loan findById(String loanId) {
		if (loanId == null || "".equals(loanId))
			return null;
		return loanManager.findById(Long.parseLong(loanId));
	}

	public List<Loan> findLoanByDisbursementDate(Date startDate, Date endDate){
		return loanManager.findLoanByDisbursementDate(startDate, endDate);
	}
	
	public List<Ap3rData> findLoanByDisbursementDateManual(Date startDate, Date endDate){
		return loanManager.findLoanByDisbursementDateManual(startDate, endDate, WebGuiConstant.STATUS_APPROVED);
		
	}
	
}