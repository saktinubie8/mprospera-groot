package id.co.telkomsigma.btpns.mprospera.service;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import id.co.telkomsigma.btpns.mprospera.manager.MailParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;

@Service("mailParameterService")
public class MailParameterService extends GenericService {

    @Autowired
    private MailParamManager mailParamMgr;

    public List<MailParameter> getAll() {
        return mailParamMgr.getAll();
    }

    public void clearMailParamCache() {
        mailParamMgr.clearCache();
    }

    public Page<MailParameter> searchMailParam(String keyword, LinkedHashMap<String, String> columnMap, int finalOffset, int finalLimit) {
        return mailParamMgr.searchParam(keyword, columnMap, finalOffset, finalLimit);
    }

    public void updateParam(MailParameter mailParameter, final String messageValue) throws Exception {
        mailParamMgr.updateParam(mailParameter, messageValue);
    }

    public MailParameter getParamById(Long id) {
        return mailParamMgr.getParamById(id);
    }

    public MailParameter getParamByName(String messageParam) {
        return mailParamMgr.getParamByName(messageParam);
    }

}