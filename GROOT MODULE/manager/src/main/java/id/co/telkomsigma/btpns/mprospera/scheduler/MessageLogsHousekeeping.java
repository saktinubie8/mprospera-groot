package id.co.telkomsigma.btpns.mprospera.scheduler;

import id.co.telkomsigma.btpns.mprospera.dao.MessageLogsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Created by Dzulfiqar on 13/07/2017.
 */
@Component
public class MessageLogsHousekeeping {

    @Autowired
    MessageLogsDao messageLogsDao;

    @Scheduled(cron="0 0 3 * * ?")
    public void runHouseKeeping(){
        messageLogsDao.deleteAll();
    }
}
