package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;

public interface LoanProductManager {
    
	@Cacheable(value="allProductLoan",unless="#result == null")
    List<LoanProduct> getAllLoanProduct();
    
//	List<DetailMappingProduct>getDetailMappingProduct();
//	
	@Cacheable(value = "findById", unless = "#result == null")
	LoanProduct findById(Long id);

	LoanProduct findByProductCode(String productCode);

	LoanProduct save(LoanProduct loanProduct);
	
    Page<LoanProduct> searchLoanMappingproduct(String keyword, LinkedHashMap<String, String> orderMap, int offset, int limit);
//    
//    Page<DetailMappingProduct> searchMappingProduct (String keyword, LinkedHashMap<String, String> orderMap, int offset, int limit);
    
    @Cacheable(value="productById", unless="#result == null")
    LoanProduct getLoanProductById(Long productId);
    
    LoanProduct insertLoanProduct(LoanProduct loanProductl);
    
    
    @Caching(put = {
			@CachePut(value = "loanProductById", key="#loanProduct.loanProductId", unless = "#result == null")
			 },
		evict = {
				@CacheEvict("allProductLoan") })
    LoanProduct updateLoanProduct (LoanProduct loanProduct);

}