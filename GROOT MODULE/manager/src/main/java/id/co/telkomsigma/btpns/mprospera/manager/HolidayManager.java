package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.Holiday;

import java.util.Date;

public interface HolidayManager {

	Holiday findById(Long id);

	Holiday findByDate(Date date);

	Holiday save(Holiday holiday);

}