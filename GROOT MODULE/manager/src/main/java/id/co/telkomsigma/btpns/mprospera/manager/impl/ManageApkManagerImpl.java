package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.ManageApkDao;
import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Service("manageApkManager")
public class ManageApkManagerImpl implements ManageApkManager {

    @Autowired
    ManageApkDao manageApkDao;

    @Override
    public List<ManageApk> getAll() {
        return manageApkDao.findAll();
    }

    @Override
    public Page<ManageApk> search(LinkedHashMap<String, String> orderMap,
                                  int offset, int limit) {
        PageRequest pageRequest = new PageRequest((offset / limit), limit);
        return manageApkDao.getApk(pageRequest);
    }

    @Override
    @CacheEvict(allEntries = true, beforeInvocation = true, cacheNames = {"gro.apk.getById",
            "gro.apk.getByVersion", "vsn.apk.getByApkVersion", "vsn.apk.isApkValid"})
    public void uploadApk(byte[] apkBytes, String version, String description, User user) {
        ManageApk apk = new ManageApk();
        apk.setCreatedBy(user);
        apk.setCreatedDate(new Date());
        apk.setUpdateBy(user);
        apk.setUpdateDate(new Date());
        apk.setAllowedVersion(true);
        apk.setApkVersion(version);
        apk.setChangeDescription(description);
        apk.setApkFile(apkBytes);
        manageApkDao.save(apk);
    }

    @Override
    @Cacheable(value = "gro.apk.getById", unless = "#result == null")
    public ManageApk getById(Long apkId) {
        return manageApkDao.findOne(apkId);
    }

    @Override
    @Cacheable(value = "gro.apk.getByVersion", unless = "#result == null")
    public ManageApk getByVersion(String version) {
        return manageApkDao.findByApkVersion(version);
    }

    @Override
    @CacheEvict(allEntries = true, beforeInvocation = true, cacheNames = {"gro.apk.getById",
            "gro.apk.getByVersion"})
    public void clearCache() {
        // TODO Auto-generated method stub

    }

    @Override
    @CacheEvict(allEntries = true, beforeInvocation = true, cacheNames = {"gro.apk.getById",
            "gro.apk.getByVersion", "vsn.apk.getByApkVersion", "vsn.apk.isApkValid"})
    public ManageApk updateApk(ManageApk updateApk) {
        // TODO Auto-generated method stub
        ManageApk apkFromDb = manageApkDao.findOne(updateApk.getApkId());
        apkFromDb.setAllowedVersion(updateApk.isAllowedVersion());
        apkFromDb.setApkFile(updateApk.getApkFile());
        apkFromDb.setApkVersion(updateApk.getApkVersion());
        apkFromDb.setChangeDescription(updateApk.getChangeDescription());
        apkFromDb.setCreatedBy(updateApk.getCreatedBy());
        apkFromDb.setCreatedDate(updateApk.getCreatedDate());
        apkFromDb.setFile(updateApk.getFile());
        apkFromDb.setName(updateApk.getName());
        apkFromDb.setUpdateBy(updateApk.getUpdateBy());
        apkFromDb.setUpdateDate(updateApk.getUpdateDate());
        manageApkDao.save(apkFromDb);
        return updateApk;
    }

}