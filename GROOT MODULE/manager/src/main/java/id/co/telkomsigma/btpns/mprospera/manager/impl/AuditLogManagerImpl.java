package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.AuditLogDao;
import id.co.telkomsigma.btpns.mprospera.manager.AuditLogManager;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;


@Service("auditLogManager")
public class AuditLogManagerImpl implements AuditLogManager {

    @Autowired
    private AuditLogDao auditLogDao;

    @Override
    @Cacheable(value = "gro.audit.getAll", unless = "#result == null")
    public List<AuditLog> getAll() {
        return auditLogDao.findAll();
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "gro.audit.getAll", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "gro.audit.searchAuditLog", allEntries = true, beforeInvocation = true)})
    public AuditLog insertAuditLog(AuditLog auditLog) {
        auditLog = auditLogDao.save(auditLog);
        return auditLog;
    }

    @Override
    //@Cacheable(value = "gro.audit.searchAuditLog", unless = "#result == null")
    public Page<AuditLog> searchAuditLog(String keyword, String startDate, String endDate, String activityType, LinkedHashMap<String, String> orderMap, int offset, int limit) {

        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
        Date sDate = new Date();
        Date eDate = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            if (!startDate.isEmpty()) {
                Long tanggalMulai = formatter.parse(startDate).getTime();
                sDate = new Date(tanggalMulai);
            }
            if (!endDate.isEmpty()) {
                Long tanggalAkhir = formatter.parse(endDate).getTime() + (1000 * 60 * 60 * 24);
                eDate = new Date(tanggalAkhir);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        PageRequest pageRequest = new PageRequest((offset / limit), limit);
        //get datanya
        if (activityType.isEmpty()) {
            if (startDate.isEmpty() && endDate.isEmpty()) {
                //cari based on keyword saja
                return (auditLogDao.findByDescriptionLikeIgnoreCase(keyword, pageRequest));
            } else if (!startDate.isEmpty() && !endDate.isEmpty()) {
                //cari based on keyword dan date
                return (auditLogDao.findByDescriptionLikeIgnoreCaseAndCreatedDateBetweenOrderByCreatedDateDesc(keyword, sDate, eDate, pageRequest));
            }
        }
        if (startDate.isEmpty() && endDate.isEmpty()) {
            if (activityType.isEmpty()) {
                //cari based on keyword saja
                return (auditLogDao.findByDescriptionLikeIgnoreCase(keyword, pageRequest));
            } else {
                //cari based on keyword dan activity type
                return (auditLogDao.findByDescriptionLikeIgnoreCaseAndActivityType(keyword, Integer.parseInt(activityType), pageRequest));
            }
        }
        if ((!activityType.isEmpty()) && (!startDate.isEmpty() && !endDate.isEmpty())) {
            //kondisi semua parameter terpenuhi
            return (auditLogDao.findByDescriptionLikeIgnoreCaseAndActivityTypeAndCreatedDateBetweenOrderByCreatedDateDesc(keyword, Integer.parseInt(activityType), sDate, eDate, pageRequest));
        }
        return auditLogDao.findAll(pageRequest);
    }
}