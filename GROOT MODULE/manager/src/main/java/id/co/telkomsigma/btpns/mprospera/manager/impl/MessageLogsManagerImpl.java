package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.MessageLogsDao;
import id.co.telkomsigma.btpns.mprospera.manager.MessageLogsManager;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service("messageLogsManager")
public class MessageLogsManagerImpl implements MessageLogsManager{
	
    @Autowired
	MessageLogsDao messageLogsDao;
	
	@Override
	public Page<MessageLogs> search(String terminalActivityId, int finalOffset, int finalLimit) {

		//untuk isian data, untuk tese
		TerminalActivity terminalActivity = new TerminalActivity();
		terminalActivity.setTerminalActivityId(terminalActivityId);
		Page<MessageLogs> msg = messageLogsDao.findByTerminalActivityCache(terminalActivity,new PageRequest((finalOffset / finalLimit), finalLimit));		
        
        return msg;
	}

	@Override
	public void insert(MessageLogs messageLogs) {
		messageLogsDao.save(messageLogs);
	}

}