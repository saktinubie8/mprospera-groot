package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.PRSParamDao;
import id.co.telkomsigma.btpns.mprospera.dao.ParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedHashMap;
import java.util.List;


@Service("paramManager")
public class ParamManagerImpl implements ParamManager {

    @Autowired
    private ParamDao paramDao;

    @Autowired
    private PRSParamDao prsParamDao;

    @Override
    public List<SystemParameter> getAll() {
        return paramDao.findAll();
    }

    @Override
    public Page<SystemParameter> searchParam(String keyword, String paramGroup, LinkedHashMap<String, String> orderMap, int offset, int limit) {
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
        PageRequest pageRequest = new PageRequest((offset / limit), limit);
        if (paramGroup != null && !paramGroup.equalsIgnoreCase("")) {
            return paramDao.getParams(keyword, paramGroup, pageRequest);
        } else {
            return paramDao.findByParamNameLikeOrParamDescriptionLikeAllIgnoreCaseOrderByParamNameAsc(keyword, keyword, pageRequest);
        }
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "gro.param.getParamByParamId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "gro.param.paramByParamName", key = "#param.paramName", beforeInvocation = true),
            @CacheEvict(value = "ppi.param.paramByParamName", key = "#param.paramName", beforeInvocation = true),
            @CacheEvict(value = "irn.param.paramByParamName", key = "#param.paramName", beforeInvocation = true),
            @CacheEvict(value = "hwk.param.paramByParamName", key = "#param.paramName", beforeInvocation = true),
            @CacheEvict(value = "wln.param.paramByParamName", key = "#param.paramName", beforeInvocation = true),
            @CacheEvict(value = "vsn.param.paramByParamName", key = "#param.paramName", beforeInvocation = true)
    })
    public SystemParameter updateParam(SystemParameter param, String paramValue) {
        // TODO Auto-generated method stub
        SystemParameter result = paramDao.findByParameterId(param.getParameterId());
        result.setParamValue(paramValue);
        paramDao.save(result);
        return result;
    }

    @Transactional
    @Override
    @Cacheable(value = "gro.param.getParamByParamId", unless = "#result == null")
    public SystemParameter getParamByParamId(Long paramId) {
        SystemParameter param = paramDao.findByParameterId(paramId);
        return param;
    }

    @Transactional
    @Override
    @Cacheable(value = "gro.prs.param.getParamByParamId", unless = "#result == null")
    public PRSParameter getPRSParamByParamId(Long paramId) {
        PRSParameter param = prsParamDao.findByParameterId(paramId);
        return param;
    }

    @Override
    @Cacheable(value = "gro.param.paramByParamName", unless = "#result == null", key = "#paramName")
    public SystemParameter getParamByParamName(String paramName) {
        SystemParameter param = paramDao.findByParamName(paramName);
        if (param != null)
            param.getCreatedBy().setOfficeCode(null);
        ;
        return param;
    }

    @Override
    @CacheEvict(value = {"gro.param.paramByParamName", "gro.param.getParamByParamId",
            "gro.prs.param.getParamByParamId", "gro.prs.param.findByParameterId"}, allEntries = true, beforeInvocation = true)
    public void clearCache() {

    }

    @Override
    @Cacheable(value = "gro.prs.param.findByParameterId", unless = "#result == null")
    public PRSParameter findByParameterId(Long parameterId) {
        // TODO Auto-generated method stub
        return prsParamDao.findByParameterId(parameterId);
    }

    @Override
    public List<PRSParameter> getAllPRSParam() {
        // TODO Auto-generated method stub
        return prsParamDao.findAll();
    }

    @Override
    public Page<PRSParameter> searchPRSParam(String keyword, String paramGroup, LinkedHashMap<String, String> orderMap,
                                             int offset, int limit) {
        // TODO Auto-generated method stub
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
        PageRequest pageRequest = new PageRequest((offset / limit), limit);
        if (paramGroup != null && !paramGroup.equalsIgnoreCase("")) {
            return prsParamDao.getParams(keyword, paramGroup, pageRequest);
        } else {
            return prsParamDao.findByParamNameLikeOrParamDescriptionLikeAllIgnoreCaseOrderByParamNameAsc(keyword, keyword, pageRequest);
        }
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "gro.prs.param.getParamByParamId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "gro.prs.param.findByParameterId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.getAllPrsParam", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.findByParameterId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.getFileDayaByFileName", allEntries = true, beforeInvocation = true)
    })
    public PRSParameter updatePRSParam(PRSParameter param, String paramValue, byte[] paramValueByte, String paramDesc) {
        // TODO Auto-generated method stub
        PRSParameter result = prsParamDao.findByParameterId(param.getParameterId());
        result.setParamValueString(paramValue);
        result.setParamValueByte(paramValueByte);
        result.setParamDescription(paramDesc);
        prsParamDao.save(result);
        return result;

    }

    @Override
    @Caching(evict = {@CacheEvict(value = "gro.prs.param.getParamByParamId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "gro.prs.param.findByParameterId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.getAllPrsParam", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.findByParameterId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.getFileDayaByFileName", allEntries = true, beforeInvocation = true)
    })
    public PRSParameter insertPRSParam(PRSParameter prsparam) {
        // TODO Auto-generated method stub
        return prsParamDao.save(prsparam);
    }

    @Override
    @Caching(evict = {@CacheEvict(value = "gro.prs.param.getParamByParamId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "gro.prs.param.findByParameterId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.getAllPrsParam", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.findByParameterId", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "irn.prs.param.getFileDayaByFileName", allEntries = true, beforeInvocation = true)
    })
    public PRSParameter deleteByParameterId(Long parameterId) {
        PRSParameter result = prsParamDao.findByParameterId(parameterId);
        // TODO Auto-generated method stub
        prsParamDao.delete(result);
        return result;
    }

}