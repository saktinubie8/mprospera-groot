package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.ManageApkManager;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service("manageApkService")
public class ManageApkService extends GenericService{
	
    @Autowired
    private ManageApkManager manageApkManager;
    
	private boolean enable = true;

	private String progressMessage;

	public String getProgressMessage() {
		return progressMessage;
	}

    public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	
	public List<ManageApk> getAll() {
        return manageApkManager.getAll();
    }
    
	public Page<ManageApk> search(LinkedHashMap<String, String> columnMap, int finalOffset, int finalLimit) {
		return manageApkManager.search(columnMap, finalOffset, finalLimit);
	}
	
	public void uploadApk(byte[] apkBytes, String version, String description,  User user) {
		progressMessage = "Sedang meng-upload Apk";
		manageApkManager.uploadApk(apkBytes, version, description, user);
	}

    public ManageApk getById(Long apkId)
    {
    	return manageApkManager.getById(apkId);
    }
    
    public ManageApk update(ManageApk apkUpdate){
    	return manageApkManager.updateApk(apkUpdate);
    }
    
    public ManageApk getByVersion(String version){
    	return manageApkManager.getByVersion(version);
    }

}