package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.data.domain.Page;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;

public interface MailParamManager {

    List<MailParameter> getAll();

    Page<MailParameter> searchParam(String keyword, LinkedHashMap<String, String> orderMap, int offset, int limit);

    MailParameter updateParam(MailParameter param, String messageValue);

    MailParameter getParamById(Long id);

    MailParameter getParamByName(String messageParam);

    void clearCache();

}