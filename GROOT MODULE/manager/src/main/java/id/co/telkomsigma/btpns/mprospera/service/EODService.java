package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.feign.GambitInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Dzulfiqar on 5/05/2017.
 */
@Service("eodService")
public class EODService extends GenericService {

    @Autowired
    private GambitInterface gambitInterface;

    public String runEod(){
        log.trace("Run EOD");
        gambitInterface.runEod();
        return "";
    }

}