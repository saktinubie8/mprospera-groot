package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.sda.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface AreaManager {	

	AreaKelurahan findById(String id);

	List<AreaSubDistrict> getSubDistrictByParentId(String areaId);

	Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByParentAreaId(String areaId);

	Page<AreaSubDistrictDetails> findByCreatedDate(String areaId, Date parse,
			Date parse2);

	Page<AreaSubDistrictDetails> findByCreatedDatePageable(String areaId,
			Date parse, Date parse2, PageRequest pageRequest);

	void clearCache();

	List<AreaDistrict> getWismaById(String officeCode);

	AreaDistrict findDistrictById(String districtCode);

}