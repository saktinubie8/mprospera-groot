package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;
import id.co.telkomsigma.btpns.mprospera.model.survey.SurveyDetail;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;

public interface SurveyManager {

	Page<Survey> findByCreatedDate(Date startDate, Date endDate, String officeId);

	Page<Survey> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId);

	void clearCache();

}