package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.wowib.WowIbBalance;

public interface WowibManager {

	WowIbBalance findById(Long id);

	WowIbBalance save(WowIbBalance account);

}