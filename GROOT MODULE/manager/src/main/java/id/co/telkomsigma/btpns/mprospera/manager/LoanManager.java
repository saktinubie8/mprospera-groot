package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.pojo.Ap3rData;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface LoanManager {

	void save(Loan loan);

	Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId);

	Page<Loan> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId);

	void clearCache();

	Loan findById(long parseLong);

	List<Loan> findLoanByDisbursementDate(Date startDate, Date endDate);
	
	List<Ap3rData> findLoanByDisbursementDateManual(Date startDate, Date endDate, String status);

}