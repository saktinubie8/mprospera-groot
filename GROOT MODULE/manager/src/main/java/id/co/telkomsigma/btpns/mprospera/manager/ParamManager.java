package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
public interface ParamManager {
    
	List<SystemParameter> getAll();
	
	List<PRSParameter> getAllPRSParam();

	Page<SystemParameter> searchParam(String keyword, String paramGroup, LinkedHashMap<String, String> orderMap, int offset, int limit);
	
	Page<PRSParameter> searchPRSParam(String keyword, String paramGroup, LinkedHashMap<String, String> orderMap, int offset, int limit);

	SystemParameter updateParam (SystemParameter param, String paramValue);
	
	PRSParameter updatePRSParam (PRSParameter param, String paramValue, byte[] paramValueByte, String paramDesc);
	
	PRSParameter insertPRSParam (PRSParameter prsparam);
	
	SystemParameter getParamByParamId(Long paramId);
	
	PRSParameter getPRSParamByParamId(Long paramId);

	SystemParameter getParamByParamName(String paramName);
	
	void clearCache();
	
	PRSParameter findByParameterId(Long parameterId);
	
	PRSParameter deleteByParameterId(Long parameterId);
	
}