package id.co.telkomsigma.btpns.mprospera.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient("vision")
public interface VisionInterface {	
	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
	@RequestMapping(value = "/vision/internal/wsValidation", method = {RequestMethod.POST})
	public String wsValidation(@RequestParam(name="username") String username,@RequestParam(name="imei")  String imei,@RequestParam(name="sessionKey")  String sessionKey,@RequestParam(name="apkVersion")  String apkVersion);
//	
	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
	@RequestMapping(value = "/vision/internal/loginValidation", method = {RequestMethod.POST})
	public String loginValidation(@RequestParam(name="username")  String username,@RequestParam(name="imei")  String imei,@RequestParam(name="apkVersion")  String apkVersion);

	@HystrixCommand
	@HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
	@RequestMapping(value = "/vision/clearCache", method = {RequestMethod.POST})
	public String clearAllCache();

}
