package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.TerminalActivityDao;
import id.co.telkomsigma.btpns.mprospera.dao.TerminalDao;
import id.co.telkomsigma.btpns.mprospera.manager.TerminalManager;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;


/**
 * Created by Dzulfiqar on 11/11/15.
 */
@Service("terminalManager")
public class TerminalManagerImpl implements TerminalManager {
     
    @Autowired
	TerminalDao terminalDao;
    
    @Autowired
    TerminalActivityDao terminalActivityDao;
	 
    
    public Terminal getTerminalByTerminalId(Long terminalId) {
    	Terminal terminal = terminalDao.findOne(terminalId);
        return terminal;
    }
    
    public Terminal getTerminalByImei(String imei) {
    	return terminalDao.findByImei(imei);
    }

    @Override
    public Terminal updateTerminal(Terminal terminal) {
        terminalDao.save(terminal);
        return terminal;
    }

	@Override
	public List<Terminal> getAllTerminal() {
		List<Terminal> terminals = terminalDao.findAll();
		return  terminals;
	}
	
	@Override
	public Page<Terminal> searchTerminals(String keyword,
			LinkedHashMap<String, String> orderMap, int offset, int limit) {
		if (keyword == null) {
			keyword = "";
        }
		keyword = "%" + keyword + "%";
		

            PageRequest pageRequest = new PageRequest((offset / limit), limit);
        
        return terminalDao.findByModelNumberLikeOrImeiLikeAllIgnoreCase(keyword,keyword, pageRequest);
	}

	@Override
	public Page<TerminalActivity> searchTerminalActivity(String keyword, Long terminalId, String startedDate, String endedDate, 
			LinkedHashMap<String, String> orderMap, int offset, int limit) {
		if (keyword == null) {
			keyword = "";
        }
		keyword = "%" + keyword + "%";
		
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
							
            PageRequest pageRequest = new PageRequest((offset / limit), limit);
            if(!startedDate.equals("") && !endedDate.equals("")){
            	 
				try {
					
					Long tanggalMulai = formatter.parse(startedDate).getTime();
					Long tanggalAkhir = formatter.parse(endedDate).getTime()+ (1000 * 60 * 60 * 24);
					Date startDate = new Date(tanggalMulai);
					Date endDate = new Date(tanggalAkhir);
					
	            	return terminalActivityDao.getTerminalActivity(terminalId, startDate, endDate,  pageRequest);

				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
    	    }            	
       return terminalActivityDao.getTerminalActivityWithoutRange(terminalId, pageRequest);            
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		
	}

}