package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.BatchStepExecutionDao;
import id.co.telkomsigma.btpns.mprospera.dao.JobExecutionDao;
import id.co.telkomsigma.btpns.mprospera.manager.SpringBatchManager;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchJobExecution;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchStepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("springBatchManager")
public class SpringBatchManagerImpl implements SpringBatchManager {

    @Autowired
    private JobExecutionDao jobExecutionDao;
    @Autowired
    private BatchStepExecutionDao batchStepExecutionDao;

    @Override
    public Page<BatchJobExecution> findJobExecution(int offset, int limit) {
        // TODO Auto-generated method stub
        PageRequest pageRequest = new PageRequest((offset / limit), limit);
        return jobExecutionDao.findAllByOrderByCreateTimeDesc(pageRequest);
    }

    @Override
    public Page<BatchJobExecution> findJobExecution(Date startDate,
                                                    Date endDate, int offset, int limit) {
        // TODO Auto-generated method stub
        return jobExecutionDao.findByCreateTimeBetween(startDate, endDate, new PageRequest(offset, limit));
    }

    @Override
    public Page<BatchStepExecution> findStepExecution(Long jobExecutionId,
                                                      int offset, int limit) {
        BatchJobExecution batchJobExecution = new BatchJobExecution();
        batchJobExecution.setJobExecutionId(jobExecutionId);
        return batchStepExecutionDao.findByBatchJobExecution(batchJobExecution, new PageRequest((offset / limit), limit));
    }

}