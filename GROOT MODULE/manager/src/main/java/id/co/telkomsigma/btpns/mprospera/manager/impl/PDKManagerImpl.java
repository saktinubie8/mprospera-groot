package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.PDKDao;
import id.co.telkomsigma.btpns.mprospera.manager.PDKManager;
import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("pdkManager")
public class PDKManagerImpl implements PDKManager {

	@Autowired
	PDKDao pdkDao;

	@Override
	public int countAll() {
		// TODO Auto-generated method stub
		return pdkDao.countAll();
	}

	@Override
	public Page<PelatihanDasarKeanggotaan> findAll(List<String> userList) {
		// TODO Auto-generated method stub
		return pdkDao.findAll(userList, new PageRequest(0, countAll()));
	}

	@Override
	public Page<PelatihanDasarKeanggotaan> findByCreatedDate(List<String> userList, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.DATE, 1);
		return pdkDao.findByCreatedDate(userList, startDate, cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<PelatihanDasarKeanggotaan> findByCreatedDatePageable(List<String> userList, Date startDate, Date endDate,
			PageRequest pageRequest) {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.DATE, 1);
		return pdkDao.findByCreatedDate(userList, startDate,cal.getTime() , pageRequest);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
	}

}