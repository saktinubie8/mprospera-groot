package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sda.*;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service("areaService")
public class AreaService extends GenericService {

	@Autowired
	private AreaManager areaManager;		
	
	@Autowired
	private UserManager userManager;
	
	@Autowired
	private LocationManager locationManager;
	
	public Integer countAll(AreaDistrict areaDistrictByUser) {
		return areaManager.getAllSubDistrictDetailsByParentAreaId(areaDistrictByUser.getAreaId()).getSize();
	}

	public List<AreaDistrict> getAreaDistrictByUsername(String username) {		
		User user = userManager.getUserByUsername(username);
		List<AreaDistrict> retVal = new ArrayList<>();
		AreaDistrict single = areaManager.findDistrictById(user.getDistrictCode());
		if(single!=null)
		{
			retVal.add(single);
			return retVal;
		}
		else
			return areaManager.getWismaById(user.getOfficeCode());
	}
	
	public List<AreaSubDistrict> getAreaSubDistrictByUsername(String username)
	{
		List<AreaDistrict> districtList = this.getAreaDistrictByUsername(username);
		List<AreaSubDistrict> subDistrictRetVal = new ArrayList<>();
		for(AreaDistrict district:districtList)
		{
			List<AreaSubDistrict> subDistrict = areaManager.getSubDistrictByParentId(district.getAreaId());
			if(subDistrict!=null && subDistrict.size()>0)
				subDistrictRetVal.addAll(subDistrict);
		}
		return subDistrictRetVal;
	}

	public String findLocationCodeByLocationId(String id) {
		Location loc = locationManager.findById(id);
		if(loc!=null)
			return loc.getLocationCode();
		return id;
	}
	
	public List<Location> findAllKfo(){
		return locationManager.findAllKfo();	
	}

	public Location findLocationById(String id){
		return locationManager.findByLocationId(id);
	}
	
	public List<Location> findByParentLocation(String id){
		return locationManager.findByParentLocation(id);
	}

	public Location findByCode(String code){
		return locationManager.findByCode(code);
	}

}