package id.co.telkomsigma.btpns.mprospera.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.pojo.Ap3rData;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

@Service("ap3rservice")
public class Ap3rService {
	
    protected Log log = LogFactory.getLog(this.getClass());

    @Autowired
    private LoanService loanService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private CacheManager cacheManager;
    
    @Autowired
    private AuditLogService auditLogService;
    
    @Autowired
    private TerminalActivityService terminalActivityService;
    
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    
    @Autowired
    private TerminalService terminalService;
    
    protected final Log LOGGER = LogFactory.getLog(getClass());
    private int days;
    private String reffNo;
    
    
    private void loadAreaDataToMemory() {
        List<Location> locs = areaService.findAllKfo();
        for (Location loc : locs) {
            cacheManager.getCache("gro.location.findByCode").put(loc.getLocationCode(), loc);
            cacheManager.getCache("gro.location.findByLocationId").put(loc.getLocationId(), loc);
        }
    }

    public void generateAp3r(String strStartDate, String strEndDate,String path) throws Exception {
		int ok = 0;
		int fail = 0;
		LOGGER.info("getAp3rReport : "+path);
        String fileName = path+"/AP3R_M-Prospera_No_APPID";
        String zipName = path+"/";
		String logFileNameErr = zipName+new SimpleDateFormat("yyyyMMdd").format(new Date())+".error.log";
		String logFileNameOK = zipName+new SimpleDateFormat("yyyyMMdd").format(new Date())+".success.log";
		LOGGER.info("generateAp3r service running: "+strStartDate+" - "+strEndDate);

		try {
    		days = getDays()==0?6:getDays()+1;
	        loadAreaDataToMemory();
	
	        Map<String, Object> parameters = new HashMap<String, Object>();
	
	        Date startDate = new Date();
	        Date endDate = new Date();
	
	        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        final String dateTimeFormatPattern = "yyyyMMMdd";
	        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
	
	        if (strStartDate != null && !"".equals(strStartDate)) {
	            startDate = java.sql.Date.valueOf(LocalDate.parse(strStartDate, dtf));
	        }
	
	        // convert date to calendar
	        Calendar c = Calendar.getInstance();
	        c.setTime(startDate);
	
	        // manipulate date
	        //c.add(Calendar.MONTH, 1);
	        c.add(Calendar.DATE, days);
	        c.clear(Calendar.HOUR_OF_DAY);
	        c.clear(Calendar.AM_PM);
	        c.clear(Calendar.MINUTE);
	        c.clear(Calendar.SECOND);
	        c.clear(Calendar.MILLISECOND);//same with c.add(Calendar.DAY_OF_MONTH, 1);
	
	        // convert calendar to date
	        Date currentDatePlusOne = c.getTime();
	
	        if (strEndDate != null && !"".equals(strEndDate)) {
	            endDate = java.sql.Date.valueOf(LocalDate.parse(strEndDate, dtf));
	            Calendar cEnd = Calendar.getInstance();
	            cEnd.setTime(endDate);
	            cEnd.add(Calendar.DATE, days);
	            cEnd.clear(Calendar.HOUR_OF_DAY);
	            cEnd.clear(Calendar.AM_PM);
	            cEnd.clear(Calendar.MINUTE);
	            cEnd.clear(Calendar.SECOND);
	            cEnd.clear(Calendar.MILLISECOND);
	            endDate = cEnd.getTime();
	        } else {
	            endDate = currentDatePlusOne;
	        }
	
	        List<Loan> loanList = loanService.findLoanByDisbursementDate(startDate, endDate);
	        for (Loan loan : loanList) {
	        	if (loan.getSwId() != null) {
//                    LOGGER.info("APPID : " + loan.getAppId());
                    String appidInfo = "";
	        		try {
        				long start = System.currentTimeMillis();

	        			Long loanId = loan.getLoanId();
	                    parameters.put("loanId", loanId);
	                    String wisma = loan.getAppId().substring(0, 5);
	                    Location wismaLoc = areaService.findByCode(wisma);
	                    String kfoId = wismaLoc.getParentLocation();
	                    Location kfoLoc = areaService.findLocationById(kfoId);
	                    String kfoCode = kfoLoc.getLocationCode();
	                    String kfoName = kfoLoc.getName();
	        			appidInfo = kfoCode + "-" + kfoName + "-" + loan.getAppId();
	        			
	                    File file = new File(fileName + "_" + loan.getAppId() + "_" + sdf.format(loan.getDisbursementDate()) + ".pdf");
	                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	                    reportService.generatePdf("AP3R.jrxml", parameters, byteArrayOutputStream);
	                    byte[] bniBytes = byteArrayOutputStream.toByteArray();
	                    FileOutputStream fos = new FileOutputStream(file);
	                    fos.write(bniBytes);
	                    ZipFile zipFile = new ZipFile(zipName + kfoCode + kfoName + ".zip");
	                    ZipParameters zipParameters = new ZipParameters();
	                    zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
	                    zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
	                    String dateJustYMD = String.valueOf(loan.getDisbursementDate()).substring(0, 10);
	                    zipParameters.setRootFolderInZip(dateJustYMD);
	                    zipFile.addFile(file, zipParameters);
	                    fos.flush();
	                    fos.close();
	                    file.delete();
	                    
	                    ok++;
                        long duration = System.currentTimeMillis()-start;
	                	saveLogFile(logFileNameOK,"Success generateAp3r: "+appidInfo+" ("+duration+" ms)");
		            } catch (Exception e) {
		            	fail++;
		            	log.error("Error generateAp3r loanList: "+e.getMessage());
		            	saveLogFile(logFileNameErr,appidInfo+": "+e.getMessage());
		            }
	        	}
	        }
    	}catch (Exception e) {
        	log.error("Error generateAp3r: "+e.getMessage());
        	saveLogFile(logFileNameErr,"Error generateAp3r: "+e.getMessage()+"\n"+e.toString());
		}finally {
			log.info(WebGuiConstant.GENERATE_AP3R+": Success="+ok+", Error="+fail);
			auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R+": Success="+ok+", Error="+fail));
			
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
                        
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_AP3R_REPORT);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(getReffNo());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername("system");

                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, null);
                    }
                });
            } catch (Exception e) {
                log.error("generateAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
		}
    }
    
    
    public void getAp3rReport(String strStartDate, String strEndDate,String path) throws Exception {
		int ok = 0;
		int fail = 0;
		LOGGER.info("getAp3rReport : "+path);
        String fileName = path+"/AP3R_M-Prospera_No_APPID";
        String zipName = path+"/";
		String logFileNameErr = zipName+new SimpleDateFormat("yyyyMMdd").format(new Date())+".error.log";
		String logFileNameOK = zipName+new SimpleDateFormat("yyyyMMdd").format(new Date())+".success.log";
		LOGGER.info("generateAp3r service running: "+strStartDate+" - "+strEndDate);

		try {
    		days = getDays()==0?6:getDays()+1;
	        loadAreaDataToMemory();
	
	        Map<String, Object> parameters = new HashMap<String, Object>();
	
	        Date startDate = new Date();
	        Date endDate = new Date();
	
	        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        final String dateTimeFormatPattern = "yyyyMMMdd";
	        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
	
	        if (strStartDate != null && !"".equals(strStartDate)) {
	            startDate = java.sql.Date.valueOf(LocalDate.parse(strStartDate, dtf));
	        }
	
	        // convert date to calendar
	        Calendar c = Calendar.getInstance();
	        c.setTime(startDate);
	
	        // manipulate date
	        //c.add(Calendar.MONTH, 1);
	        c.add(Calendar.DATE, days);
	        c.clear(Calendar.HOUR_OF_DAY);
	        c.clear(Calendar.AM_PM);
	        c.clear(Calendar.MINUTE);
	        c.clear(Calendar.SECOND);
	        c.clear(Calendar.MILLISECOND);//same with c.add(Calendar.DAY_OF_MONTH, 1);
	
	        // convert calendar to date
	        Date currentDatePlusOne = c.getTime();
	
	        if (strEndDate != null && !"".equals(strEndDate)) {
	            endDate = java.sql.Date.valueOf(LocalDate.parse(strEndDate, dtf));
	            Calendar cEnd = Calendar.getInstance();
	            cEnd.setTime(endDate);
	            cEnd.add(Calendar.DATE, days);
	            cEnd.clear(Calendar.HOUR_OF_DAY);
	            cEnd.clear(Calendar.AM_PM);
	            cEnd.clear(Calendar.MINUTE);
	            cEnd.clear(Calendar.SECOND);
	            cEnd.clear(Calendar.MILLISECOND);
	            endDate = cEnd.getTime();
	        } else {
	            endDate = currentDatePlusOne;
	        }
	
	        try {
	        	File files = new File(path);
	 	        File[] oldReport = files.listFiles();
	 	        if(oldReport != null) {
	 	        	int i = 0;
	 	        	for(File oldFile : oldReport) {
	 	        		String extension = getFileExtension(oldFile);
	 	        		if(extension.contains("zip")) {
	 	        			oldFile.delete();
	 	        			i++;
	 	        		}
	 	        	}
	 	        	log.info(i+" old report deleted, Generating new ones");
	 	        }else {
	 	        	log.info("old report already cleared");
	 	        }
	        }catch(Exception e) {
	        	log.error("Error deleting old files : "+e.getMessage());
	        	saveLogFile(logFileNameErr, "Error deleting old files : "+e.getMessage());
	        }
	       
	        
			List<Ap3rData> ap3rDataList = loanService.findLoanByDisbursementDateManual(startDate, endDate);
	        
	        log.info(ap3rDataList.toArray().toString());
	        ap3rDataList.forEach(System.out::println);
	        ap3rDataList.stream().forEach(System.out::println);
	        
	        int dataCount = 0;
	        for (Ap3rData ap3rData : ap3rDataList) {
	        	if (ap3rData.getSwId() != null) {
//                    LOGGER.info("APPID : " + loan.getAppId());
                    String appidInfo = "";
	        		try {
        				long start = System.currentTimeMillis();

	        			Long loanId = ap3rData.getLoanId();
	                    parameters.put("loanId", loanId);
	                    String kfoId = ap3rData.getMmsParentCode();
	                    Location kfoLoc = areaService.findLocationById(kfoId);
	                    String kfoCode = kfoLoc.getLocationCode();
	                    String kfoName = kfoLoc.getName();
	        			appidInfo = kfoCode + "-" + kfoName + "-" + ap3rData.getAppId();
	        			
	        			
	                    File file = new File(fileName + "_" + ap3rData.getAppId() + "_" + sdf.format(ap3rData.getDisbursementDate()) + ".pdf");
	                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	                    reportService.generatePdf("AP3R.jrxml", parameters, byteArrayOutputStream);
	                    byte[] bniBytes = byteArrayOutputStream.toByteArray();
	                    FileOutputStream fos = new FileOutputStream(file);
	                    fos.write(bniBytes);
	                    
	                    
	                    ZipFile zipFile = new ZipFile(zipName + ap3rData.getMmsLocationCode() +"_"+ ap3rData.getMmsName() + ".zip");
	                    ZipParameters zipParameters = new ZipParameters();
	                    zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
	                    zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
	                    String dateJustYMD = String.valueOf(ap3rData.getDisbursementDate()).substring(0, 10);
	                    zipParameters.setRootFolderInZip(dateJustYMD);
	                    zipFile.addFile(file, zipParameters);
	                    fos.flush();
	                    fos.close();
	                    file.delete();
	                    
						File mmsZip = new File(zipName + ap3rData.getMmsLocationCode() + "_" + ap3rData.getMmsName() + ".zip");
						ZipFile kfoZip = new ZipFile(zipName + kfoLoc.getLocationCode().trim() + "_" + kfoLoc.getName().trim() + ".zip");
						ZipParameters zipParam = new ZipParameters();
						zipParam.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
						zipParam.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
						kfoZip.addFile(mmsZip, zipParam);
						
	
						if(dataCount >= ap3rDataList.size()-1) {
							mmsZip.delete();
						}else if(!ap3rData.getMmsName().equalsIgnoreCase(ap3rDataList.get(dataCount+1).getMmsName())) {
							mmsZip.delete();
						}
						dataCount++;
	                    
	                    ok++;
                        long duration = System.currentTimeMillis()-start;
	                	saveLogFile(logFileNameOK,"Success generateAp3r: "+appidInfo+" ("+duration+" ms)");
		            } catch (Exception e) {
		            	fail++;
		            	dataCount++;
		            	log.error("Error generateAp3r loanList: "+e.getMessage());
		            	saveLogFile(logFileNameErr,appidInfo+": "+e.getMessage());
		            }
	        	}else {
	        		dataCount++;
	        	}
	        }
    	}catch (Exception e) {
        	log.error("Error generateAp3r: "+e.getMessage());
        	saveLogFile(logFileNameErr,"Error generateAp3r: "+e.getMessage()+"\n"+e.toString());
		}finally {
			log.info(WebGuiConstant.GENERATE_AP3R+": Success="+ok+", Error="+fail);
			auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R+": Success="+ok+", Error="+fail));
			
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
                        
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_AP3R_REPORT);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(getReffNo());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername("system");

                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, null);
                    }
                });
            } catch (Exception e) {
                log.error("generateAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
		}
    }
    
    private String getFileExtension(File file) {
    	String filename = file.getName();
    	int lastIndexOf = filename.lastIndexOf(".");
    	if(lastIndexOf <= 0) {
    		return "";
    	}else {
    		return filename.substring(lastIndexOf+1);
    	}
    }

	private AuditLog generateAuditLog(String description) {
		reffNo = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(AuditLog.GENERATE_REPORT);
		auditLog.setCreatedBy("Mprospera");
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription(description);
		auditLog.setReffNo(reffNo);
		return auditLog;
	}

	public int getDays() {
		return days;
	}

	public String getReffNo() {
		return reffNo;
	}

	public void setReffNo(String reffNo) {
		this.reffNo = reffNo;
	}

	public void setDays(int days) {
		this.days = days;
	}
	
	
	private void saveLogFile(String filename, String logtxt) {
		try {
			FileWriter fw = new FileWriter(filename, true); // the true will Append the new data
			fw.write(logtxt+"\n");// appends the string to the file
			fw.close();
		} catch (Exception ioe) {
			log.error("saveLogFile IOException: " + ioe.getMessage());
		}
	}

}