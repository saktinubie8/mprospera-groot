package id.co.telkomsigma.btpns.mprospera.manager.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import id.co.telkomsigma.btpns.mprospera.dao.HolidayDao;
import id.co.telkomsigma.btpns.mprospera.manager.HolidayManager;
import id.co.telkomsigma.btpns.mprospera.model.Holiday;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;

@Service("holidayManager")
public class HolidayManagerImpl implements HolidayManager {

	@Autowired
	private HolidayDao dao;

	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	@Override
	public Holiday findById(Long id) {
		return dao.findOne(id);
	}

	@Override
	public Holiday findByDate(Date date) {
		return dao.findByDate(date);
	}

	@Override
	@Transactional
	public Holiday save(Holiday holiday) {
		Holiday dbData = new Holiday();
		if(holiday.getId()!=null)
			dbData = dao.findOne(holiday.getId());
		ILock lock = hazelcastInstance.getLock("Holiday-"+holiday.getId());
		lock.lock();
		try {
			dbData.setDate(holiday.getDate());
			dao.save(dbData);
		}finally {
			lock.unlock();
		}
		return dbData;
	}
	
}