package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface PmManager {

	Page<ProjectionMeeting> findByCreatedDate(List<String> kelIdList, Date startDate, Date endDate);
	
	Page<ProjectionMeeting> findByCreatedDatePageable(List<String> kelIdList, Date startDate, Date endDate, PageRequest pageRequest);
	
	void clearCache();

}