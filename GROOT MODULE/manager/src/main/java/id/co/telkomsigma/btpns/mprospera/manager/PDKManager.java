package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Date;
import java.util.List;

public interface PDKManager {
	
	int countAll();
	
	Page<PelatihanDasarKeanggotaan> findAll(List<String> userList);

	Page<PelatihanDasarKeanggotaan> findByCreatedDate(List<String> userList, Date startDate, Date endDate);

	Page<PelatihanDasarKeanggotaan> findByCreatedDatePageable(List<String> userList, Date startDate, Date endDate, PageRequest pageRequest);

	void clearCache();

}