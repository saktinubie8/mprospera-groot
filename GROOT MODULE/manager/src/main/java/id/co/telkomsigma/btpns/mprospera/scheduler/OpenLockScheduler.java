package id.co.telkomsigma.btpns.mprospera.scheduler;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class OpenLockScheduler {

	@Autowired
	UserService userServiceWithCache;
	
	@Scheduled(fixedRate=60000)
	public void checkUnlock()
	{
		List<User> listLockedUser = userServiceWithCache.getAllLockedUser();
		if(listLockedUser!=null)
			if(listLockedUser.size()>0)
			{
				for(User user:listLockedUser)
				{
					String lockUntil = user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_LOCK_UNTIL);
					if(lockUntil!=null)
					{
						Long unlockTime = Long.parseLong(lockUntil);
						Long currentTime = new Date().getTime();
						if(currentTime>unlockTime)
						{
							user.setAccountNonLocked(true);
							user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, "0");
							userServiceWithCache.unlockUser(user);
						}
					}
				}
				userServiceWithCache.clearUserCache();
			}
	}

}