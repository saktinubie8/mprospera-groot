package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.ParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dzulfiqar on 11/12/15.
 */
@Service("parameterService")
public class ParameterService extends GenericService {

	@Autowired
	private ParamManager paramManager;

	/**
	 * Get all parameters
	 * 
	 * @return list of Locations object
	 */

	public List<SystemParameter> getParameter() {
		return paramManager.getAll();
	}

	public List<PRSParameter> getPRSParameter() {
		return paramManager.getAllPRSParam();
	}

	public void updateParam(SystemParameter param, final String paramValue) throws Exception {
		paramManager.updateParam(param, paramValue);
	}

	public void updatePRSParam(PRSParameter param, final String paramValue, byte[] paramValueByte, String paramDesc) throws Exception {
		paramManager.updatePRSParam(param, paramValue, paramValueByte, paramDesc);
	}

	public SystemParameter loadParamByParamId(final Long paramId) throws UsernameNotFoundException {
		return paramManager.getParamByParamId(paramId);
	}

	public PRSParameter loadPrsParamByParamId(final Long paramId) throws UsernameNotFoundException {
		return paramManager.getPRSParamByParamId(paramId);
	}

	public String loadParamByParamName(final String paramName, final String defaultValue) {
		SystemParameter param = paramManager.getParamByParamName(paramName);
		if (param != null)
			return param.getParamValue();
		return defaultValue;
	}

	public Page<SystemParameter> searchParam(String keyword, String paramGroup,
			LinkedHashMap<String, String> columnMap, int finalOffset, int finalLimit) {
		// TODO Auto-generated method stub
		return paramManager.searchParam(keyword, paramGroup, columnMap, finalOffset, finalLimit);
	}

	public PRSParameter findByParameterId(Long parameterId) {
		return paramManager.findByParameterId(parameterId);
	}

	public Page<PRSParameter> searchPrsParam(String keyword, String paramGroup,
			LinkedHashMap<String, String> columnMap, int finalOffset, int finalLimit) {
		// TODO Auto-generated method stub
		return paramManager.searchPRSParam(keyword, paramGroup, columnMap, finalOffset, finalLimit);
	}

	public void insertPrsParam(PRSParameter prsparam) {
		paramManager.insertPRSParam(prsparam);
	}
	
	public void deletePrsParam(Long parameterId){
		paramManager.deleteByParameterId(parameterId);
	}

}