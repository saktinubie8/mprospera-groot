package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchJobExecution;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchStepExecution;
import org.springframework.data.domain.Page;

import java.util.Date;

public interface SpringBatchManager {

	Page<BatchJobExecution> findJobExecution(int offset, int limit);

	Page<BatchJobExecution> findJobExecution(Date startDate, Date endDate,
			int offset, int limit);

	Page<BatchStepExecution> findStepExecution(Long jobExecutionId, int offset,
			int limit);

}