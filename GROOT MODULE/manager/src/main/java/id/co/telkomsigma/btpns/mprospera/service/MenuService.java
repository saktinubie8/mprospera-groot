package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.MenuManager;
import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("menuService")
public class MenuService extends GenericService {

    @Autowired
    private MenuManager menuMgr;

    public List<Menu> getMenuByRole(final Long roleId) {
        return menuMgr.getListMenuId(roleId);
    }

    public List<Menu> getAllMenus() {
        List<Menu> retVal = new ArrayList<>();
        for (Menu menu : menuMgr.getAll()) {
            if (!menu.getMenuPath().contains("LABEL"))
                retVal.add(menu);
        }
        return retVal;
    }

    public Menu getMenuByMenuId(Long menuId) {
        return menuMgr.getMenuByMenuId(menuId);
    }

}