package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.manager.EODManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service("eodManager")
public class EODManagerImpl implements EODManager {

    @Override
    @CacheEvict(allEntries = true, value = { "eod", "lock"}, beforeInvocation = true)
    public void clearCache() {

    }

}