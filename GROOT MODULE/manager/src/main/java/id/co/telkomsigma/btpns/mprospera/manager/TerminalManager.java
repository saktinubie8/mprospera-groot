package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;
import java.util.List;

public interface TerminalManager {

    Terminal updateTerminal(Terminal terminal);

    List<Terminal> getAllTerminal();

    Page<Terminal> searchTerminals(String keyword, LinkedHashMap<String, String> orderMap, int offset, int limit);

    Page<TerminalActivity> searchTerminalActivity(String keyword, Long terminalId, String startedDate, String endedDate, LinkedHashMap<String, String> orderMap, int offset, int limit);

    void clearCache();

	Terminal getTerminalByImei(String imei);

}