package id.co.telkomsigma.btpns.mprospera.manager.impl;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import id.co.telkomsigma.btpns.mprospera.dao.MailParamDao;
import id.co.telkomsigma.btpns.mprospera.manager.MailParamManager;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;

@Service("mailParamManager")
public class MailParamManagerImpl implements MailParamManager {

    @Autowired
    private MailParamDao mailParamDao;

    @Override
    public List<MailParameter> getAll() {
        return mailParamDao.findAll();
    }

    @Override
    public Page<MailParameter> searchParam(String keyword, LinkedHashMap<String, String> orderMap, int offset, int limit) {
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
        PageRequest pageRequest = new PageRequest((offset / limit), limit);
        return mailParamDao.findByMessageParamLikeOrMessageValueLikeAllIgnoreCaseOrderByMessageParamAsc(keyword, keyword, pageRequest);
    }

    @Override
    public MailParameter updateParam(MailParameter parameter, String messageValue) {
        MailParameter result = mailParamDao.findById(parameter.getId());
        result.setMessageValue(messageValue);
        mailParamDao.save(result);
        return result;
    }

    @Transactional
    @Override
    public MailParameter getParamById(Long id) {
        return mailParamDao.findById(id);
    }

    @Override
    public MailParameter getParamByName(String messageParameter) {
        MailParameter param = mailParamDao.findByMessageParam(messageParameter);
        return param;
    }

    @Override
    public void clearCache() {

    }

}