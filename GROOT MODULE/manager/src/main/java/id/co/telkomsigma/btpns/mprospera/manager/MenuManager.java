package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;

import java.util.List;

public interface MenuManager {

	List<Menu> getAll();
	
	List<Menu> getListMenuId(Long roleId);

	Menu getMenuByMenuId(Long menuId);
	
	void clearCache();

}