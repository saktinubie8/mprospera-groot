package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.pojo.Ap3rData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Service("loanManager")
public class LoanManagerImpl implements LoanManager {
	
    @PersistenceContext
    private EntityManager em;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	private String ap3rData = "select acc.id as loan_id, acc.sw_id as sw_id, acc.application_id as application_id, " + 
			"acc.disbursement_date as disbursement_date, " + 
			"loc.code as mms_location_code, loc.name as mms_name, loc.parent_location as parent_location " + 
			"from t_loan_account acc " + 
			"inner join t_customer cust on acc.customer_id = cust.id " + 
			"inner join t_sentra_group sng on cust.group_id = sng.id " + 
			"inner join t_sentra snt on sng.sentra_id = snt.id " + 
			"inner join m_users usr on snt.assign_to = usr.user_login " + 
			"inner join m_location loc on usr.location_id = loc.id " +  
			"where acc.status=:status and acc.disbursement_date between :startDate and :endDate order by loc.name";

	@Autowired
	private LoanDao loanDao;
	
	@Override
	public void save(Loan loan) {
		loanDao.save(loan);
	}

	@Override
	public Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId) {
		// TODO Auto-generated method stub
		return loanDao.findByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE), officeId);
	}

	@Override
	public Page<Loan> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId) {
		// TODO Auto-generated method stub
		return loanDao.findByCreatedDate(startDate, endDate, pageRequest, officeId);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub

	}

	@Override
	public Loan findById(long parseLong) {
		// TODO Auto-generated method stub
		return loanDao.findOne(parseLong);
	}

	@Override
	public List<Loan> findLoanByDisbursementDate(Date startDate, Date endDate) {
		return loanDao.findByDisbursementDateAndStatus(startDate,endDate, WebGuiConstant.STATUS_APPROVED);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Ap3rData> findLoanByDisbursementDateManual(Date startDate, Date endDate, String status) {
		Query q = this.em.createNativeQuery(ap3rData,"Ap3rData");
		q.setParameter("status", status);
		q.setParameter("startDate", sdf.format(startDate));
		q.setParameter("endDate", sdf.format(endDate));
		List<Ap3rData> listAp3r = q.getResultList();
		em.close();
		return listAp3r;
	}

}