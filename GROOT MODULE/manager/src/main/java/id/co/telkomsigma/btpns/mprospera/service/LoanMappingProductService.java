package id.co.telkomsigma.btpns.mprospera.service;

import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.manager.LoanProductManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;

@Service("LoanMappingProductService")
public class LoanMappingProductService extends GenericService{
    @Autowired
    LoanProductManager loanProductManager;

    public LoanProduct getRoleById(Long productId) {
        return loanProductManager.findById(productId);
    }
    
    //detailMapping
//    public List<DetailMappingProduct> getDetailMappingProduct(){
//    	return loanProductManager.getDetailMappingProduct();
//    }
    //ProductLoan
    public List<LoanProduct> getLoanProduct(){
    	return loanProductManager.getAllLoanProduct();
    }
    
    //list tampilkan data
    public Page<LoanProduct> search(String keyword,
			LinkedHashMap<String, String> sbOrder, int finalOffset, int limit){
    	return loanProductManager.searchLoanMappingproduct(keyword, sbOrder, finalOffset, limit);
    	
    }
    //List Tampilkan Data search detailMappingProduct
//    public Page<DetailMappingProduct> searchMapping(String keyword,
//			LinkedHashMap<String, String> sbOrder, int finalOffset, int limit){
//    	return loanProductManager.searchMappingProduct(keyword, sbOrder, finalOffset, limit);
//    }
    public LoanProduct createLoanProduct(LoanProduct loanProduct) {
    	return loanProductManager.insertLoanProduct(loanProduct);
    }
    
    public LoanProduct laodUserByLoanproductId(final Long productId) {
    	return loanProductManager.getLoanProductById(productId);
    }
    
    
    //loadUserByProduct
    public LoanProduct loadUserByproductId(final Long productId) {
    	return loanProductManager.getLoanProductById(productId);
    }
    
    public LoanProduct updateLoanProduct(LoanProduct loanProduct) 
    {
    	return loanProductManager.updateLoanProduct(loanProduct);
    }
}
