package id.co.telkomsigma.btpns.mprospera.manager;

import java.util.LinkedHashMap;

import org.springframework.data.domain.Page;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.pojo.CustomerIDPhoto;

public interface CustomerManager {

    void clearCache();

    Customer findById(long parseLong);

    Page<CustomerIDPhoto> searchCustomerByCiffOrIdNumber(String keyword, LinkedHashMap<String, String> orderMap,
                                                         int offset, int limit);

}