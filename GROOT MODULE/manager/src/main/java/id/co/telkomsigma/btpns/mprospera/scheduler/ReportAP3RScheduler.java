package id.co.telkomsigma.btpns.mprospera.scheduler;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Scanner;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.service.Ap3rService;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;

/**
 * Created by Dzulfiqar on 8/05/2017.
 */
@Component
public class ReportAP3RScheduler {

	@Autowired
	private Ap3rService ap3rService;

	@Autowired
	private AuditLogService auditLogService;

	private static Boolean v1Enable = false;
	private static Boolean v2Enable = false;

	protected final Log LOGGER = LogFactory.getLog(getClass());

	/*
	 * Edited by : Kusnendi 19/11/2019
	 * Perubahan : Merubah deteksi ip untuk node yang dipakai scheduller menjadi hostname
	 */
	@Scheduled(cron = WebGuiConstant.CRON_AP3R)
	public void runReportAp3r() {
		if (v1Enable) {
			try {
				String currentHostName = System.getProperty(WebGuiConstant.IP_AP3R).trim();
//			String ip = getIpAddress();
				String path =  System.getProperty(WebGuiConstant.GENERATE_AP3R_PATH1).trim();
				String hostName = getHostName();
				String daysProperty = null == System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS) ? "0"
						: System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS).trim();

				LOGGER.info("AP3R Scheduler V1 running.. on " + System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R)
						+ " for " + currentHostName + " in " + hostName);
				LOGGER.info("hostname : " + hostName);
				LOGGER.info("currentHostName : " + currentHostName);
				String a = WebGuiConstant.GENERATE_AP3R_PATH1;
				LOGGER.info(a);
				if (hostName.equals(currentHostName) || currentHostName.equalsIgnoreCase("all")) {

				long start = System.currentTimeMillis();
				LOGGER.info("AP3R Scheduler V1 running.....START");
				auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R + "..START"));
				ap3rService.setDays(new Integer(daysProperty).intValue());
				ap3rService.generateAp3r(null, null,path);

				long duration = System.currentTimeMillis() - start;
				LOGGER.info("AP3R Scheduler V1 running.....FINISH (" + duration + " ms)");
				auditLogService.insertAuditLog(
						generateAuditLog(WebGuiConstant.GENERATE_AP3R + "..FINISH (" + duration + " ms)"));
				 }
			} catch (Exception e) {
				LOGGER.error("error runReportAp3r: " + e.getMessage());
			}
		}
	}

	@Scheduled(cron = WebGuiConstant.CRON_AP3R)
	public void runReportAp3rV2() {
		if (v2Enable) {
			try {
				String currentHostName = System.getProperty(WebGuiConstant.IP_AP3R).trim();
				String path =  System.getProperty(WebGuiConstant.GENERATE_AP3R_PATH2).trim();
//				String ip = getIpAddress();
				String hostName = getHostName();
				String daysProperty = null == System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS) ? "0"
						: System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS).trim();

				LOGGER.info("AP3R Scheduler V2 running.. on " + System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R)
						+ " for " + currentHostName + " in " + hostName);
				LOGGER.info("hostname : " + hostName);
				LOGGER.info("currentHostName : " + currentHostName);
				String a = WebGuiConstant.GENERATE_AP3R_PATH2;
				LOGGER.info(a);
				if (hostName.equals(currentHostName) || currentHostName.equalsIgnoreCase("all")) {
					long start = System.currentTimeMillis();
					LOGGER.info("AP3R Scheduler V2 running.....START");
					auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R + "..START"));
					ap3rService.setDays(new Integer(daysProperty).intValue());
					// ap3rService.generateAp3r(null,null);
					ap3rService.getAp3rReport(null, null,path);

					long duration = System.currentTimeMillis() - start;
					LOGGER.info("AP3R Scheduler V2 running.....FINISH (" + duration + " ms)");
					auditLogService.insertAuditLog(
						generateAuditLog(WebGuiConstant.GENERATE_AP3R + "..FINISH (" + duration + " ms)"));
				}
			} catch (Exception e) {
				LOGGER.error("error runReportAp3r: " + e.getMessage());
			}
		}
	}

	private String getIpAddress() {
		String ip = "";
		try {
			ip = InetAddress.getLocalHost().getHostAddress();

			String interfaceName = "eth0";
			NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
			Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
			InetAddress currentAddress;
			currentAddress = inetAddress.nextElement();
			while (inetAddress.hasMoreElements()) {
				currentAddress = inetAddress.nextElement();
				if (currentAddress instanceof InetAddress && !currentAddress.isLoopbackAddress()) {
					ip = currentAddress.toString();
					break;
				}
			}
			
			if(ip.equals("")) {
				final NetworkInterface netInf = NetworkInterface.getNetworkInterfaces().nextElement();
				Enumeration<InetAddress> addresses = netInf.getInetAddresses();
				
				ip = addresses.nextElement().getHostAddress();
			}

		} catch (Exception e) {
			LOGGER.error("error: "+e.getMessage());
		}

		return ip.replace("/", "");
	}
	
	private String getHostName() {
		String os = System.getProperty("os.name").toLowerCase();
		String hostname;
		if(os.contains("win")) {
			hostname = System.getenv("COMPUTERNAME") == null ? executeToString("hostname"): System.getenv("COMPUTERNAME");
		}else {
			hostname = System.getenv("HOSTNAME") == null ? executeToString("hostname"): System.getenv("HOSTNAME");
		}
		return hostname;
	}
	
	private String executeToString(String command) {
		String result = null;
		try {
			Scanner scan = new Scanner(Runtime.getRuntime().exec(command).getInputStream());
			scan.useDelimiter("\\A");
			result = scan.hasNext() ? scan.next() : "";
			scan.close();
		}catch(IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	private static AuditLog generateAuditLog(String description) {
		String reffNo = new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date());
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(AuditLog.GENERATE_REPORT);
		auditLog.setCreatedBy("Mprospera");
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription(description);
		auditLog.setReffNo(reffNo);
		return auditLog;
	}

	@Value(WebGuiConstant.VERSION_AP3R)
	public void generateAp3rVersioning(String version) {
		version.trim();
		v1Enable = version.contains("v1")?true:false;
		v2Enable = version.contains("v2")?true:false;
	}

}
