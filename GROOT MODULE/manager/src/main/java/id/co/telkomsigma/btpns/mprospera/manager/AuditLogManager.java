package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;
import java.util.List;

public interface AuditLogManager {
	
	 List<AuditLog> getAll();

	 AuditLog insertAuditLog(AuditLog auditLog);
	 
	 Page<AuditLog> searchAuditLog(String keyword, String startDate, String endDate, String activityType, LinkedHashMap<String, String> orderMap, int offset, int limit);

}