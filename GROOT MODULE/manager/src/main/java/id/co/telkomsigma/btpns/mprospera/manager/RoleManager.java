package id.co.telkomsigma.btpns.mprospera.manager;


import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;
import java.util.List;


/**
 * Created by daniel on 4/16/15.
 */
public interface RoleManager {
    /**
     * Get all roles
     * @return list of Role object
     */
    List<Role> getAll();

    //get role by Id, return Role
    Role getRoleById(Long roleId);
        
    Page<Role> searchRoles(String authority, LinkedHashMap<String, String> orderMap, int offset, int limit);
    
    Role insertRole(Role role);
    
    Role updateRole(Role role);

	void clearCache();
  
}