package id.co.telkomsigma.btpns.mprospera.request;

public class SyncAreaRequest extends BaseRequest {
	private String username;
	private String sessionKey;
	private String imei;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}
}
