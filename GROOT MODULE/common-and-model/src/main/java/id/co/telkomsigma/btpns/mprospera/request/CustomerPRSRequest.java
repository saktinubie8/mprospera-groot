package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CustomerPRSRequest {

	private String customerId;
	private String customerPrsId;
	private String isAttend;
	private String notAttendReason;
	private List<LoanPRSRequest> loanList;
	private List<SavingPRSRequest> savingList;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getIsAttend() {
		return isAttend;
	}

	public void setIsAttend(String isAttend) {
		this.isAttend = isAttend;
	}

	public String getNotAttendReason() {
		return notAttendReason;
	}

	public void setNotAttendReason(String notAttendReason) {
		this.notAttendReason = notAttendReason;
	}

	public List<LoanPRSRequest> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<LoanPRSRequest> loanList) {
		this.loanList = loanList;
	}

	public List<SavingPRSRequest> getSavingList() {
		return savingList;
	}

	public void setSavingList(List<SavingPRSRequest> savingList) {
		this.savingList = savingList;
	}

	public String getCustomerPrsId() {
		return customerPrsId;
	}

	public void setCustomerPrsId(String customerPrsId) {
		this.customerPrsId = customerPrsId;
	}

	@Override
	public String toString() {
		return "CustomerPRSRequest [customerId=" + customerId + ", customerPrsId=" + customerPrsId + ", isAttend="
				+ isAttend + ", notAttendReason=" + notAttendReason + ", loanList=" + loanList + ", savingList="
				+ savingList + ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()="
				+ super.toString() + "]";
	}

}
