package id.co.telkomsigma.btpns.mprospera.pojo;

import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

@SqlResultSetMapping(name = "CustomerIDPhoto", entities = {
        @EntityResult(entityClass = CustomerIDPhoto.class, fields = {
                @FieldResult(name = "customerId", column = "customer_id"),
                @FieldResult(name = "customerCifNumber", column = "cif_number"),
                @FieldResult(name = "customerIdNumber", column = "id_number"),
                @FieldResult(name = "customerName", column = "name"),
                @FieldResult(name = "dateOfBirth", column = "date_of_birth"),
                @FieldResult(name = "idPhoto", column = "id_photo"),
        })})
@Entity
public class CustomerIDPhoto {

    @Id
    private Long customerId;
    private String customerCifNumber;
    private String customerIdNumber;
    private String customerName;
    private Date dateOfBirth;
    private byte[] idPhoto;

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getCustomerCifNumber() {
        return customerCifNumber;
    }

    public void setCustomerCifNumber(String customerCifNumber) {
        this.customerCifNumber = customerCifNumber;
    }

    public String getCustomerIdNumber() {
        return customerIdNumber;
    }

    public void setCustomerIdNumber(String customerIdNumber) {
        this.customerIdNumber = customerIdNumber;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public byte[] getIdPhoto() {
        return idPhoto;
    }

    public void setIdPhoto(byte[] idPhoto) {
        this.idPhoto = idPhoto;
    }

}