package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class UserPSResponse extends BaseResponse{

	List<SentraListResponse> sentraList;

	public List<SentraListResponse> getSentraList() {
		return sentraList;
	}

	public void setSentraList(List<SentraListResponse> sentraList) {
		this.sentraList = sentraList;
	}


}
