package id.co.telkomsigma.btpns.mprospera.constant;

/**
 * Created by daniel on 4/17/15.
 */
public final class WebGuiConstant {

    /**
     * URL MENU Clear Cache
     */
    public static final String PATH_CACHE_CLEAR_ALL = "/webservice/clearAllCache";

    /**
     * URL MENU Clear Session Key
     */
    public static final String PATH_CACHE_CLEAR_SESSION_KEY = "/webservice/clearSessionKey";

    /**
     * URL CAPTCHA
     */
    private static final String CAPTCHA_PATH = "/captcha";
    public static final String CAPTCHA_REGISTRATION_PATH_MAPPING = CAPTCHA_PATH + "-registration*";
    public static final String CAPTCHA_RESEND_ACTIVATION_LINK_PATH_MAPPING = CAPTCHA_PATH + "-resend-activation*";
    public static final String CAPTCHA_RESET_PASSWORD_PATH_MAPPING = CAPTCHA_PATH + "-reset-password*";
    public static final String CAPTCHA_PATH_SECURITY_MAPPING = CAPTCHA_PATH + "**";

    /**
     * URL GLOBAL
     */
    private static final String AFTER_LOGIN_PATH = "/home";
    public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";
    public static final String AFTER_LOGIN_PATH_HOME_PAGE = "/home";
    public static final String AFTER_LOGIN_PATH_SECURITY_MAPPING = AFTER_LOGIN_PATH + "/**";
    public static final String AFTER_LOGIN_PATH_PROFILE_PAGE = AFTER_LOGIN_PATH + "/profile";
    public static final String AFTER_LOGIN_PATH_PARAM_PAGE = "/home/param";
    public static final String AFTER_LOGIN_PATH_HOME_GET_LIST_USER = AFTER_LOGIN_PATH + "/list*";

    /**
     * URL DASHBOARD EOD
     */
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION = AFTER_LOGIN_PATH
            + "/manage/dashboard/jobExecution";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION
            + "*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_RERUN_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION
            + "/rerun*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_GET_LIST_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION
            + "/get-list-jobexec*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION = AFTER_LOGIN_PATH
            + "/manage/dashboard/jobExecution/stepExecution";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION
            + "*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_REQUEST = AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION
            + "?jobExecutionId={jobExecutionId}";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_GET_LIST_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION
            + "/get-list-stepexec*";

    /**
     * URL AUDIT LOG
     */
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG = AFTER_LOGIN_PATH + "/auditLog";
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "*";
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG_GET_LIST_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "/get-list*";
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG_SEARCH_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "/search*";

    /**
     * URL USER
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_USER = AFTER_LOGIN_PATH + "/manage/user";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD = AFTER_LOGIN_PATH_MANAGE_USER + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ADD + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_ADD
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK = AFTER_LOGIN_PATH_MANAGE_USER + "/lock";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_LOCK + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_LOCK
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK = AFTER_LOGIN_PATH_MANAGE_USER + "/unlock";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE = AFTER_LOGIN_PATH_MANAGE_USER + "/enable";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ENABLE + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_ENABLE
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE = AFTER_LOGIN_PATH_MANAGE_USER + "/disable";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_DISABLE
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_DISABLE
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET = AFTER_LOGIN_PATH_MANAGE_USER + "/reset";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_RESET + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_RESET
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION = AFTER_LOGIN_PATH_MANAGE_USER
            + "/clearSession";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION
            + "?userId={userId}";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_AJAX_SEARCH_LOCATION_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ADD
            + "/ajax/location*";

    /**
     * URL PARAMETER
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM = AFTER_LOGIN_PATH + "/manage/parameter";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT = AFTER_LOGIN_PATH_MANAGE_PARAM + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT
            + "?paramId={paramId}";

    public static final String PARAMETER_NAME_MAX_FAILED_ATTEMPT = "security.max.failedAttempt";
    public static final String PARAMETER_MAX_FAILED_ATTEMPT_DEFAULT_VALUE = "5";

    public static final String PARAMETER_NAME_LOCK_DURATION = "security.lock.duration";
    public static final String PARAMETER_LOCK_DURATION_DEFAULT_VALUE = "30";

    // custom parameter di tabel user kolom raw
    public static final String PARAMETER_USER_CURRENT_FAILED_ATTEMPT = "current.failedAttempt";
    public static final String PARAMETER_USER_CURRENT_LOCK_UNTIL = "lock.until";

    public static final String PARAMETER_NAME_MAX_GPS_ACCURACT = "android.max.gpsAccuracy";
    public static final String PARAMETER_NAME_MAX_GPS_ACCURACT_DEFAULT_VALUE = "200";

    /**
     * URL PRS PARAMETER
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM = AFTER_LOGIN_PATH + "/manage/prsparam";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT
            + "?prsParamId={prsParamId}";

    private static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD
            + "?parameterId={parameterId}";

    private static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM
            + "/delete";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE_REQUEST = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE
            + "?parameterId={parameterId}";

    /**
     * URL ROLE
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE = AFTER_LOGIN_PATH + "/manage/role";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD = AFTER_LOGIN_PATH_MANAGE_ROLE + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE_ADD + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_ROLE_ADD
            + "?roleId={roleId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT = AFTER_LOGIN_PATH_MANAGE_ROLE + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT
            + "?u={rolename}";

    /**
     * URL Mapping Product
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT = AFTER_LOGIN_PATH + "/manage/mappingproduct";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRODUKMAPPING_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRODUKMAPPING_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD
            + "?productId={productId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_EDIT = AFTER_LOGIN_PATH_MANAGE_ROLE + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_EDIT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_REQUEST = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_EDIT
            + "?p={productId}";

    /**
     * URL TERMINAL
     */
    public static final String AFTER_LOGIN_PATH_TERMINAL = AFTER_LOGIN_PATH + "/terminal";
    public static final String AFTER_LOGIN_PATH_TERMINAL_MAPPING = AFTER_LOGIN_PATH_TERMINAL + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_GET_LIST_MAPPING = AFTER_LOGIN_PATH_TERMINAL
            + "/get-list-terminal*";

    private static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS = AFTER_LOGIN_PATH_TERMINAL + "/show-details";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_REQUEST = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "?terminalId={terminalId}";

    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_GET_LIST_DETAILS_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL
            + "/get-list-terminal-details*";

    private static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "/show-message-details";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE
            + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE_REQUEST = AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE
            + "?terminalActivityId={terminalActivityId}&date={date}";

    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_GET_MESSAGE_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_GET_LIST_MESSAGE_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "/get-list-message-details*";

    /**
     * URL ADMIN
     */
    private static final String ADMIN_AFTER_LOGIN_PATH = "/admin";
    public static final String ADMIN_AFTER_LOGIN_PATH_HOME_PAGE = ADMIN_AFTER_LOGIN_PATH + "/home";
    public static final String ADMIN_AFTER_LOGIN_PATH_SECURITY_MAPPING = ADMIN_AFTER_LOGIN_PATH + "/**";

    public static final String ADMIN_AFTER_LOGIN_PATH_PROFILE_PAGE = ADMIN_AFTER_LOGIN_PATH + "/profile";

    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER = ADMIN_AFTER_LOGIN_PATH + "/manage/user";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER + "*";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_GET_LIST_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER
            + "/get-list*";
    private static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_ADD = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER + "/add";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_ADD_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_ADD
            + "*";
    private static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER + "/edit";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT
            + "*";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT_REQUEST = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT
            + "?u={username}";

    /**
     * SESSION CONSTANT
     */
    public static final String SESSION_REGISTERED_USER = "registeredUser";
    public static final String SESSION_RESEND_ACTIVATION_LINK_USER = "resendActivationLinkUser";
    public static final String SESSION_REQUEST_RESET_PASSWORD_USER = "requestResetPasswordUser";
    public static final String SESSION_RESET_PASSWORD_USER = "resetPasswordUser";

    public static final String SESSION_CAPTCHA_REGISTRATION = "registeredUser";
    public static final String SESSION_CAPTCHA_REQUEST_RESET_PASSWORD = "requestResetPassword";
    public static final String SESSION_CAPTCHA_RESEND_ACTIVATION_LINK = "resendActivationLink";

    /**
     * URL Cache
     */
    public static final String AFTER_LOGIN_PATH_CACHE = AFTER_LOGIN_PATH + "/manage/cache";

    /**
     * URL Kelola APK
     */
    public static final String AFTER_LOGIN_PATH_APK = AFTER_LOGIN_PATH + "/manage/apk";
    public static final String AFTER_LOGIN_PATH_APK_MAPPING = AFTER_LOGIN_PATH_APK + "*";
    public static final String AFTER_LOGIN_PATH_APK_GET_LIST_MAPPING = AFTER_LOGIN_PATH_APK + "/get-list-apk*";

    public static final String AFTER_LOGIN_PATH_APK_GET_APK = AFTER_LOGIN_PATH_APK + "/get-apk";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_MAPPING = AFTER_LOGIN_PATH_APK_GET_APK + "*";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_REQUEST = AFTER_LOGIN_PATH_APK_GET_APK + "?apkId={apkId}";

    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_OK = AFTER_LOGIN_PATH_APK + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_OK_MAPPING = AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_OK
            + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_ERROR = AFTER_LOGIN_PATH_APK + "/progress/error";
    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_ERROR_MAPPING = AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_ERROR
            + "*";

    private static final String AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED = AFTER_LOGIN_PATH_APK + "/notAllowed";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED_MAPPING = AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED_REQUEST = AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED
            + "?apkId={apkId}";

    private static final String AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED = AFTER_LOGIN_PATH_APK + "/allowed";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED_MAPPING = AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED_REQUEST = AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED
            + "?apkId={apkId}";

    private static final String AFTER_LOGIN_PATH_PUBLIC = "/apk";

    public static final String AFTER_LOGIN_PATH_APK_GET_APK_OUTSIDE = AFTER_LOGIN_PATH_PUBLIC + "/get-apk";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_MAPPING_OUTSIDE = AFTER_LOGIN_PATH_APK_GET_APK_OUTSIDE
            + "*";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_REQUEST_OUTSIDE = AFTER_LOGIN_PATH_APK_GET_APK_OUTSIDE
            + "?version={version:.+}";

    /**
     * URL REPORT
     */
    public static final String AFTER_LOGIN_PATH_REPORT = AFTER_LOGIN_PATH + "/report";
    public static final String AFTER_LOGIN_PATH_REPORT_MAPPING = AFTER_LOGIN_PATH_REPORT + "*";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_LIST_MAPPING = AFTER_LOGIN_PATH_REPORT + "/get-list-report*";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_PDF = AFTER_LOGIN_PATH_REPORT + "/get-pdf";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_PDF_MAPPING = AFTER_LOGIN_PATH_REPORT_GET_PDF + "*";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_PDF_REQUEST = AFTER_LOGIN_PATH_REPORT_GET_PDF
            + "?downloadId={downloadId}";

    /**
     * URL WebService Inquery PRS Rekap MS
     */
    public static final String TERMINAL_INQUERY_PRS = "/webservice/inquery/prs**";
    public static final String TERMINAL_INQUERY_PRS_REQUEST = TERMINAL_INQUERY_PRS + "/{apkVersion:.+}";

    public static final String AFTER_LOGIN_PATH_GENERATE_RMK_REPORT = AFTER_LOGIN_PATH + "/manage/report/generateReportRmk";
    public static final String AFTER_LOGIN_PATH_GENERATE_RMK_REPORT_MAPPING = AFTER_LOGIN_PATH_GENERATE_RMK_REPORT + "*";
    public static final String AFTER_LOGIN_PATH_LIST_GENERATE_REPORT_MAPPING = AFTER_LOGIN_PATH + "/manage/report*";
    public static final String AFTER_LOGIN_PATH_GENERATE_RMK_REPORT_AJAX_MAPPING = AFTER_LOGIN_PATH_GENERATE_RMK_REPORT
            + "/ajaxGetMms*";

    public static final String AFTER_LOGIN_PATH_GENERATE_DMK_REPORT = AFTER_LOGIN_PATH + "/manage/report/generateReportDmk";
    public static final String AFTER_LOGIN_PATH_GENERATE_DMK_REPORT_MAPPING = AFTER_LOGIN_PATH_GENERATE_DMK_REPORT + "*";
    public static final String AFTER_LOGIN_PATH_LIST_GENERATE_DMK_REPORT_MAPPING = AFTER_LOGIN_PATH + "/manage/report*";
    public static final String AFTER_LOGIN_PATH_GENERATE_DMK_REPORT_AJAX_MAPPING = AFTER_LOGIN_PATH_GENERATE_DMK_REPORT
            + "/ajaxGetMms*";

    /**
     * URL IDCard Search
     */
    public static final String AFTER_LOGIN_PATH_IDCARD_SEARCH = AFTER_LOGIN_PATH + "/manage/idcardsearch";
    public static final String AFTER_LOGIN_PATH_IDCARD_SEARCH_MAPPING = AFTER_LOGIN_PATH_IDCARD_SEARCH + "*";
    public static final String AFTER_LOGIN_PATH_IDCARD_SEARCH_GET_LIST_MAPPING = AFTER_LOGIN_PATH_IDCARD_SEARCH + "/get-list*";
    public static final String AFTER_LOGIN_PATH_IDCARD_SEARCH_VIEW = AFTER_LOGIN_PATH_IDCARD_SEARCH + "/view";
    public static final String AFTER_LOGIN_PATH_IDCARD_SERACH_VIEW_MAPPING = AFTER_LOGIN_PATH_IDCARD_SEARCH_VIEW + "*";
    public static final String AFTER_LOGIN_PATH_IDCARD_SEARCH_VIEW_REQUEST = AFTER_LOGIN_PATH_IDCARD_SEARCH_VIEW + "?customerId={customerId}";

    /**
     * MENU
     */
    public static final String HEADER_AFTER_LOGIN_PATH = "/header";

    /**
     * OTHER CONSTANT
     */
    public static final long DEFAULT_VALUE_COUNTRY_CODE = 94;

    /**
     * AP3R REPORT SCHEDULER CONSTANT
     */
    public static final String GENERATE_AP3R = "Generate AP3R Report";
    public static final String CRON_AP3R = "${ap3r.cron.scheduler}";
    public static final String IP_AP3R = "ap3r.cron.ip.running";
    public static final String SYSTEM_PROPERTY_AP3R = "ap3r.cron.scheduler";
    public static final String SYSTEM_PROPERTY_AP3R_DAYS = "ap3r.cron.days";
    public static final String GENERATE_AP3R_PATH1 = "ap3r.default.path1";
    public static final String GENERATE_AP3R_PATH2 = "ap3r.default.path2";
    public static final String VERSION_AP3R = "${ap3r.running.version}";

    /**
     * RESPONSE CODE
     */
    public static final String RC_SUCCESS = "00";
    public static final String RC_FAILED_LOGIN = "01";
    public static final String RC_RESET_PASSWORD_FAILED = "02";
    public static final String RC_USER_DISABLE = "03";
    public static final String RC_TERMINAL_NOT_FOUND = "04";
    public static final String RC_GLOBAL_EXCEPTION = "05";
    public static final String RC_ERROR_SCORING = "06";
    public static final String RC_ROLE_CANNOT_ACCESS_MOBILE = "07";
    public static final String RC_INVALID_SDA_CATEGORY = "08";
    public static final String RC_KFO_CUT_OFF = "09";
    public static final String RC_CREATE_SW_FAILED = "10";
    public static final String RC_SW_ALREADY_EXIST = "11";
    public static final String RC_DELETE_SW_FAILED = "12";
    public static final String RC_CIF_NOT_FOUND = "13";
    public static final String RC_UPDATE_SW_FAILED = "14";
    public static final String RC_INVALID_APK = "15";
    public static final String RC_UNKNOWN_MM_ID = "16";
    public static final String RC_UNKNOWN_PM_ID = "17";
    public static final String RC_UNKNOWN_LOAN_ID = "18";
    public static final String RC_UNKNOWN_SURVEY_ID = "19";
    public static final String RC_CREATE_SENTRA_FAILED = "20";
    public static final String RC_SENTRA_ALREADY_EXIST = "21";
    public static final String RC_UPDATE_SENTRA_FAILED = "22";
    public static final String RC_SENTRA_ID_NOT_FOUND = "23";
    public static final String RC_ASSIGN_PS_ERROR = "24";
    public static final String RC_CREATE_GROUP_FAILED = "25";
    public static final String RC_GROUP_ALREADY_EXIST = "26";
    public static final String RC_DELETE_GROUP_FAILED = "27";
    public static final String RC_UPDATE_GROUP_FAILED = "28";
    public static final String RC_GROUP_ID_NOT_FOUND = "29";
    public static final String RC_CREATE_CUSTOMER_FAILED = "30";
    public static final String RC_CUSTOMER_ALREADY_EXIST = "31";
    public static final String RC_TRANSACTION_INQUIRY_ERROR = "32";
    public static final String RC_SENTRAGROUP_NOT_APPROVED = "33";
    public static final String RC_SENTRAGROUP_NULL = "34";
    public static final String RC_CREATE_LOAN_FAILED = "35";
    public static final String RC_LOAN_ALREADY_EXIST = "36";
    public static final String RC_DELETE_LOAN_FAILED = "37";
    public static final String RC_APPROVAL_DEVIATION_FAILED = "38";
    public static final String RC_CLAIM_INSURANCE_FAILED = "39";
    public static final String RC_SUBMIT_PRS_FAILED = "40";
    public static final String RC_INQUIRY_PRS_ERROR = "41";
    public static final String RC_ID_PHOTO_NULL = "42";
    public static final String RC_SURVEY_PHOTO_NULL = "43";
    public static final String RC_PRS_REJECTED = "44";
    public static final String RC_UNKNOWN_SAVING = "45";
    public static final String RC_NO_LOAN_HIST = "46";
    public static final String RC_NO_SAVING_HIST = "47";
    public static final String RC_INVALID_KECAMATAN = "48";
    public static final String RC_INVALID_KELURAHAN = "49";
    public static final String RC_SUBMIT_COLLECTION_FAILED = "50";
    public static final String RC_AP3R_ALREADY_EXIST = "51";
    public static final String RC_SW_PRODUCT_NULL = "52";
    public static final String RC_SW_APPROVED_FAILED = "53";
    public static final String RC_PHOTO_ALREADY_EXIST = "54";
    public static final String RC_USER_CANNOT_APPROVE = "55";
    public static final String RC_UNKNOWN_INSURANCE_CLAIM_ID = "56";
    public static final String RC_FILE_DAYA_NULL = "57";
    public static final String RC_EARLY_TERMINATION_PLAN_NULL = "58";
    public static final String RC_UNREGISTERED_USER = "59";
    public static final String RC_CUSTOMER_WAIT_APPROVED = "60";
    public static final String RC_LOCATION_ID_NOT_FOUND = "61";
    public static final String RC_APPID_NULL = "62";
    public static final String RC_GROUP_LEADER_NULL = "63";
    public static final String RC_INVALID_RRN = "64";
    public static final String RC_AP3R_NULL = "65";
    public static final String RC_DEVIATION_NOT_APPROVED = "66";
    public static final String RC_DEVIATION_NULL = "67";
    public static final String RC_SENTRA_PHOTO_NULL = "68";
    public static final String RC_SW_NOT_APPROVED = "69";
    public static final String RC_DEVIATION_CANNOT_DELETE = "70";
    public static final String RC_EOD_PROSPERA_IS_RUNNING = "91";
    public static final String RC_GENERATE_AP3R_FAILED = "A9";
    public static final String RC_ESB_UNAVAILABLE = "SU";
    public static final String RC_INVALID_SESSION_KEY = "B1";
    public static final String RC_UNKNOWN_PARENT_AREA = "B2";
    public static final String RC_UNKNOWN_SDA_ID = "B3";
    public static final String RC_UNKNOWN_SENTRA_ID = "B4";
    public static final String RC_UNKNOWN_PDK_ID = "B5";
    public static final String RC_UNKNOWN_SW_ID = "B6";
    public static final String RC_UNKNOWN_CUSTOMER_ID = "B7";
    public static final String RC_USERNAME_NOT_EXISTS = "B8";
    public static final String RC_IMEI_NOT_EXISTS = "B9";
    public static final String RC_SESSION_KEY_NOT_EXISTS = "C1";
    public static final String RC_IMEI_NOT_VALID_FORMAT = "C2";
    public static final String RC_USER_INVALID_CITY = "C3";
    public static final String RC_SENTRA_ALREADY_APPROVED = "C4";
    public static final String RC_UNKNOWN_STATUS = "C5";
    public static final String RC_ALREADY_CLOSED = "C6";
    public static final String RC_ALREADY_REJECTED = "C7";
    public static final String RC_ALREADY_CANCEL = "C8";
    public static final String RC_SENTRA_NOT_APPROVED = "C9";
    public static final String RC_UNKNOWN_APPID = "D1";
    public static final String RC_USERNAME_USED = "D2";
    public static final String RC_UNKNOWN_CIF_NUMBER = "D3";
    public static final String RC_PRS_NULL = "D4";
    public static final String RC_PRS_PHOTO_NULL = "D5";
    public static final String RC_PRS_ALREADY_SUBMIT = "D6";
    public static final String RC_UNKNOWN_LOAN_PRS = "D7";
    public static final String RC_AP3R_ALREADY_APPROVED = "D8";
    public static final String RC_ROLE_NULL = "D9";
    public static final String RC_SW_ALREADY_APPROVED = "E1";
    public static final String RC_RECORD_ALREADY_APPROVED = "E2";
    public static final String RC_SERVER_TIMEOUT = "ZZ";
    public static final String RC_INVALID_USER_PASSWORD = "1A";
    public static final String RC_USER_LOCKED = "1B";
    public static final String RC_USER_INACTIVE = "1C";
    public static final String RC_GENERAL_ERROR = "XX";

    public static final String DATE_FORMAT_ERROR = "field.error.invalid.date.format";
    public static final String PARAMETER_CURRENT_VERSION = "android.apk.version.current";
    public static final String PARAMETER_ALLOWED_VERSION = "android.apk.version.allowed";
    public static final String PARAMETER_DOWNLOAD_LINK = "android.apk.url.download";
    public static final String ENDPOINT_MPROSPERA = "MPROSPERA";

    /**
     * Constant STATUS
     */
    public static final String STATUS_DRAFT = "DRAFT";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_SUBMIT = "SUBMIT";
    public static final String STATUS_CLOSED = "CLOSED";
    public static final String STATUS_REJECTED = "REJECTED";
    public static final String STATUS_CANCEL = "CANCEL";
    public static final String STATUS_NA = "N/A";
    public static final String STATUS_DONE = "DONE";
    public static final String STATUS_APPROVED_MS = "APPROVED-MS";

    /**
     * URL UPLOAD
     */
    public static final String AFTER_LOGIN_PATH_UPLOAD = AFTER_LOGIN_PATH + "/upload";
    public static final String AFTER_LOGIN_PATH_UPLOAD_MAPPING = AFTER_LOGIN_PATH_UPLOAD + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_REQUEST = AFTER_LOGIN_PATH_UPLOAD + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_OK = AFTER_LOGIN_PATH_UPLOAD + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_ERROR = AFTER_LOGIN_PATH_UPLOAD + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_ERROR + "*";

    public static final String AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE = AFTER_LOGIN_PATH + "/uploadProductRate";
    public static final String AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE_MAPPING = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "*";
    public static final String AFTER_LOGIN_UPLOAD_PRODUCT_RATE_MANAGE_PRODUKMAPPING_GET_LIST_MAPPING = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE
            + "/get-list*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE_REQUEST = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_OK = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_ERROR = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_ERROR + "*";

    public static final String AFTER_LOGIN_PATH_UPLOAD_HOLIDAY = AFTER_LOGIN_PATH + "/uploadHoliday";
    public static final String AFTER_LOGIN_PATH_UPLOAD_HOLIDAY_MAPPING = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_HOLIDAY_REQUEST = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_OK = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_ERROR = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_ERROR + "*";

    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA = AFTER_LOGIN_PATH + "/uploadJenisUsaha";
    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_LIST = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "/list*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_MAPPING = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_REQUEST = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_OK = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_ERROR = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_ERROR + "*";

    /**
     * Mail Param Path
     */
    public static final String MAIL_EDITOR_PATH = AFTER_LOGIN_PATH + "/manage/maileditor";
    public static final String MAIL_EDITOR_PATH_MAPPING = MAIL_EDITOR_PATH + "*";
    public static final String MAIL_EDITOR_PATH_MAPPING_GET_LIST = MAIL_EDITOR_PATH + "/get-list*";
    public static final String MAIL_EDITOR_EDIT_PATH = MAIL_EDITOR_PATH + "/edit";
    public static final String MAIL_EDITOR_EDIT_PATH_REQUEST = MAIL_EDITOR_EDIT_PATH + "?id={paramId}";
    public static final String MAIL_EDITOR_EDIT_PATH_MAPPING = MAIL_EDITOR_EDIT_PATH + "*";

}