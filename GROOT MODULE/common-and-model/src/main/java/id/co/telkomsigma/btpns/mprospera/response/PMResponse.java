package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.ArrayList;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PMResponse extends BaseResponse {

	/* untuk addPM */
	private String pmId;

	/* untuk list PM */
	private String grandTotal;
	private String currentTotal;
	private String totalPage;
	private List<ListPMResponse> pmList;

	public String getPmId() {
		return pmId;
	}

	public void setPmId(String pmId) {
		this.pmId = pmId;
	}

	public String getGrandTotal() {
		return grandTotal;
	}

	public void setGrandTotal(String grandTotal) {
		this.grandTotal = grandTotal;
	}

	public String getCurrentTotal() {
		return currentTotal;
	}

	public void setCurrentTotal(String currentTotal) {
		this.currentTotal = currentTotal;
	}

	public String getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(String totalPage) {
		this.totalPage = totalPage;
	}

	public List<ListPMResponse> getPmList() {
		if (this.pmList == null)
			this.pmList = new ArrayList<>();
		return pmList;
	}

	public void setPmList(List<ListPMResponse> pmList) {
		this.pmList = pmList;
	}

}
