package id.co.telkomsigma.btpns.mprospera.model.parameter;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

@Entity
@Table(name = "M_MAIL_PARAMETER")
public class MailParameter extends GenericModel implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String messageParameter;
    private String messageDescription;
    private String messageValue;
//	private User createdBy;
//	private Date createdDate;
//	private User updatedBy;
//	private Date updatedDate;

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "MESSAGE_PARAM", length = MAX_LENGTH_PARAM_NAME, nullable = false)
    public String getMessageParam() {
        return messageParameter;
    }

    public void setMessageParam(String messageParam) {
        this.messageParameter = messageParam;
    }

    @Column(name = "MESSAGE_DESCRIPTION", length = MAX_LENGTH_DESCRIPTION, nullable = false)
    public String getMessageDescription() {
        return messageDescription;
    }

    public void setMessageDescription(String messageDescription) {
        this.messageDescription = messageDescription;
    }

    @Column(name = "MESSAGE_VALUE", length = MAX_LENGTH_PARAM_VALUE, nullable = false)
    public String getMessageValue() {
        return messageValue;
    }

    public void setMessageValue(String messageValue) {
        this.messageValue = messageValue;
    }

//	@ManyToOne(targetEntity = User.class)
//	@JoinColumn(name = "CREATED_BY", nullable = false)
//	public User getCreatedBy() {
//		return createdBy;
//	}
//	public void setCreatedBy(User createdBy) {
//		this.createdBy = createdBy;
//	}
//	
//	@Column(name = "CREATED_DT", nullable = false)
//	public Date getCreatedDate() {
//		return createdDate;
//	}
//	public void setCreatedDate(Date createdDate) {
//		this.createdDate = createdDate;
//	}
//	
//
//	@ManyToOne(targetEntity = User.class)
//	@JoinColumn(name = "UPDATE_BY", nullable = false)
//	public User getUpdatedBy() {
//		return updatedBy;
//	}
//	public void setUpdatedBy(User updatedBy) {
//		this.updatedBy = updatedBy;
//	}
//	
//	@Column(name = "UPDATE_DT", nullable = false)
//	public Date getUpdatedDate() {
//		return updatedDate;
//	}
//	public void setUpdatedDate(Date updatedDate) {
//		this.updatedDate = updatedDate;
//	}

}