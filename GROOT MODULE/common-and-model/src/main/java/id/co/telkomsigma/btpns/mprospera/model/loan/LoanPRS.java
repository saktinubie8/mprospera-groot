package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_LOAN_PRS")
public class LoanPRS extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3443750376719441652L;

	private Long loanPRSId;
	private Loan loanId;
	private CustomerPRS customerId;
	private String appId;
	private BigDecimal disbursementAmount;
	private char disbursementFlag;
	private Boolean canPendingDisbursement;
	private BigDecimal installmentAmount;
	private BigDecimal installmentPaymentAmount;
	private Boolean useEmergencyFund;
	private BigDecimal marginAmount;
	private BigDecimal plafonAmount;
	private BigDecimal outstandingAmount;
	private BigDecimal remainingPrincipal;;
	private Long outstandingCount;
	private Date dueDate;
	private Long overdueDays;
	private Boolean isEarlyTermination;
	private PRSParameter earlyTerminationReason;
	private Boolean marginDiscountDeviationFlag;
	private BigDecimal marginDiscountDeviationAmount;
	private BigDecimal marginDiscountDeviationPercentage;
	private Date createdDate;
	private String createdBy;
	private PRS prsId;
	private Date updatedDate;
	private String updatedBy;
	private BigDecimal currentMargin;
	private Boolean wowIbStatus;

	@Column(name = "OUTSTANDING_COUNT")
	public Long getOutstandingCount() {
		return outstandingCount;
	}

	public void setOutstandingCount(Long outstandingCount) {
		this.outstandingCount = outstandingCount;
	}

	@Column(name = "REMAINING_PRINCIPAL")
	public BigDecimal getRemainingPrincipal() {
		return remainingPrincipal;
	}

	public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
		this.remainingPrincipal = remainingPrincipal;
	}

	@Column(name = "CURRENT_MARGIN")
	public BigDecimal getCurrentMargin() {
		return currentMargin;
	}

	public void setCurrentMargin(BigDecimal currentMargin) {
		this.currentMargin = currentMargin;
	}

	@Id
	@Column(name = "ID", nullable = false, unique = true)
	@GeneratedValue
	public Long getLoanPRSId() {
		return loanPRSId;
	}

	public void setLoanPRSId(Long loanPRSid) {
		this.loanPRSId = loanPRSid;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Loan.class)
	@JoinColumn(name = "loan_Id", referencedColumnName = "id", nullable = true)
	public Loan getLoanId() {
		return loanId;
	}

	public void setLoanId(Loan loanId) {
		this.loanId = loanId;
	}

	@Column(name = "APP_ID", nullable = true)
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = CustomerPRS.class)
	@JoinColumn(name = "customer_Id", referencedColumnName = "id", nullable = true)
	public CustomerPRS getCustomerId() {
		return customerId;
	}

	public void setCustomerId(CustomerPRS customerId) {
		this.customerId = customerId;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = PRS.class)
	@JoinColumn(name = "prs_Id", referencedColumnName = "id", nullable = false)
	public PRS getPrsId() {
		return prsId;
	}

	public void setPrsId(PRS prsId) {
		this.prsId = prsId;
	}

	@Column(name = "DISBURSEMENT_AMT")
	public BigDecimal getDisbursementAmount() {
		return disbursementAmount;
	}

	public void setDisbursementAmount(BigDecimal disbursementAmount) {
		this.disbursementAmount = disbursementAmount;
	}

	@Column(name = "DISBURSEMENT_FLAG")
	public char getDisbursementFlag() {
		return disbursementFlag;
	}

	public void setDisbursementFlag(char disbursementFlag) {
		this.disbursementFlag = disbursementFlag;
	}

	@Column(name = "DISBURSEMENT_PENDING")
	public Boolean getCanPendingDisbursement() {
		return canPendingDisbursement;
	}

	public void setCanPendingDisbursement(Boolean canPendingDisbursement) {
		this.canPendingDisbursement = canPendingDisbursement;
	}

	@Column(name = "INSTALLMENT_AMT")
	public BigDecimal getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(BigDecimal installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	@Column(name = "INSTALLMENT_PAY_AMT")
	public BigDecimal getInstallmentPaymentAmount() {
		return installmentPaymentAmount;
	}

	public void setInstallmentPaymentAmount(BigDecimal installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}

	@Column(name = "EMERGENCY_FUND")
	public Boolean getUseEmergencyFund() {
		return useEmergencyFund;
	}

	public void setUseEmergencyFund(Boolean useEmergencyFund) {
		this.useEmergencyFund = useEmergencyFund;
	}

	@Column(name = "MARGIN_AMT")
	public BigDecimal getMarginAmount() {
		return marginAmount;
	}

	public void setMarginAmount(BigDecimal marginAmount) {
		this.marginAmount = marginAmount;
	}

	@Column(name = "PLAFON_AMT")
	public BigDecimal getPlafonAmount() {
		return plafonAmount;
	}

	public void setPlafonAmount(BigDecimal plafonAmount) {
		this.plafonAmount = plafonAmount;
	}

	@Column(name = "OUTSTANDING_AMT")
	public BigDecimal getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(BigDecimal outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	@Column(name = "DUE_DATE")
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	@Column(name = "OVERDUE_DAYS")
	public Long getOverdueDays() {
		return overdueDays;
	}

	public void setOverdueDays(Long overdueDays) {
		this.overdueDays = overdueDays;
	}

	@Column(name = "EARLY_TERMINATION")
	public Boolean getIsEarlyTermination() {
		return isEarlyTermination;
	}

	public void setIsEarlyTermination(Boolean isEarlyTermination) {
		this.isEarlyTermination = isEarlyTermination;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = PRSParameter.class)
	@JoinColumn(name = "early_termination_reason", referencedColumnName = "id", nullable = true)
	public PRSParameter getEarlyTerminationReason() {
		return earlyTerminationReason;
	}

	public void setEarlyTerminationReason(PRSParameter earlyTerminationReason) {
		this.earlyTerminationReason = earlyTerminationReason;
	}

	@Column(name = "MARGIN_DISC_FLAG")
	public Boolean getMarginDiscountDeviationFlag() {
		return marginDiscountDeviationFlag;
	}

	public void setMarginDiscountDeviationFlag(Boolean marginDiscountDeviationFlag) {
		this.marginDiscountDeviationFlag = marginDiscountDeviationFlag;
	}

	@Column(name = "MARGIN_DISC_AMT")
	public BigDecimal getMarginDiscountDeviationAmount() {
		return marginDiscountDeviationAmount;
	}

	public void setMarginDiscountDeviationAmount(BigDecimal marginDiscountDeviationAmount) {
		this.marginDiscountDeviationAmount = marginDiscountDeviationAmount;
	}

	@Column(name = "MARGIN_DISC_PERCENTAGE")
	public BigDecimal getMarginDiscountDeviationPercentage() {
		return marginDiscountDeviationPercentage;
	}

	public void setMarginDiscountDeviationPercentage(BigDecimal marginDiscountDeviationPercentage) {
		this.marginDiscountDeviationPercentage = marginDiscountDeviationPercentage;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@Column(name = "WOW_IB_FLAG")
	public Boolean getWowIbStatus() {
		return wowIbStatus;
	}

	public void setWowIbStatus(Boolean wowIbStatus) {
		this.wowIbStatus = wowIbStatus;
	}

}
