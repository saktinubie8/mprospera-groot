package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class SentraForNonPrsRequest {
	
	private String sentraId;
	private List<CustomerForNonPrsRequest> customerList;
	public String getSentraId() {
		return sentraId;
	}
	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}
	public List<CustomerForNonPrsRequest> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(List<CustomerForNonPrsRequest> customerList) {
		this.customerList = customerList;
	}
	@Override
	public String toString() {
		return "SentraForNonPrsRequest [sentraId=" + sentraId + ", customerList=" + customerList + ", getClass()="
				+ getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}
	
}
