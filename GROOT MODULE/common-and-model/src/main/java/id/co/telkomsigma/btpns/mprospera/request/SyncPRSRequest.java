package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class SyncPRSRequest extends BaseRequest {

	private String username;
	private String imei;
	private String sessionKey;
	private String longitude;
	private String latitude;
	private List<SentraRequest> sentraList;
	private String fetchPreviousPrs;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public List<SentraRequest> getSentraList() {
		return sentraList;
	}

	public void setSentraList(List<SentraRequest> sentraList) {
		this.sentraList = sentraList;
	}

	public String getFetchPreviousPrs() {
		return fetchPreviousPrs;
	}

	public void setFetchPreviousPrs(String fetchPreviousPrs) {
		this.fetchPreviousPrs = fetchPreviousPrs;
	}

	@Override
	public String toString() {
		return "SyncPRSRequest [username=" + username + ", imei=" + imei + ", sessionKey=" + sessionKey
				+ ", longitude=" + longitude + ", latitude=" + latitude + ", sentraList=" + sentraList
				+ ", fetchPreviousPrs=" + fetchPreviousPrs + ", getTransmissionDateAndTime()="
				+ getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
