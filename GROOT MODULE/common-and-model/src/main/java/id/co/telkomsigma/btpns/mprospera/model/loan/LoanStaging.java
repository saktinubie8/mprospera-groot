package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "STAGING_M_LOAN_ACCOUNT")
public class LoanStaging extends GenericModel {

	private String appId;
	private String status;
	private String accountNumber;
	private Date disbursementDate;
	private String customerId;
	private Date createdDate;
	protected String reffId;
	protected Integer modulus;

	@Column(name = "modulus")
	public Integer getModulus() {
		return modulus;
	}

	public void setModulus(Integer modulus) {
		this.modulus = modulus;
	}

	@Column(name = "reff_id")
	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	@Column(name = "CREATED_DT")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Id
	@Column(name = "id", nullable = false, unique = true)
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "ACCOUNT_NUMBER")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Column(name = "DISBURSEMENT_DATE")
	public Date getDisbursementDate() {
		return disbursementDate;
	}

	public void setDisbursementDate(Date disbursementDate) {
		this.disbursementDate = disbursementDate;
	}

	@Column(name = "CUSTOMER_ID_PROSPERA")
	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerName) {
		this.customerId = customerName;
	}
}
