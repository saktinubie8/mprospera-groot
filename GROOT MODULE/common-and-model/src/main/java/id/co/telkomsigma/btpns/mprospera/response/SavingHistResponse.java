package id.co.telkomsigma.btpns.mprospera.response;

import java.math.BigDecimal;
import java.util.List;

public class SavingHistResponse extends BaseResponse {
	
	private String accountNumber;
	private BigDecimal clearBalance;
	private List<HistoryList> historyList;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public BigDecimal getClearBalance() {
		return clearBalance;
	}
	public void setClearBalance(BigDecimal clearBalance) {
		this.clearBalance = clearBalance;
	}
	public List<HistoryList> getHistoryList() {
		return historyList;
	}
	public void setHistoryList(List<HistoryList> historyList) {
		this.historyList = historyList;
	}
	@Override
	public String toString() {
		return "SavingHistResponse [accountNumber=" + accountNumber + ", clearBalance=" + clearBalance
				+ ", historyList=" + historyList + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
		
}
