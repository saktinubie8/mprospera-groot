package id.co.telkomsigma.btpns.mprospera.model.sentra;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "STAGING_T_SENTRA")
public class SentraStaging extends GenericModel {

	protected String sentraName;
	protected String status;
	protected String reffId;
	protected String customerLevelId;
	protected String officeCode;
	protected String address;
	protected Date createdDate;
	protected Integer modulus;

	@Column(name = "modulus")
	public Integer getModulus() {
		return modulus;
	}

	public void setModulus(Integer modulus) {
		this.modulus = modulus;
	}

	@Column(name = "sentra_name")
	public String getSentraName() {
		return sentraName;
	}

	public void setSentraName(String sentraName) {
		this.sentraName = sentraName;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Id
	@Column(name = "reff_id")
	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	@Column(name = "level")
	public String getCustomerLevelId() {
		return customerLevelId;
	}

	public void setCustomerLevelId(String customerLevelId) {
		this.customerLevelId = customerLevelId;
	}

	@Column(name = "office_code")
	public String getOfficeCode() {
		return officeCode;
	}

	public void setOfficeCode(String officeCode) {
		this.officeCode = officeCode;
	}

	@Column(name = "address")
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	@Column(name = "created_dt", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
