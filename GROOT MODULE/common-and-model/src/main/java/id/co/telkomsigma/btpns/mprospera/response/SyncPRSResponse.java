package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SyncPRSResponse extends BaseResponse {

	private List<PRSResponseList> prsList;

	public List<PRSResponseList> getPrsList() {
		return prsList;
	}

	public void setPrsList(List<PRSResponseList> prsList) {
		this.prsList = prsList;
	}

}
