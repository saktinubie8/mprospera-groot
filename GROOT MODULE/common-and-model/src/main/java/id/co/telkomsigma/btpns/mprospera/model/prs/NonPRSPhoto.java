package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.loan.LoanNonPRS;

import javax.persistence.*;

@Entity
@Table(name = "T_NON_PRS_PHOTO")
public class NonPRSPhoto extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long nonPrsPhotoId;
	private LoanNonPRS nonPrsId;
	private byte[] nonPrsPhoto;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getNonPrsPhotoId() {
		return nonPrsPhotoId;
	}

	public void setNonPrsPhotoId(Long nonPrsPhotoId) {
		this.nonPrsPhotoId = nonPrsPhotoId;
	}

	@OneToOne(fetch = FetchType.EAGER, targetEntity = LoanNonPRS.class)
	@JoinColumn(name = "NON_PRS_ID", referencedColumnName = "id")
	public LoanNonPRS getNonPrsId() {
		return nonPrsId;
	}

	public void setNonPrsId(LoanNonPRS nonPrsId) {
		this.nonPrsId = nonPrsId;
	}

	@Column(name = "NON_PRS_PHOTO", nullable = true, length = MAX_LENGTH_RAW)
	public byte[] getNonPrsPhoto() {
		return nonPrsPhoto;
	}

	public void setNonPrsPhoto(byte[] nonPrsPhoto) {
		this.nonPrsPhoto = nonPrsPhoto;
	}

}
