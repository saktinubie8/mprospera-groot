package id.co.telkomsigma.btpns.mprospera.response;

public class TokenResponse extends BaseResponse {

	private String tokenChallenge;

	public String getTokenChallenge() {
		return tokenChallenge;
	}

	public void setTokenChallenge(String tokenChallenge) {
		this.tokenChallenge = tokenChallenge;
	}

	@Override
	public String toString() {
		return "TokenResponse [tokenChallenge=" + tokenChallenge + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
