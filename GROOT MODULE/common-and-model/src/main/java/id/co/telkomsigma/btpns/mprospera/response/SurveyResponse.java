package id.co.telkomsigma.btpns.mprospera.response;

public class SurveyResponse {
	private String surveyId;
	private String customerName;
	private String customerId;
	private String status;
	private String createdBy;
	private String createdDate;
	private String surveyDate;
	private String disbursementDate;
	private String surveyType;

	public String getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(String surveyId) {
		this.surveyId = surveyId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getSurveyDate() {
		return surveyDate;
	}

	public void setSurveyDate(String surveyDate) {
		this.surveyDate = surveyDate;
	}

	public String getDisbursementDate() {
		return disbursementDate;
	}

	public void setDisbursementDate(String disbursementDate) {
		this.disbursementDate = disbursementDate;
	}

	public String getSurveyType() {
		return surveyType;
	}

	public void setSurveyType(String surveyType) {
		this.surveyType = surveyType;
	}

	@Override
	public String toString() {
		return "SurveyResponse [surveyId=" + surveyId + ", customerName=" + customerName + ", customerId=" + customerId
				+ ", status=" + status + ", createdBy=" + createdBy + ", createdDate=" + createdDate + ", surveyDate="
				+ surveyDate + ", disbursementDate=" + disbursementDate + ", surveyType=" + surveyType + "]";
	}

}
