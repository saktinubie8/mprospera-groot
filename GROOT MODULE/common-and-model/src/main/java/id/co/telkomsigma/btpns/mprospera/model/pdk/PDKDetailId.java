package id.co.telkomsigma.btpns.mprospera.model.pdk;

import scala.Serializable;

import javax.persistence.Embeddable;

@Embeddable
public class PDKDetailId implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long swId;

	public PDKDetailId() {

	}

	public Long getSwId() {
		return swId;
	}

	public void setSwId(Long swId) {
		this.swId = swId;
	}

	private PelatihanDasarKeanggotaan pdk;

	public PelatihanDasarKeanggotaan getPdk() {
		return pdk;
	}

	public void setPdk(PelatihanDasarKeanggotaan pdk) {
		this.pdk = pdk;
	}

}
