package id.co.telkomsigma.btpns.mprospera.request;

import java.math.BigDecimal;

public class LoanNonPrsRequest {

	private String loanNonPrsId;
	private String appId;
	private BigDecimal installmentAmount;
	private BigDecimal currentMargin;
	private BigDecimal installmentPaymentAmount;
	private BigDecimal inputMoney;
	private BigDecimal depositMoney;
	private BigDecimal autoDebet;
	private String overdueDays;
	private BigDecimal outstandingAmount;
	private String outstandingReason;
	private String meetingDate;
	private String peopleMeet;
    private boolean payCommitment;
    private String status;
	public String getLoanNonPrsId() {
		return loanNonPrsId;
	}
	public void setLoanNonPrsId(String loanNonPrsId) {
		this.loanNonPrsId = loanNonPrsId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public BigDecimal getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(BigDecimal installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public BigDecimal getCurrentMargin() {
		return currentMargin;
	}
	public void setCurrentMargin(BigDecimal currentMargin) {
		this.currentMargin = currentMargin;
	}
	public String getOverdueDays() {
		return overdueDays;
	}
	public void setOverdueDays(String overdueDays) {
		this.overdueDays = overdueDays;
	}
	public BigDecimal getOutstandingAmount() {
		return outstandingAmount;
	}
	public void setOutstandingAmount(BigDecimal outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}
	public String getOutstandingReason() {
		return outstandingReason;
	}
	public void setOutstandingReason(String outstandingReason) {
		this.outstandingReason = outstandingReason;
	}
	public String getMeetingDate() {
		return meetingDate;
	}
	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}
	public String getPeopleMeet() {
		return peopleMeet;
	}
	public void setPeopleMeet(String peopleMeet) {
		this.peopleMeet = peopleMeet;
	}
	public boolean isPayCommitment() {
		return payCommitment;
	}
	public void setPayCommitment(boolean payCommitment) {
		this.payCommitment = payCommitment;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getInstallmentPaymentAmount() {
		return installmentPaymentAmount;
	}
	public void setInstallmentPaymentAmount(BigDecimal installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}
	public BigDecimal getInputMoney() {
		return inputMoney;
	}
	public void setInputMoney(BigDecimal inputMoney) {
		this.inputMoney = inputMoney;
	}
	public BigDecimal getDepositMoney() {
		return depositMoney;
	}
	public void setDepositMoney(BigDecimal depositMoney) {
		this.depositMoney = depositMoney;
	}
	public BigDecimal getAutoDebet() {
		return autoDebet;
	}
	public void setAutoDebet(BigDecimal autoDebet) {
		this.autoDebet = autoDebet;
	}
	@Override
	public String toString() {
		return "LoanNonPrsRequest [loanNonPrsId=" + loanNonPrsId + ", appId=" + appId + ", installmentAmount="
				+ installmentAmount + ", currentMargin=" + currentMargin + ", installmentPaymentAmount="
				+ installmentPaymentAmount + ", inputMoney=" + inputMoney + ", depositMoney=" + depositMoney
				+ ", autoDebet=" + autoDebet + ", overdueDays=" + overdueDays + ", outstandingAmount="
				+ outstandingAmount + ", outstandingReason=" + outstandingReason + ", meetingDate=" + meetingDate
				+ ", peopleMeet=" + peopleMeet + ", payCommitment=" + payCommitment + ", status=" + status + "]";
	}
      
}
