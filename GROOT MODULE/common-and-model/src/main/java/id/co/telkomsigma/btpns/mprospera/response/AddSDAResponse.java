package id.co.telkomsigma.btpns.mprospera.response;

public class AddSDAResponse extends BaseResponse {

	@Override
	public String toString() {
		return "AddSDAResponse [" + (areaId != null ? "areaId=" + areaId + ", " : "")
				+ (sdaId != null ? "sdaId=" + sdaId + ", " : "")
				+ (getResponseCode() != null ? "ResponseCode=" + getResponseCode() + ", " : "")
				+ (getResponseMessage() != null ? "ResponseMessage=" + getResponseMessage() : "") + "]";
	}

	private String areaId;
	private String sdaId;

	public String getSdaId() {
		return sdaId;
	}

	public void setSdaId(String sdaId) {
		this.sdaId = sdaId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

}
