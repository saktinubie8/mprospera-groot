package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class LoanRequest extends BaseRequest {

	// for add

	private String username;
	private String sessionKey;
	private String imei;
	private String swId;
	private String customerName;
	private String status;
	private String longitude;
	private String latitude;
	private String loanAmountRecommended;
	private String action;
	private String loanId;

	// for list
	private String getCountData;
	private String page;
	private String startLookupDate;
	private String endLookupDate;

	// for prospera purpose
	private String custReffId;
	private String loanReffId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSwId() {
		return swId;
	}

	public void setSwId(String swId) {
		this.swId = swId;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getGetCountData() {
		return getCountData;
	}

	public void setGetCountData(String getCountData) {
		this.getCountData = getCountData;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getStartLookupDate() {
		return startLookupDate;
	}

	public void setStartLookupDate(String startLookupDate) {
		this.startLookupDate = startLookupDate;
	}

	public String getEndLookupDate() {
		return endLookupDate;
	}

	public void setEndLookupDate(String endLookupDate) {
		this.endLookupDate = endLookupDate;
	}

	public String getCustReffId() {
		return custReffId;
	}

	public void setCustReffId(String custReffId) {
		this.custReffId = custReffId;
	}

	public String getLoanReffId() {
		return loanReffId;
	}

	public void setLoanReffId(String loanReffId) {
		this.loanReffId = loanReffId;
	}

	public String getLoanAmountRecommended() {
		return loanAmountRecommended;
	}

	public void setLoanAmountRecommended(String loanAmountRecommended) {
		this.loanAmountRecommended = loanAmountRecommended;
	}

	@Override
	public String toString() {
		return "LoanRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei + ", swId=" + swId
				+ ", customerName=" + customerName + ", status=" + status + ", longitude=" + longitude + ", latitude="
				+ latitude + ", loanAmountRecommended=" + loanAmountRecommended + ", action=" + action + ", loanId="
				+ loanId + ", getCountData=" + getCountData + ", page=" + page + ", startLookupDate=" + startLookupDate
				+ ", endLookupDate=" + endLookupDate + ", custReffId=" + custReffId + ", loanReffId=" + loanReffId
				+ "]";
	}

}
