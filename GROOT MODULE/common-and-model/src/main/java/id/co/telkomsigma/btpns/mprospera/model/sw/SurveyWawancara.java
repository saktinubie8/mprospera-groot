package id.co.telkomsigma.btpns.mprospera.model.sw;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_SURVEY_WAWANCARA")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SurveyWawancara extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8383613399759073380L;
	private Long swId;
	private String areaId;
	private String customerIdNumber;
	private String customerName;
	private String createdBy;
	private Date createdDate;
	private Character customerRegistrationType;
	private String customerCifNumber;
	private Character workType;
	private Date surveyDate;
	private Character houseType;
	private Boolean isNeighborhoodRecommendation = false;
	private BigDecimal workIncome;
	private BigDecimal totalPurchase;
	private BigDecimal workExpense;
	private BigDecimal iir;
	private BigDecimal loanAmountRequested;
	private BigDecimal loanAmountRecommended;
	private Boolean isFinalRecommendation = false;
	private String longitude;
	private String latitude;
	private Boolean isDeleted = false;
	private String appId;
	private String rrn;

	@Column(name = "app_id", nullable = true)
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getSwId() {
		return swId;
	}

	public void setSwId(Long swId) {
		this.swId = swId;
	}

	@Column(name = "area_id", nullable = false)
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	@Column(name = "cust_id_number", nullable = false)
	public String getCustomerIdNumber() {
		return customerIdNumber;
	}

	public void setCustomerIdNumber(String customerIdNumber) {
		this.customerIdNumber = customerIdNumber;
	}

	@Column(name = "cust_name", nullable = false)
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name = "created_by", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "created_date", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "cust_reg_type", nullable = true)
	public Character getCustomerRegistrationType() {
		return customerRegistrationType;
	}

	public void setCustomerRegistrationType(Character customerRegistrationType) {
		this.customerRegistrationType = customerRegistrationType;
	}

	@Column(name = "cust_cif_number", nullable = true)
	public String getCustomerCifNumber() {
		return customerCifNumber;
	}

	public void setCustomerCifNumber(String customerCifNumber) {
		this.customerCifNumber = customerCifNumber;
	}

	@Column(name = "work_type", nullable = true)
	public Character getWorkType() {
		return workType;
	}

	public void setWorkType(Character workType) {
		this.workType = workType;
	}

	@Column(name = "survey_date", nullable = true)
	public Date getSurveyDate() {
		return surveyDate;
	}

	public void setSurveyDate(Date surveyDate) {
		this.surveyDate = surveyDate;
	}

	@Column(name = "house_type", nullable = true)
	public Character getHouseType() {
		return houseType;
	}

	public void setHouseType(Character houseType) {
		this.houseType = houseType;
	}

	@Column(name = "is_neighborhood_recommendation", nullable = true)
	public Boolean getIsNeighborhoodRecommendation() {
		return isNeighborhoodRecommendation;
	}

	public void setIsNeighborhoodRecommendation(Boolean isNeighborhoodRecommendation) {
		this.isNeighborhoodRecommendation = isNeighborhoodRecommendation;
	}

	@Column(name = "work_income", nullable = true)
	public BigDecimal getWorkIncome() {
		return workIncome;
	}

	public void setWorkIncome(BigDecimal workIncome) {
		this.workIncome = workIncome;
	}

	@Column(name = "total_purchase", nullable = true)
	public BigDecimal getTotalPurchase() {
		return totalPurchase;
	}

	public void setTotalPurchase(BigDecimal totalPurchase) {
		this.totalPurchase = totalPurchase;
	}

	@Column(name = "work_expense", nullable = true)
	public BigDecimal getWorkExpense() {
		return workExpense;
	}

	public void setWorkExpense(BigDecimal workExpense) {
		this.workExpense = workExpense;
	}

	@Column(name = "iir", nullable = true)
	public BigDecimal getIir() {
		return iir;
	}

	public void setIir(BigDecimal iir) {
		this.iir = iir;
	}

	@Column(name = "loan_amt_req", nullable = true)
	public BigDecimal getLoanAmountRequested() {
		return loanAmountRequested;
	}

	public void setLoanAmountRequested(BigDecimal loanAmountRequested) {
		this.loanAmountRequested = loanAmountRequested;
	}

	@Column(name = "is_final_recommedation", nullable = true)
	public Boolean getIsFinalRecommendation() {
		return isFinalRecommendation;
	}

	public void setIsFinalRecommendation(Boolean isFinalRecommendation) {
		this.isFinalRecommendation = isFinalRecommendation;
	}

	@Column(name = "longitude", nullable = true)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "latitude", nullable = true)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "is_deleted", nullable = true)
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name = "loan_amt_rec", nullable = true)
	public BigDecimal getLoanAmountRecommended() {
		return loanAmountRecommended;
	}

	public void setLoanAmountRecommended(BigDecimal loanAmountRecommended) {
		this.loanAmountRecommended = loanAmountRecommended;
	}

	@Column(name = "rrn")
	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

}
