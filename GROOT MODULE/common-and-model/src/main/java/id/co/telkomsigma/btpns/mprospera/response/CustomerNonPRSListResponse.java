package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CustomerNonPRSListResponse {
	
	private String customerId;
	private String customerName;
	private List<LoanNonPRSListResponse> loanList;
	private List<SavingNonPRSListResponse> savingList;
	public List<LoanNonPRSListResponse> getLoanList() {
		return loanList;
	}
	public void setLoanList(List<LoanNonPRSListResponse> loanList) {
		this.loanList = loanList;
	}
	public List<SavingNonPRSListResponse> getSavingList() {
		return savingList;
	}
	public void setSavingList(List<SavingNonPRSListResponse> savingList) {
		this.savingList = savingList;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

}
