package id.co.telkomsigma.btpns.mprospera.request;

public class SDARequest {
	private String areaName;
	private KkHistoryRequest[] kkHistory;

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public KkHistoryRequest[] getKkHistory() {
		return kkHistory;
	}

	public void setKkHistory(KkHistoryRequest[] kkHistory) {
		this.kkHistory = kkHistory;
	}
}
