package id.co.telkomsigma.btpns.mprospera.response;

public class MMResponse {
	private String mmId;
	private String areaName;
	private String areaId;
	private String createdBy;
	private String createdDate;
	private String mmNumber;
	private String mmLocationName;
	private String phoneNumber;
	private String mmOwner;

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMmOwner() {
		return mmOwner;
	}

	public void setMmOwner(String mmOwner) {
		this.mmOwner = mmOwner;
	}

	public String getMmId() {
		return mmId;
	}

	public void setMmId(String mmId) {
		this.mmId = mmId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getMmNumber() {
		return mmNumber;
	}

	public void setMmNumber(String mmNumber) {
		this.mmNumber = mmNumber;
	}

	public String getMmLocationName() {
		return mmLocationName;
	}

	public void setMmLocationName(String mmLocationName) {
		this.mmLocationName = mmLocationName;
	}

	@Override
	public String toString() {
		return "MMResponse [mmId=" + mmId + ", areaName=" + areaName + ", areaId=" + areaId + ", createdBy="
				+ createdBy + ", createdDate=" + createdDate + ", mmNumber=" + mmNumber + ", mmLocationName="
				+ mmLocationName + "]";
	}

}
