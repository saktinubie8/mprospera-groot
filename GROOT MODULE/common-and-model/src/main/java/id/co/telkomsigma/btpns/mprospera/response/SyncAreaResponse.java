package id.co.telkomsigma.btpns.mprospera.response;

import java.util.ArrayList;
import java.util.List;

public class SyncAreaResponse extends BaseResponse {
	private List<AreaKecamatanResponse> areaListKecamatan;

	public List<AreaKecamatanResponse> getAreaListKecamatan() {
		if (areaListKecamatan == null)
			areaListKecamatan = new ArrayList<>();
		return areaListKecamatan;
	}

	public void setAreaListKecamatan(List<AreaKecamatanResponse> areaListKecamatan) {
		this.areaListKecamatan = areaListKecamatan;
	}
}
