package id.co.telkomsigma.btpns.mprospera.model.customer;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "STAGING_M_CUSTOMER")
public class CustomerStaging extends GenericModel {
	protected String customerCifNumber;
	protected String customerName;
	protected String customerIdNumber;
	protected Date createdDate;
	protected String customerStatus;
	protected String customerLevelId;
	protected String group;
	protected String assignedUsername;
	protected String reffId;
	protected String prosperaId;
	protected Integer modulus;

	@Column(name = "modulus")
	public Integer getModulus() {
		return modulus;
	}

	public void setModulus(Integer modulus) {
		this.modulus = modulus;
	}

	@Column(name = "reff_id")
	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	@Column(name = "status")
	public String getCustomerStatus() {
		return customerStatus;
	}

	public void setCustomerStatus(String customerStatus) {
		this.customerStatus = customerStatus;
	}

	@Column(name = "level")
	public String getCustomerLevelId() {
		return customerLevelId;
	}

	public void setCustomerLevelId(String customerLevelId) {
		this.customerLevelId = customerLevelId;
	}

	@Column(name = "group_id")
	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	@Column(name = "assigned_username")
	public String getAssignedUsername() {
		return assignedUsername;
	}

	public void setAssignedUsername(String assignedUsername) {
		this.assignedUsername = assignedUsername;
	}

	@Id
	@Column(name = "cif_number", nullable = false, unique = true)
	public String getCustomerCifNumber() {
		return customerCifNumber;
	}

	public void setCustomerCifNumber(String customerCifNumber) {
		this.customerCifNumber = customerCifNumber;
	}

	@Column(name = "name", nullable = false)
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name = "id_number", nullable = false)
	public String getCustomerIdNumber() {
		return customerIdNumber;
	}

	public void setCustomerIdNumber(String customerIdNumber) {
		this.customerIdNumber = customerIdNumber;
	}

	@Column(name = "created_dt", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "prospera_id", nullable = false)
	public String getProsperaId() {
		return prosperaId;
	}

	public void setProsperaId(String prosperaId) {
		this.prosperaId = prosperaId;
	}

}
