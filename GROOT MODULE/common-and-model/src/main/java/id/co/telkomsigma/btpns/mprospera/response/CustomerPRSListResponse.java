package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class CustomerPRSListResponse {

	private String customerId;
	private String customerPrsId;
	private String groupId;
	private String hasIdCardPhoto;
	private String hasBusinessPlacePhoto;
	private Boolean isAttend;
	private String notAttendReason;
	// private String hasDisbursementPlan;
	private List<LoanPRSListResponse> loanList;
	private List<SavingPRSListResponse> savingList;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerPrsId() {
		return customerPrsId;
	}

	public void setCustomerPrsId(String customerPrsId) {
		this.customerPrsId = customerPrsId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getHasIdCardPhoto() {
		return hasIdCardPhoto;
	}

	public void setHasIdCardPhoto(String hasIdCardPhoto) {
		this.hasIdCardPhoto = hasIdCardPhoto;
	}

	public Boolean getIsAttend() {
		return isAttend;
	}

	public void setIsAttend(Boolean isAttend) {
		this.isAttend = isAttend;
	}

	public String getNotAttendReason() {
		return notAttendReason;
	}

	public void setNotAttendReason(String notAttendReason) {
		this.notAttendReason = notAttendReason;
	}

	public List<LoanPRSListResponse> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<LoanPRSListResponse> loanList) {
		this.loanList = loanList;
	}

	public List<SavingPRSListResponse> getSavingList() {
		return savingList;
	}

	public void setSavingList(List<SavingPRSListResponse> savingList) {
		this.savingList = savingList;
	}

	/*
	 * public String getHasDisbursementPlan() { return hasDisbursementPlan; }
	 * 
	 * public void setHasDisbursementPlan(String hasDisbursementPlan) {
	 * this.hasDisbursementPlan = hasDisbursementPlan; }
	 */
	public String getHasBusinessPlacePhoto() {
		return hasBusinessPlacePhoto;
	}

	public void setHasBusinessPlacePhoto(String hasBusinessPlacePhoto) {
		this.hasBusinessPlacePhoto = hasBusinessPlacePhoto;
	}

	@Override
	public String toString() {
		return "CustomerPRSListResponse [customerId=" + customerId + ", customerPrsId=" + customerPrsId + ", groupId="
				+ groupId + ", hasIdCardPhoto=" + hasIdCardPhoto + ", hasBusinessPlacePhoto=" + hasBusinessPlacePhoto
				+ ", isAttend=" + isAttend + ", notAttendReason=" + notAttendReason + ", loanList=" + loanList
				+ ", savingList=" + savingList + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
