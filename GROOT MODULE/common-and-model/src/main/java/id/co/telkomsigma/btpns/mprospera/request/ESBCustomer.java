package id.co.telkomsigma.btpns.mprospera.request;

import java.util.Arrays;


public class ESBCustomer {

	private String customerId;
	private String isAttend;
	private String notAttendReason;
	private ESBLoanCustomer[] loanList;
	private ESBSaving[] savingList;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getIsAttend() {
		return isAttend;
	}

	public void setIsAttend(String isAttend) {
		this.isAttend = isAttend;
	}

	public String getNotAttendReason() {
		return notAttendReason;
	}

	public void setNotAttendReason(String notAttendReason) {
		this.notAttendReason = notAttendReason;
	}

	public ESBLoanCustomer[] getLoanList() {
		return loanList;
	}

	public void setLoanList(ESBLoanCustomer[] loanList) {
		this.loanList = loanList;
	}

	public ESBSaving[] getSavingList() {
		return savingList;
	}

	public void setSavingList(ESBSaving[] savingList) {
		this.savingList = savingList;
	}

	@Override
	public String toString() {
		return "Customer [customerId=" + customerId + ", isAttend=" + isAttend + ", notAttendReason=" + notAttendReason
				+ ", loanList=" + Arrays.toString(loanList) + ", savingList=" + Arrays.toString(savingList) + "]";
	}

}
