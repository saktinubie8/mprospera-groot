package id.co.telkomsigma.btpns.mprospera.request;

public class ESBLoanCustomer {

	private String loanId;
	private String appid;
	private String disbursementFlag;
	private String installmentPaymentAmount;
	private String useEmergencyFund;
	private String isEarlyTermination;
	private String earlyTerminationReason;
	private String marginDiscountDeviationFlag;
	private String marginDiscountDeviationAmount;
	private String marginDiscountDeviationPercentage;

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getDisbursementFlag() {
		return disbursementFlag;
	}

	public void setDisbursementFlag(String disbursementFlag) {
		this.disbursementFlag = disbursementFlag;
	}

	public String getInstallmentPaymentAmount() {
		return installmentPaymentAmount;
	}

	public void setInstallmentPaymentAmount(String installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}

	public String getUseEmergencyFund() {
		return useEmergencyFund;
	}

	public void setUseEmergencyFund(String useEmergencyFund) {
		this.useEmergencyFund = useEmergencyFund;
	}

	public String getIsEarlyTermination() {
		return isEarlyTermination;
	}

	public void setIsEarlyTermination(String isEarlyTermination) {
		this.isEarlyTermination = isEarlyTermination;
	}

	public String getEarlyTerminationReason() {
		return earlyTerminationReason;
	}

	public void setEarlyTerminationReason(String earlyTerminationReason) {
		this.earlyTerminationReason = earlyTerminationReason;
	}

	public String getMarginDiscountDeviationFlag() {
		return marginDiscountDeviationFlag;
	}

	public void setMarginDiscountDeviationFlag(String marginDiscountDeviationFlag) {
		this.marginDiscountDeviationFlag = marginDiscountDeviationFlag;
	}

	public String getMarginDiscountDeviationAmount() {
		return marginDiscountDeviationAmount;
	}

	public void setMarginDiscountDeviationAmount(String marginDiscountDeviationAmount) {
		this.marginDiscountDeviationAmount = marginDiscountDeviationAmount;
	}

	public String getMarginDiscountDeviationPercentage() {
		return marginDiscountDeviationPercentage;
	}

	public void setMarginDiscountDeviationPercentage(String marginDiscountDeviationPercentage) {
		this.marginDiscountDeviationPercentage = marginDiscountDeviationPercentage;
	}

	@Override
	public String toString() {
		return "LoanCustomer [loanId=" + loanId + ", appid=" + appid + ", disbursementFlag=" + disbursementFlag
				+ ", installmentPaymentAmount=" + installmentPaymentAmount + ", useEmergencyFund=" + useEmergencyFund
				+ ", isEarlyTermination=" + isEarlyTermination + ", earlyTerminationReason=" + earlyTerminationReason
				+ ", marginDiscountDeviationFlag=" + marginDiscountDeviationFlag + ", marginDiscountDeviationAmount="
				+ marginDiscountDeviationAmount + ", marginDiscountDeviationPercentage="
				+ marginDiscountDeviationPercentage + "]";
	}

}
