package id.co.telkomsigma.btpns.mprospera.response;

public class LDAPLoginResponse {

	private Userauth userauth;

	public Userauth getUserauth() {
		return userauth;
	}

	public void setUserauth(Userauth userauth) {
		this.userauth = userauth;
	}

	@Override
	public String toString() {
		return "LDAPLoginResponse [userauth=" + userauth + "]";
	}

	public class Userauth {

		private String success;
		private String description;

		public String getSuccess() {
			return success;
		}

		public void setSuccess(String success) {
			this.success = success;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}

		@Override
		public String toString() {
			return "userauth [success=" + success + ", description=" + description + "]";
		}
	}
}
