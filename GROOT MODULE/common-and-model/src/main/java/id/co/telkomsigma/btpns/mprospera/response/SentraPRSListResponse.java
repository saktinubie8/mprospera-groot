package id.co.telkomsigma.btpns.mprospera.response;

public class SentraPRSListResponse {
	
	private String sentraId;
	private String statusPrs;
	private String updatedDate;
	public String getSentraId() {
		return sentraId;
	}
	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}
	public String getStatusPrs() {
		return statusPrs;
	}
	public void setStatusPrs(String statusPrs) {
		this.statusPrs = statusPrs;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	
	

}
