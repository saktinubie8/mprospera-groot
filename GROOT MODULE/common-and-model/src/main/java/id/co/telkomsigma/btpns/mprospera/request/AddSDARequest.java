package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddSDARequest extends BaseRequest {

	private String username;
	private String imei;
	private String sessionKey;
	private String transmissionDateAndTime;
	private String retrievalReferenceNumber;
	private String areaCategory;
	private String parentAreaId;
	private String areaId;
	private String areaName;
	private String countKkMiskin;
	private String sdaId;
	private String action;
	private String longitude;
	private String latitude;

	public String getLongitude() {
		return longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getSdaId() {
		return sdaId;
	}

	public void setSdaId(String sdaId) {
		this.sdaId = sdaId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}

	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}

	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}

	public String getAreaCategory() {
		return areaCategory;
	}

	public void setAreaCategory(String areaCategory) {
		this.areaCategory = areaCategory;
	}

	public String getParentAreaId() {
		return parentAreaId;
	}

	public void setParentAreaId(String parentAreaId) {
		this.parentAreaId = parentAreaId;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getCountKkMiskin() {
		return countKkMiskin;
	}

	public void setCountKkMiskin(String countKkMiskin) {
		this.countKkMiskin = countKkMiskin;
	}

	@Override
	public String toString() {
		return "AddSDARequest [" + (username != null ? "username=" + username + ", " : "")
				+ (imei != null ? "imei=" + imei + ", " : "")
				+ (sessionKey != null ? "sessionKey=" + sessionKey + ", " : "")
				+ (transmissionDateAndTime != null ? "transmissionDateAndTime=" + transmissionDateAndTime + ", " : "")
				+ (retrievalReferenceNumber != null ? "retrievalReferenceNumber=" + retrievalReferenceNumber + ", "
						: "")
				+ (areaCategory != null ? "areaCategory=" + areaCategory + ", " : "")
				+ (parentAreaId != null ? "parentAreaId=" + parentAreaId + ", " : "")
				+ (areaId != null ? "areaId=" + areaId + ", " : "")
				+ (areaName != null ? "areaName=" + areaName + ", " : "")
				+ (countKkMiskin != null ? "countKkMiskin=" + countKkMiskin + ", " : "")
				+ (sdaId != null ? "sdaId=" + sdaId + ", " : "") + (action != null ? "action=" + action + ", " : "")
				+ (longitude != null ? "longitude=" + longitude + ", " : "")
				+ (latitude != null ? "latitude=" + latitude : "") + "]";
	}

}
