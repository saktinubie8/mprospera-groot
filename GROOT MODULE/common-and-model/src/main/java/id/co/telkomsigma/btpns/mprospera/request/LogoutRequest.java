package id.co.telkomsigma.btpns.mprospera.request;

public class LogoutRequest extends BaseRequest {
	private String imei;
	private String sessionKey;
	private String username;
	private String errorLog;

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getErrorLog() {
		return errorLog;
	}

	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}

	@Override
	public String toString() {
		return "LogoutRequest [imei=" + imei + ", errorLog=" + errorLog + "]";
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
