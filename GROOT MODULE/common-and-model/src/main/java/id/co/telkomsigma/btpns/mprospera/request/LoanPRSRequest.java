package id.co.telkomsigma.btpns.mprospera.request;

public class LoanPRSRequest {

	private String loanId;
	private String loanPrsId;
	private String appId;
	private String disbursementFlag;
	private String disbursementPhoto;
	private String installmentPaymentAmount;
	private String useEmergencyFund;
	private String isEarlyTermination;
	private String earlyTerminationReason;
	private String marginDiscountDeviationFlag;
	private String marginDiscountDeviationAmount;
	private String marginDiscountDeviationPercentage;
	private String wowIbStatus;

	public String getLoanId() {
		return loanId;
	}

	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getDisbursementFlag() {
		return disbursementFlag;
	}

	public void setDisbursementFlag(String disbursementFlag) {
		this.disbursementFlag = disbursementFlag;
	}

	public String getDisbursementPhoto() {
		return disbursementPhoto;
	}

	public void setDisbursementPhoto(String disbursementPhoto) {
		this.disbursementPhoto = disbursementPhoto;
	}

	public String getInstallmentPaymentAmount() {
		return installmentPaymentAmount;
	}

	public void setInstallmentPaymentAmount(String installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}

	public String getUseEmergencyFund() {
		return useEmergencyFund;
	}

	public void setUseEmergencyFund(String useEmergencyFund) {
		this.useEmergencyFund = useEmergencyFund;
	}

	public String getIsEarlyTermination() {
		return isEarlyTermination;
	}

	public void setIsEarlyTermination(String isEarlyTermination) {
		this.isEarlyTermination = isEarlyTermination;
	}

	public String getEarlyTerminationReason() {
		return earlyTerminationReason;
	}

	public void setEarlyTerminationReason(String earlyTerminationReason) {
		this.earlyTerminationReason = earlyTerminationReason;
	}

	public String getMarginDiscountDeviationFlag() {
		return marginDiscountDeviationFlag;
	}

	public void setMarginDiscountDeviationFlag(String marginDiscountDeviationFlag) {
		this.marginDiscountDeviationFlag = marginDiscountDeviationFlag;
	}

	public String getMarginDiscountDeviationAmount() {
		return marginDiscountDeviationAmount;
	}

	public String getMarginDiscountDeviationPercentage() {
		return marginDiscountDeviationPercentage;
	}

	public void setMarginDiscountDeviationPercentage(String marginDiscountDeviationPercentage) {
		this.marginDiscountDeviationPercentage = marginDiscountDeviationPercentage;
	}

	public void setMarginDiscountDeviationAmount(String marginDiscountDeviationAmount) {
		this.marginDiscountDeviationAmount = marginDiscountDeviationAmount;
	}

	public String getLoanPrsId() {
		return loanPrsId;
	}

	public void setLoanPrsId(String loanPrsId) {
		this.loanPrsId = loanPrsId;
	}

	public String getWowIbStatus() {
		return wowIbStatus;
	}

	public void setWowIbStatus(String wowIbStatus) {
		this.wowIbStatus = wowIbStatus;
	}

}
