package id.co.telkomsigma.btpns.mprospera.model.survey;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "T_SURVEY")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class Survey extends GenericModel {

	private static final long serialVersionUID = 408866013256083202L;

	private Long surveyId;
	private String surveyTypeId;
	private Date surveyDate;
	private String customerName;
	private String status;
	private String appId;
	private Date disbursementDate;
	private String longitude;
	private String latitude;
	private Date createdDate;
	private String createdBy;
	private List<SurveyDetail> surveyDetail;
	private Customer customer;

	@Column(name = "app_id", nullable = true)
	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Id
	@GeneratedValue
	@Column(name = "id", unique = true, nullable = false)
	public Long getSurveyId() {
		return surveyId;
	}

	public void setSurveyId(Long surveyId) {
		this.surveyId = surveyId;
	}

	@Column(name = "survey_type", nullable = true)
	public String getSurveyTypeId() {
		return surveyTypeId;
	}

	public void setSurveyTypeId(String surveyTypeId) {
		this.surveyTypeId = surveyTypeId;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = false)
	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customerId) {
		this.customer = customerId;
	}

	@Column(name = "longitude", nullable = true)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "latitude", nullable = true)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "created_dt", nullable = true)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "created_by", nullable = true)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "survey_dt", nullable = true)
	public Date getSurveyDate() {
		return surveyDate;
	}

	public void setSurveyDate(Date surveyDate) {
		this.surveyDate = surveyDate;
	}

	@Column(name = "customer_name", nullable = true)
	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	@Column(name = "status", nullable = true)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "disbursement_dt", nullable = true)
	public Date getDisbursementDate() {
		return disbursementDate;
	}

	public void setDisbursementDate(Date disbursementDate) {
		this.disbursementDate = disbursementDate;
	}

	@OneToMany(fetch = FetchType.EAGER, targetEntity = SurveyDetail.class, cascade = CascadeType.ALL)
	@JoinColumn(name = "survey_id")
	public List<SurveyDetail> getSurveyDetail() {
		return surveyDetail;
	}

	public void setSurveyDetail(List<SurveyDetail> surveyDetail) {
		this.surveyDetail = surveyDetail;
	}
}
