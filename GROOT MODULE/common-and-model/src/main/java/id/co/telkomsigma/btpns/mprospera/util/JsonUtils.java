package id.co.telkomsigma.btpns.mprospera.util;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class JsonUtils {
	ObjectMapper objectMapper;

	public JsonUtils() {
		this.objectMapper = new ObjectMapper();
	}

	public String toJson(Object obj) throws IOException, JsonMappingException, JsonGenerationException {
		return this.objectMapper.writeValueAsString(obj);
	}

	public <T> Object fromJson(String json, Class<T> cls) throws IOException, JsonParseException {
		return this.objectMapper.readValue(json, cls);
	}
}
