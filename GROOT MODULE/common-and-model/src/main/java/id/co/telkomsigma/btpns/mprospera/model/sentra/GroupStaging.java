package id.co.telkomsigma.btpns.mprospera.model.sentra;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "STAGING_T_GROUP")
public class GroupStaging extends GenericModel {

	protected String groupName;
	protected String status;
	protected String reffId;
	protected String customerLevelId;
	protected String sentraId;
	protected Date createdDate;
	protected Integer modulus;

	@Column(name = "modulus")
	public Integer getModulus() {
		return modulus;
	}

	public void setModulus(Integer modulus) {
		this.modulus = modulus;
	}

	@Column(name = "group_name")
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	@Column(name = "status")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Id
	@Column(name = "reff_id", unique = false)
	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	@Column(name = "level")
	public String getCustomerLevelId() {
		return customerLevelId;
	}

	public void setCustomerLevelId(String customerLevelId) {
		this.customerLevelId = customerLevelId;
	}

	@Column(name = "sentra_id")
	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	@Column(name = "created_dt", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

}
