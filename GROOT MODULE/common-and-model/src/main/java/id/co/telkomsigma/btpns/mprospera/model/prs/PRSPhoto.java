package id.co.telkomsigma.btpns.mprospera.model.prs;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

@Entity
@Table(name = "T_PRS_PHOTO")
public class PRSPhoto extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1949525186839109784L;

	private Long prsPhotoId;
	private PRS prsId;
	private byte[] prsPhoto;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getPrsPhotoId() {
		return prsPhotoId;
	}

	public void setPrsPhotoId(Long prsPhotoId) {
		this.prsPhotoId = prsPhotoId;
	}

	@OneToOne(fetch = FetchType.EAGER, targetEntity = PRS.class)
	@JoinColumn(name = "PRS_ID", referencedColumnName = "id")
	public PRS getPrsId() {
		return prsId;
	}

	public void setPrsId(PRS prsId) {
		this.prsId = prsId;
	}

	@Column(name = "PRS_PHOTO", nullable = true, length = MAX_LENGTH_RAW)
	public byte[] getPrsPhoto() {
		return prsPhoto;
	}

	public void setPrsPhoto(byte[] prsPhoto) {
		this.prsPhoto = prsPhoto;
	}

}
