package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ESBLoanRefreshResponse extends BaseResponse {

	List<ESBLoanRefreshResponse> loanList;

	public List<ESBLoanRefreshResponse> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<ESBLoanRefreshResponse> loanList) {
		this.loanList = loanList;
	}

	@Override
	public String toString() {
		return "ProsperaLoanRefreshResponse [loanList=" + loanList + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + "]";
	}

}
