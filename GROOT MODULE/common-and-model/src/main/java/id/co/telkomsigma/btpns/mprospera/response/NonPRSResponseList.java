package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class NonPRSResponseList {

	private String sentraId;
	private String mmsCode;
	private String mmsName;
	private List<CustomerNonPRSListResponse> customerList;
	
	
	public String getMmsCode() {
		return mmsCode;
	}
	public void setMmsCode(String mmsCode) {
		this.mmsCode = mmsCode;
	}
	public String getMmsName() {
		return mmsName;
	}
	public void setMmsName(String mmsName) {
		this.mmsName = mmsName;
	}
	public String getSentraId() {
		return sentraId;
	}
	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}
	public List<CustomerNonPRSListResponse> getCustomerList() {
		return customerList;
	}
	public void setCustomerList(List<CustomerNonPRSListResponse> customerList) {
		this.customerList = customerList;
	}
	
	
}
