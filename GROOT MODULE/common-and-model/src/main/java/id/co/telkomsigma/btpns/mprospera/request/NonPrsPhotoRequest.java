package id.co.telkomsigma.btpns.mprospera.request;

public class NonPrsPhotoRequest extends BaseRequest {
	
	private String imei;
	private String username;
	private String sessionKey;
	private String longitude;
	private String latitude;
	private String loanNonPrsId;
	private String nonPrsPhoto;
	public String getImei() {
		return imei;
	}
	public void setImei(String imei) {
		this.imei = imei;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLoanNonPrsId() {
		return loanNonPrsId;
	}
	public void setLoanNonPrsId(String loanNonPrsId) {
		this.loanNonPrsId = loanNonPrsId;
	}
	public String getNonPrsPhoto() {
		return nonPrsPhoto;
	}
	public void setNonPrsPhoto(String nonPrsPhoto) {
		this.nonPrsPhoto = nonPrsPhoto;
	}
	@Override
	public String toString() {
		return "NonPrsPhotoRequest [imei=" + imei + ", username=" + username + ", sessionKey=" + sessionKey
				+ ", longitude=" + longitude + ", latitude=" + latitude + ", loanNonPrsId=" + loanNonPrsId
				+ ", nonPrsPhoto=" + nonPrsPhoto + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}
	
}
