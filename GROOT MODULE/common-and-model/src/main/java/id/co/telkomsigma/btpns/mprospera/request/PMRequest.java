package id.co.telkomsigma.btpns.mprospera.request;

public class PMRequest extends BaseRequest {

	/* untuk add PM */
	private String transmissionDateAndTime;
	private String retrievalReferenceNumber;
	private String username;
	private String sessionKey;
	private String imei;
	private String areaId;
	private String pmLocationName;
	private String pmOwner;
	private String pmNumber;
	private String longitude;
	private String latitude;
	private String action;
	private String pmId;

	/* untuk list PM */
	private String getCountData;
	private String lastPmId;
	private String startLookupDate;
	private String endLookupDate;
	private String page;

	@Override
	public String toString() {
		return "PMRequest [transmissionDateAndTime=" + transmissionDateAndTime + ", retrievalReferenceNumber="
				+ retrievalReferenceNumber + ", username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei
				+ ", areaId=" + areaId + ", pmLocationName=" + pmLocationName + ", pmOwner=" + pmOwner + ", pmNumber="
				+ pmNumber + ", longitude=" + longitude + ", latitude=" + latitude + ", action=" + action + ", pmId="
				+ pmId + ", getCountData=" + getCountData + ", lastPmId=" + lastPmId + ", startLookupDate="
				+ startLookupDate + ", endLookupDate=" + endLookupDate + ", page=" + page
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + ", getUsername()=" + getUsername() + ", getSessionKey()="
				+ getSessionKey() + ", getImei()=" + getImei() + ", getAreaId()=" + getAreaId()
				+ ", getPmLocationName()=" + getPmLocationName() + ", getPmOwner()=" + getPmOwner()
				+ ", getPmNumber()=" + getPmNumber() + ", getLongitude()=" + getLongitude() + ", getLattitude()="
				+ getLatitude() + ", getAction()=" + getAction() + ", getPmId()=" + getPmId() + ", getGetCountData()="
				+ getGetCountData() + ", getLastPmId()=" + getLastPmId() + ", getStartLookupDate()="
				+ getStartLookupDate() + ", getPage()=" + getPage() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}

	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}

	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getPmLocationName() {
		return pmLocationName;
	}

	public void setPmLocationName(String pmLocationName) {
		this.pmLocationName = pmLocationName;
	}

	public String getPmOwner() {
		return pmOwner;
	}

	public void setPmOwner(String pmOwner) {
		this.pmOwner = pmOwner;
	}

	public String getPmNumber() {
		return pmNumber;
	}

	public void setPmNumber(String pmNumber) {
		this.pmNumber = pmNumber;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPmId() {
		return pmId;
	}

	public void setPmId(String pmId) {
		this.pmId = pmId;
	}

	public String getGetCountData() {
		return getCountData;
	}

	public void setGetCountData(String getCountData) {
		this.getCountData = getCountData;
	}

	public String getLastPmId() {
		return lastPmId;
	}

	public void setLastPmId(String lastPmId) {
		this.lastPmId = lastPmId;
	}

	public String getStartLookupDate() {
		return startLookupDate;
	}

	public void setStartLookupDate(String startLookupDate) {
		this.startLookupDate = startLookupDate;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getEndLookupDate() {
		return endLookupDate;
	}

	public void setEndLookupDate(String endLookupDate) {
		this.endLookupDate = endLookupDate;
	}

}
