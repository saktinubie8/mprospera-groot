package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class LoanRefreshResponse extends BaseResponse {

	List<LoanRefreshListResponse> loanList;

	public List<LoanRefreshListResponse> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<LoanRefreshListResponse> loanList) {
		this.loanList = loanList;
	}

	@Override
	public String toString() {
		return "LoanRefreshResponse [loanList=" + loanList + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
