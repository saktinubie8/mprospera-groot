package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class VersionCheckResponse extends BaseResponse {
	private String currentVersion;
	private List<String> allowedVersions;
	private String url;

	public String getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public List<String> getAllowedVersions() {
		return allowedVersions;
	}

	public void setAllowedVersions(List<String> allowedVersions) {
		this.allowedVersions = allowedVersions;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
