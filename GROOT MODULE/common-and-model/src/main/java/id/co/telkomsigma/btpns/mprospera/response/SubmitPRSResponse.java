package id.co.telkomsigma.btpns.mprospera.response;

public class SubmitPRSResponse extends BaseResponse {

	String prsId;

	public String getPrsId() {
		return prsId;
	}

	public void setPrsId(String prsId) {
		this.prsId = prsId;
	}
}
