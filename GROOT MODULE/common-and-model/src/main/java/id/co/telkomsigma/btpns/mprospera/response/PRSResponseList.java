package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;
import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PRSResponseList {

	private String prsId;
	private String sentraId;
	private String hasPrsPhoto;
	private String prsDate;
	private String status;
	private BigDecimal bringMoney;
	private BigDecimal actualMoney;
	private BigDecimal paymentMoney;
	private BigDecimal withdrawalAdhoc;
	private String psCompanion;
	private String updatedDate;
	private List<PSCompanionPojo> psIdCompanionList;
	private List<DenomListResponse> denomList;
	private List<CustomerPRSListResponse> customerList;

	public String getPrsId() {
		return prsId;
	}

	public void setPrsId(String prsId) {
		this.prsId = prsId;
	}

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	public String getHasPrsPhoto() {
		return hasPrsPhoto;
	}

	public void setHasPrsPhoto(String hasPrsPhoto) {
		this.hasPrsPhoto = hasPrsPhoto;
	}

	public String getPrsDate() {
		return prsDate;
	}

	public void setPrsDate(String prsDate) {
		this.prsDate = prsDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPsCompanion() {
		return psCompanion;
	}

	public void setPsCompanion(String psCompanion) {
		this.psCompanion = psCompanion;
	}

	public List<PSCompanionPojo> getPsIdCompanionList() {
		return psIdCompanionList;
	}

	public void setPsIdCompanionList(List<PSCompanionPojo> psIdCompanionList) {
		this.psIdCompanionList = psIdCompanionList;
	}

	public List<DenomListResponse> getDenomList() {
		return denomList;
	}

	public void setDenomList(List<DenomListResponse> denomList) {
		this.denomList = denomList;
	}

	public List<CustomerPRSListResponse> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<CustomerPRSListResponse> customerList) {
		this.customerList = customerList;
	}

	public String getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}

	public BigDecimal getBringMoney() {
		return bringMoney;
	}

	public void setBringMoney(BigDecimal bringMoney) {
		this.bringMoney = bringMoney;
	}

	public BigDecimal getActualMoney() {
		return actualMoney;
	}

	public void setActualMoney(BigDecimal actualMoney) {
		this.actualMoney = actualMoney;
	}

	public BigDecimal getPaymentMoney() {
		return paymentMoney;
	}

	public void setPaymentMoney(BigDecimal paymentMoney) {
		this.paymentMoney = paymentMoney;
	}

	public BigDecimal getWithdrawalAdhoc() {
		return withdrawalAdhoc;
	}

	public void setWithdrawalAdhoc(BigDecimal withdrawalAdhoc) {
		this.withdrawalAdhoc = withdrawalAdhoc;
	}
	
}
