package id.co.telkomsigma.btpns.mprospera.response;

public class ESBLoan {

	private String appId;
	private String amount;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "Loan [appId=" + appId + ", amount=" + amount + "]";
	}

}
