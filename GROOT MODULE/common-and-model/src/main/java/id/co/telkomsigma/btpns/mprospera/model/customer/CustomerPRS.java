package id.co.telkomsigma.btpns.mprospera.model.customer;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Group;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_CUSTOMER_PRS")
public class CustomerPRS extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6214402301109985699L;

	private Long customerPrsId;
	private Customer customerId;
	private Group groupId;
	private Boolean isAttend;
	private PRSParameter notAttendReason;

	private Date createdDate;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;

	private PRS prsId;

	@Id
	@Column(name = "id", unique = true, nullable = false)
	@GeneratedValue
	public Long getCustomerPrsId() {
		return customerPrsId;
	}

	public void setCustomerPrsId(Long customerPrsId) {
		this.customerPrsId = customerPrsId;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = true)
	public Customer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Group.class)
	@JoinColumn(name = "group_id", referencedColumnName = "id", nullable = true)
	public Group getGroupId() {
		return groupId;
	}

	public void setGroupId(Group groupId) {
		this.groupId = groupId;
	}

	@Column(name = "IS_ATTEND")
	public Boolean getIsAttend() {
		return isAttend;
	}

	public void setIsAttend(Boolean isAttend) {
		this.isAttend = isAttend;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = PRSParameter.class)
	@JoinColumn(name = "PARAMETER_ID", referencedColumnName = "id", nullable = true)
	public PRSParameter getNotAttendReason() {
		return notAttendReason;
	}

	public void setNotAttendReason(PRSParameter notAttendReason) {
		this.notAttendReason = notAttendReason;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = PRS.class)
	@JoinColumn(name = "PRS_ID", referencedColumnName = "id", nullable = true)
	public PRS getPrsId() {
		return prsId;
	}

	public void setPrsId(PRS prsId) {
		this.prsId = prsId;
	}

	@Column(name = "UPDATED_DATE")
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY")
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}
}
