package id.co.telkomsigma.btpns.mprospera.model.mm;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_MINI_MEETING")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class MiniMeeting extends GenericModel {

	private static final long serialVersionUID = 2245862234980003931L;
	private Long mmId;
	private String areaId;
	private String phoneNumber;
	private String mmLocationName;
	private String mmOwner;
	private String mmNumber;
	private String longitude;
	private String latitude;
	private Date createdDate;
	private String createdBy;
	private String rrn;
	private Boolean isDeleted = false;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getMmId() {
		return mmId;
	}

	public void setMmId(Long mmId) {
		this.mmId = mmId;
	}

	@Column(name = "area_id", nullable = false)
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	@Column(name = "location_name", nullable = false)
	public String getMmLocationName() {
		return mmLocationName;
	}

	public void setMmLocationName(String mmLocationName) {
		this.mmLocationName = mmLocationName;
	}

	@Column(name = "owner", nullable = false)
	public String getMmOwner() {
		return mmOwner;
	}

	public void setMmOwner(String mmOwner) {
		this.mmOwner = mmOwner;
	}

	@Column(name = "mm_number", nullable = false)
	public String getMmNumber() {
		return mmNumber;
	}

	public void setMmNumber(String mmNumber) {
		this.mmNumber = mmNumber;
	}

	@Column(name = "longitude", nullable = false)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "latitude", nullable = false)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "created_date", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "created_by", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "is_deleted")
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name = "phone_number")
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Column(name = "rrn")
	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}
}
