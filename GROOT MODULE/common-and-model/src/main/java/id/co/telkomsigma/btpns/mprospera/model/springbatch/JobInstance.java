package id.co.telkomsigma.btpns.mprospera.model.springbatch;

import org.hibernate.annotations.OrderBy;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "BATCH_JOB_INSTANCE")
public class JobInstance {

	private BigInteger jobInstanceId;
	private BigInteger version;
	private String jobName;
	private String jobKey;
	private List<BatchJobExecution> jobExecutionList;

	@OneToMany(fetch = FetchType.EAGER, targetEntity = BatchJobExecution.class)
	@JoinColumn(name = "job_execution_id")
	@OrderBy(clause = "job_execution_id asc")
	public List<BatchJobExecution> getJobExecutionList() {
		return jobExecutionList;
	}

	public void setJobExecutionList(List<BatchJobExecution> stepList) {
		this.jobExecutionList = stepList;
	}

	@Id
	@Column(name = "job_instance_id", nullable = false, unique = true)
	public BigInteger getJobInstanceId() {
		return jobInstanceId;
	}

	public void setJobInstanceId(BigInteger jobInstanceId) {
		this.jobInstanceId = jobInstanceId;
	}

	@Column(name = "version", nullable = true)
	public BigInteger getVersion() {
		return version;
	}

	public void setVersion(BigInteger version) {
		this.version = version;
	}

	@Column(name = "job_name", nullable = false, length = 100)
	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	@Column(name = "job_key", nullable = false, length = 32)
	public String getJobKey() {
		return jobKey;
	}

	public void setJobKey(String jobKey) {
		this.jobKey = jobKey;
	}

}
