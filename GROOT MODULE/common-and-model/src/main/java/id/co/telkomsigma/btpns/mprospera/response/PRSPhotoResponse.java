package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Arrays;

public class PRSPhotoResponse extends BaseResponse {

	private byte[] photo;

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "PRSPhotoResponse [photo=" + Arrays.toString(photo) + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
