package id.co.telkomsigma.btpns.mprospera.request;

public class ESBCustomerNonPrsRequest {
	
	private String customerId;
	private ESBLoanNonPrsRequest[] loanList;
	private ESBSavingNonPrsRequest[] savingList;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public ESBLoanNonPrsRequest[] getLoanList() {
		return loanList;
	}
	public void setLoanList(ESBLoanNonPrsRequest[] loanList) {
		this.loanList = loanList;
	}
	public ESBSavingNonPrsRequest[] getSavingList() {
		return savingList;
	}
	public void setSavingList(ESBSavingNonPrsRequest[] savingList) {
		this.savingList = savingList;
	}

	
	
}
