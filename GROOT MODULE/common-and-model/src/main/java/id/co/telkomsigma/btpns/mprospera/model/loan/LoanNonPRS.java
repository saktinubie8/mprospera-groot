package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_Loan_Non_PRS")
public class LoanNonPRS extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long loanNonPRSId;
	private Date createdDate;
	private Date dueDate;
	private Customer customerId;
	private Loan loanId;
	private Long outstandingDays;
	private Long outstandingCount;
	private BigDecimal outstandingAmount;
	private BigDecimal currentMargin;
	private BigDecimal installmentAmount;
	private BigDecimal installmentPaymentAmount;
	private BigDecimal inputMoney;
	private BigDecimal depositMoney;
	private BigDecimal autoDebet;
	private BigDecimal remainingPrincipal;
	private BigDecimal marginAmount;
	private Date meetingDate;
	private PRSParameter peopleMeet;
	private boolean payCommitment;
	private PRSParameter outstandingReason;
	private Sentra sentraId;
	private String createdBy;
	private Date updatedDate;
	private String updatedBy;
	private String status;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getLoanNonPRSId() {
		return loanNonPRSId;
	}

	public void setLoanNonPRSId(Long loanNonPRSId) {
		this.loanNonPRSId = loanNonPRSId;
	}

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
	@JoinColumn(name = "customer_id", referencedColumnName = "id", nullable = true)
	public Customer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

	@ManyToOne(fetch = FetchType.LAZY, targetEntity = Sentra.class)
	@JoinColumn(name = "SENTRA_ID", referencedColumnName = "id", nullable = true)
	public Sentra getSentraId() {
		return sentraId;
	}

	public void setSentraId(Sentra sentraId) {
		this.sentraId = sentraId;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Loan.class)
	@JoinColumn(name = "loan_Id", referencedColumnName = "id", nullable = true)
	public Loan getLoanId() {
		return loanId;
	}

	public void setLoanId(Loan loanId) {
		this.loanId = loanId;
	}

	@Column(name = "OUTSTANDING_DAYS")
	public Long getOutstandingDays() {
		return outstandingDays;
	}

	public void setOutstandingDays(Long outstandingDays) {
		this.outstandingDays = outstandingDays;
	}

	@Column(name = "OUTSTANDING_AMT")
	public BigDecimal getOutstandingAmount() {
		return outstandingAmount;
	}

	public void setOutstandingAmount(BigDecimal outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}

	@Column(name = "CURRENT_MARGIN")
	public BigDecimal getCurrentMargin() {
		return currentMargin;
	}

	public void setCurrentMargin(BigDecimal currentMargin) {
		this.currentMargin = currentMargin;
	}

	@Column(name = "INSTALLMENT_AMT")
	public BigDecimal getInstallmentAmount() {
		return installmentAmount;
	}

	public void setInstallmentAmount(BigDecimal installmentAmount) {
		this.installmentAmount = installmentAmount;
	}

	@Column(name = "MEETING_DATE", nullable = true)
	public Date getMeetingDate() {
		return meetingDate;
	}

	public void setMeetingDate(Date meetingDate) {
		this.meetingDate = meetingDate;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = PRSParameter.class)
	@JoinColumn(name = "OUTSTANDING_REASON", referencedColumnName = "id", nullable = true)
	public PRSParameter getOutstandingReason() {
		return outstandingReason;
	}

	public void setOutstandingReason(PRSParameter outstandingReason) {
		this.outstandingReason = outstandingReason;
	}

	@Column(name = "CREATED_DATE")
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "CREATED_BY")
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "UPDATED_DATE", nullable = true)
	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	@Column(name = "UPDATED_BY", nullable = true)
	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = PRSParameter.class)
	@JoinColumn(name = "PEOPLE_MEET", referencedColumnName = "id", nullable = true)
	public PRSParameter getPeopleMeet() {
		return peopleMeet;
	}

	public void setPeopleMeet(PRSParameter peopleMeet) {
		this.peopleMeet = peopleMeet;
	}

	@Column(name = "PAY_COMMITMENT", nullable = true)
	public boolean getPayCommitment() {
		return payCommitment;
	}

	public void setPayCommitment(boolean payCommitment) {
		this.payCommitment = payCommitment;
	}

	@Column(name = "STATUS")
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "INSTALLMENT_PAY_AMT")
	public BigDecimal getInstallmentPaymentAmount() {
		return installmentPaymentAmount;
	}

	public void setInstallmentPaymentAmount(BigDecimal installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}

	@Column(name = "OUTSTANDING_COUNT")
	public Long getOutstandingCount() {
		return outstandingCount;
	}

	public void setOutstandingCount(Long outstandingCount) {
		this.outstandingCount = outstandingCount;
	}

	@Column(name = "DUE_DATE", nullable = true)
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	@Column(name = "REMAINING_PRINCIPAL")
	public BigDecimal getRemainingPrincipal() {
		return remainingPrincipal;
	}

	public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
		this.remainingPrincipal = remainingPrincipal;
	}

	@Column(name = "MARGIN_AMOUNT")
	public BigDecimal getMarginAmount() {
		return marginAmount;
	}

	public void setMarginAmount(BigDecimal marginAmount) {
		this.marginAmount = marginAmount;
	}

	@Column(name = "INPUT_MONEY")
	public BigDecimal getInputMoney() {
		return inputMoney;
	}

	public void setInputMoney(BigDecimal inputMoney) {
		this.inputMoney = inputMoney;
	}

	@Column(name = "DEPOSIT_MONEY")
	public BigDecimal getDepositMoney() {
		return depositMoney;
	}

	public void setDepositMoney(BigDecimal depositMoney) {
		this.depositMoney = depositMoney;
	}

	@Column(name = "ÄUTO_DEBET")
	public BigDecimal getAutoDebet() {
		return autoDebet;
	}

	public void setAutoDebet(BigDecimal autoDebet) {
		this.autoDebet = autoDebet;
	}

}
