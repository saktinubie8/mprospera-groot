package id.co.telkomsigma.btpns.mprospera.pojo;

import org.springframework.web.multipart.MultipartFile;

public class UploadApkForm {

	private String name;
	private MultipartFile file;
	private String version;
	private String description;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
		this.name = file.getOriginalFilename();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
