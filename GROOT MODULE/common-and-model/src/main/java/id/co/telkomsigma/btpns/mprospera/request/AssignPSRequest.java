package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AssignPSRequest extends BaseRequest {

	private String imei;
	private String sessionKey;
	private String username;
	private String userPs;
	private List<SentraForAssignPSRequest> sentraList;

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getUserPs() {
		return userPs;
	}

	public void setUserPs(String userPs) {
		this.userPs = userPs;
	}

	public List<SentraForAssignPSRequest> getSentraList() {
		return sentraList;
	}

	public void setSentraList(List<SentraForAssignPSRequest> sentraList) {
		this.sentraList = sentraList;
	}

	@Override
	public String toString() {
		return "AssignPSRequest [imei=" + imei + ", sessionKey=" + sessionKey + ", username=" + username + ", userPs="
				+ userPs + ", sentraList=" + sentraList + ", getTransmissionDateAndTime()="
				+ getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
