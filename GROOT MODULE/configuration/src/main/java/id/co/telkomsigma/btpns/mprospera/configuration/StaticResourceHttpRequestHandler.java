package id.co.telkomsigma.btpns.mprospera.configuration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

public class StaticResourceHttpRequestHandler extends ResourceHttpRequestHandler {
	protected Log log = LogFactory.getLog(this.getClass());

	private Map<String, String> headers = Collections.emptyMap();

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	@Override
	protected void setHeaders(HttpServletResponse response, Resource resource, MediaType mediaType) throws IOException {
		log.debug("Adding headers");
		for (Map.Entry<String, String> entry : this.headers.entrySet()) {
			response.setHeader(entry.getKey(), entry.getValue());
		}
		super.setHeaders(response, resource, mediaType);
	}
}
