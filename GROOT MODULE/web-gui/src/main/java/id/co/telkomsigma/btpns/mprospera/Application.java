package id.co.telkomsigma.btpns.mprospera;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jmx.JmxAutoConfiguration;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication(exclude = JmxAutoConfiguration.class)
@EnableCaching
@EnableScheduling
@ComponentScan
@Configuration
@EnableDiscoveryClient
@EnableFeignClients
public class Application extends SpringBootServletInitializer  {
    private final Log log = LogFactory.getLog(this.getClass());
    

    public static void main(String[] args) {
        //SpringApplication.run(Application.class, args);
        ApplicationContext ctx = SpringApplication.run(Application.class, args);
    
    }
}