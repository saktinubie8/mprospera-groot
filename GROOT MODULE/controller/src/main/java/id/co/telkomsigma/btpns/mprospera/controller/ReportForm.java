package id.co.telkomsigma.btpns.mprospera.controller;

public class ReportForm {

	private String kfoName;
	private String kfoLocationId;
	private String mmsName;
	private String mmsLocationId;
	private String inputDate;
	private String extType;

	public String getMmsName() {
		return mmsName;
	}

	public void setMmsName(String mmsName) {
		this.mmsName = mmsName;
	}

	public String getMmsLocationId() {
		return mmsLocationId;
	}

	public void setMmsLocationId(String mmsLocationId) {
		this.mmsLocationId = mmsLocationId;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getKfoName() {
		return kfoName;
	}

	public void setKfoName(String kfoName) {
		this.kfoName = kfoName;
	}

	public String getKfoLocationId() {
		return kfoLocationId;
	}

	public void setKfoLocationId(String kfoLocationId) {
		this.kfoLocationId = kfoLocationId;
	}

	public String getExtType() {
		return extType;
	}

	public void setExtType(String extType) {
		this.extType = extType;
	}

	@Override
	public String toString() {
		return "ReportForm [kfoName=" + kfoName + ", kfoLocationId=" + kfoLocationId + ", mmsName=" + mmsName
				+ ", mmsLocationId=" + mmsLocationId + ", inputDate=" + inputDate + ", extType=" + extType + "]";
	}

}
