package id.co.telkomsigma.btpns.mprospera.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.parameter.MailParameter;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.MailParameterService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;

@Controller
public class MailController extends GenericController {

    private static final String MAIL_EDITOR_PAGE = "manage/maileditor/list";

    @Autowired
    private MailParameterService mailParamSvc;

    @Autowired
    private AuditLogService auditLogService;

    @RequestMapping(value = WebGuiConstant.MAIL_EDITOR_PATH_MAPPING,
            method = RequestMethod.GET)
    public String showMailEditor(final Model model, final HttpServletRequest request,
                                 final HttpServletResponse response) {
        List<MailParameter> parameter = mailParamSvc.getAll();

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.VIEW_PAGE);
        auditLog.setCreatedBy(user.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setDescription("Buka Halaman Mail Editor");
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {
            auditLogService.insertAuditLog(auditLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.debug("Added Audit Log");

        model.addAttribute("parameter", parameter);
        return getPageContent(MAIL_EDITOR_PAGE);
    }

    @RequestMapping(value = WebGuiConstant.MAIL_EDITOR_EDIT_PATH_MAPPING, method = {RequestMethod.POST})
    public String editValue(@RequestParam(value = "paramId", required = true) final Long id,
                            @RequestParam(value = "paramValue", required = false) String messageValue,
                            @RequestParam(value = "dateformat", required = false) String dateFormat,
                            @RequestParam(value = "expiredin", required = false) String expiredIn,
                            @ModelAttribute("parameter") MailParameter parameter, BindingResult errors,
                            final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        try {
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            User user = (User) auth.getPrincipal();
            AuditLog auditLog = new AuditLog();
            auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
            auditLog.setCreatedBy(user.getUsername());
            auditLog.setCreatedDate(new Date());
            auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));
            try {
                MailParameter editParameter = mailParamSvc.getParamById(id);
                if (!errors.hasErrors()) {
                    try {
                        if (messageValue != null && !messageValue.isEmpty()) {
                            mailParamSvc.updateParam(editParameter,
                                    HtmlUtils.htmlEscape(messageValue));
                        } else if (expiredIn != null && !expiredIn.isEmpty()) {
                            mailParamSvc.updateParam(editParameter,
                                    HtmlUtils.htmlEscape(expiredIn));
                        } else if (dateFormat != null && !dateFormat.isEmpty()) {
                            mailParamSvc.updateParam(editParameter,
                                    HtmlUtils.htmlEscape(dateFormat));
                        }
                        model.addAttribute("okMessage", getMessage("form.ok.edit.param"));
                        auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(editParameter.getMessageParam()) + " Berhasil");
                        try {
                            auditLogService.insertAuditLog(auditLog);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } catch (HttpClientErrorException e) {
                        log.error(e.getStatusCode().toString(), e);
                        auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(editParameter.getMessageParam()) + " Gagal");
                        try {
                            auditLogService.insertAuditLog(auditLog);
                        } catch (Exception ea) {
                            ea.printStackTrace();
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage(), e);
                        model.addAttribute("errorMessage", getMessage("field.error.param.not.found", HtmlUtils.htmlEscape(parameter.getMessageParam())));
                        auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(editParameter.getMessageParam()) + " Gagal");
                        try {
                            auditLogService.insertAuditLog(auditLog);
                        } catch (Exception ea) {
                            ea.printStackTrace();
                        }
                    }
                }
            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(parameter.getMessageParam()) + " Gagal");
                try {
                    auditLogService.insertAuditLog(auditLog);
                } catch (Exception ea) {
                    ea.printStackTrace();
                }
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                model.addAttribute("errorMessage", getMessage("field.error.param.not.found", HtmlUtils.htmlEscape(parameter.getMessageParam())));
                auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(parameter.getMessageParam()) + " Gagal");
                try {
                    auditLogService.insertAuditLog(auditLog);
                } catch (Exception ea) {
                    ea.printStackTrace();
                }
            }
        } catch (UsernameNotFoundException e) {
            log.error(e.getMessage(), e);
            model.addAttribute("errorMessage", getMessage("form.error.edit.param"));
        }
        return getPageContent(MAIL_EDITOR_PAGE);
    }

    @RequestMapping(value = WebGuiConstant.MAIL_EDITOR_PATH_MAPPING_GET_LIST, method = RequestMethod.GET)
    public void doSearch(final Model model, final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);
            DataTablesPojo dataTablePojo = new DataTablesPojo();
            try {
                dataTablePojo.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablePojo.setDraw(0);
            }

            final int orderableColumnCount = 4;
            final StringBuffer sbOrder = new StringBuffer();
            LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
                {
                    put("0", "messageParameter");
                    put("1", "messageDescription");
                    put("2", "messageValue");
                }
            };
            int offset = 0;
            int limit = 5;
            try {
                offset = Integer.parseInt(request.getParameter("start"));
            } catch (NumberFormatException e) {

            }
            try {
                limit = Integer.parseInt(request.getParameter("length"));
            } catch (NumberFormatException e) {

            }
            for (int i = 0; i < orderableColumnCount; i++) {
                String orderColumn = request.getParameter("order[" + i + "][column]");
                if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
                    break;
                } else {
                    orderColumn = columnMap.get(orderColumn);
                    if (orderColumn == null) {
                        break;
                    }
                    String orderDir = request.getParameter("order[" + i + "][dir]");
                    if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
                        orderDir = "asc";
                    }
                    sbOrder.append(orderColumn);
                    sbOrder.append("-");
                    sbOrder.append(orderDir);
                    sbOrder.append("+");
                }
            }
            final String keyword = request.getParameter("q");
            try {
                final int finalOffset = offset;
                final int finalLimit = limit;
                Page<MailParameter> paramList = mailParamSvc.searchMailParam(keyword, columnMap, finalOffset, finalLimit);
                List<String[]> data = new ArrayList<>();
                for (MailParameter param : paramList) {
                    String url = "";
                    url = url
                            + "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"edit\" data-value=\""
                            + HtmlUtils.htmlEscape(param.getMessageValue())
                            + "\" data-messageparam=\""
                            + HtmlUtils.htmlEscape(param.getMessageParam())
                            + "\" data-messagedescription=\""
                            + HtmlUtils.htmlEscape(param.getMessageDescription())
                            + "\" data-link=\""
                            + getServletContext().getContextPath()
                            + WebGuiConstant.MAIL_EDITOR_EDIT_PATH_REQUEST.replace("{paramId}",
                            "" + param.getId()) + "\" data-id=\"" + param.getId()
                            + "\"><i class='fa fa-edit'><div class='action' id='action-edit'>edit</div></i></a>";
                    data.add(new String[]{HtmlUtils.htmlEscape(param.getMessageParam()), HtmlUtils.htmlEscape(param.getMessageDescription()), HtmlUtils.htmlEscape(param.getMessageValue()), url});
                }
                dataTablePojo.setData(data);
                dataTablePojo.setRecordsFiltered(Integer.parseInt("" + paramList.getTotalElements()));
                dataTablePojo.setRecordsTotal(Integer.parseInt("" + paramList.getTotalElements()));
            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                dataTablePojo.setError(getMessage("message.datatables.error.fetch.user"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                dataTablePojo.setError(getMessage("message.datatables.error.fetch.user"));
            } finally {
                response.getWriter().write(new JsonUtils().toJson(dataTablePojo));

            }

        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

}