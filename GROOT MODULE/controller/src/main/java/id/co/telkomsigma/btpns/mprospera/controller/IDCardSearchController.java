package id.co.telkomsigma.btpns.mprospera.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.CustomerIDPhoto;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.CustomerService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;

@Controller
public class IDCardSearchController extends GenericController {

    public static final String LIST_PAGE_NAME = "manage/idcard/list";

    @Autowired
    private CustomerService customerSvc;

    @Autowired
    private AuditLogService auditLogService;

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_IDCARD_SEARCH_MAPPING, method = RequestMethod.GET)
    private String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        AuditLog auditLog = new AuditLog();
        auditLog.setActivityType(auditLog.VIEW_PAGE);
        auditLog.setCreatedBy(user.getUsername());
        auditLog.setCreatedDate(new Date());
        auditLog.setDescription("Buka Halaman Baca Kartu Identitas");
        auditLog.setReffNo(new SimpleDateFormat("YYYYMMDDhhmmss").format(new Date()));

        try {
            auditLogService.insertAuditLog(auditLog);
        } catch (Exception e) {
            e.printStackTrace();
        }
        log.debug("Added Audit Log");
        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_IDCARD_SEARCH_GET_LIST_MAPPING, method = RequestMethod.GET)
    private void doSearch(final Model model, final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);
            DataTablesPojo dataTables = new DataTablesPojo();
            try {
                dataTables.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTables.setDraw(0);
            }

            final int orderableColumnCount = 4;
            final StringBuffer sbOrder = new StringBuffer();
            LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>() {
                {
                    put("0", "customerCifNumber");
                    put("1", "customerIdNumber");
                    put("2", "customerName");
                    put("3", "dateOfBirth");

                }
            };

            int offset = 0;
            int limit = 25;
            try {
                offset = Integer.parseInt(request.getParameter("start"));
            } catch (NumberFormatException e) {

            }
            try {
                limit = Integer.parseInt(request.getParameter("length"));
            } catch (NumberFormatException e) {

            }

            for (int i = 0; i < orderableColumnCount; i++) {
                String orderColumn = request.getParameter("order[" + i + "][column]");
                if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
                    break;
                } else {
                    orderColumn = columnMap.get(orderColumn);
                    if (orderColumn == null) {
                        break;
                    }
                    String orderDir = request.getParameter("order[" + i + "][dir]");
                    if (orderDir == null || orderDir.equalsIgnoreCase("desc")) {
                        orderDir = "asc";
                    }
                    sbOrder.append(orderColumn);
                    sbOrder.append("-");
                    sbOrder.append(orderDir);
                    sbOrder.append("+");
                }
            }

            final String keyword = request.getParameter("q");
            try {
                final int finalOffset = offset;
                final int finalLimit = limit;
                if (!keyword.isEmpty() || !keyword.equalsIgnoreCase("")) {
                    if (keyword.length() < 10) {
                        dataTables.setError(getMessage("message.id.validate.keyword"));
                        setEmptyTables(dataTables);
                    } else {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/YYYY");
                        Page<CustomerIDPhoto> customerList = customerSvc.searchCustomerByCiffOrIdNumber(keyword, columnMap, finalOffset, finalLimit);
                        List<String[]> data = new ArrayList<>();
                        for (CustomerIDPhoto cust : customerList) {
                        	String photoBase64 = cust.getIdPhoto() == null ? "": new String(cust.getIdPhoto());
                            String url = "";
                            url = url
                                    + "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"view\" data-img=\""
                                    + photoBase64
                                    + "\" data-nama=\""
                                    + cust.getCustomerName()
                                    + "\" data-link=\""
                                    + getServletContext().getContextPath()
                                    + WebGuiConstant.AFTER_LOGIN_PATH_IDCARD_SEARCH_VIEW_REQUEST.replace("{customerId}",
                                    "" + cust.getCustomerId()) + "\" data-id=\"" + cust.getCustomerId()
                                    + "\"><i class='fa fa-eye'><div class='action' id='action-edit'>edit</div></i></a>";
                            data.add(new String[]{HtmlUtils.htmlEscape(cust.getCustomerCifNumber()), HtmlUtils.htmlEscape(cust.getCustomerIdNumber())
                                    , HtmlUtils.htmlEscape(cust.getCustomerName()), HtmlUtils.htmlEscape(sdf.format(cust.getDateOfBirth())), url, ""});

                        }
                        dataTables.setData(data);
                        dataTables.setRecordsFiltered(Integer.parseInt("" + customerList.getTotalElements()));
                        dataTables.setRecordsTotal(Integer.parseInt("" + customerList.getTotalElements()));
                    }
                } else {
                    setEmptyTables(dataTables);
                }
            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                dataTables.setError(getMessage("message.datatables.error.fetch.user"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                dataTables.setError(getMessage("message.datatables.error.fetch.user"));
            } finally {
                response.getWriter().write(new JsonUtils().toJson(dataTables));
            }
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }

    private void setEmptyTables(DataTablesPojo dataTables) {
        List<String[]> data = new ArrayList<>();
        data.add(new String[]{"", "", "", "", "", ""});
        dataTables.setData(data);
        dataTables.setRecordsFiltered(0);
        dataTables.setRecordsTotal(0);
    }

}